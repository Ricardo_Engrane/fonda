var util = (() => {

    let is_chrome   = navigator.userAgent.indexOf('Chrome') > -1;
    let is_explorer = navigator.userAgent.indexOf('MSIE') > -1;
    let is_firefox  = navigator.userAgent.indexOf('Firefox') > -1;
    let is_safari   = navigator.userAgent.indexOf("Safari") > -1;
    let is_opera    = navigator.userAgent.toLowerCase().indexOf("op") > -1;

    if ((is_chrome)&&(is_safari)) is_safari = false;
    if ((is_chrome)&&(is_opera)) is_chrome = false;

    return {
        isMobile: () => {
            let check = false;
            (a => {
                if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
            })(navigator.userAgent || navigator.vendor || window.opera);
            return check;
        },

        isSafari : is_safari,
    };

})();

const isMobile = util.isMobile();
const isSafari = util.isSafari;

var mySwiperInicio = new Swiper ('.slider-inicio', {
	// Optional parameters
	loop: false,
	effect: 'slide',
	// autoplay: {
	// 	delay: 10000
	// },
	resistanceRatio: 0,
	// slidesPerView: slides_PerView,
	// spaceBetween: space_Between,
	// observer: true,
	// width: 352,
	// If we need pagination
	pagination: {
		el: '.swiper-pagination',
		clickable : true
	},
	// Navigation arrows
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
});




if(isMobile){
	$('.form-wrap.horario .input').addClass('place-mobile');
	$('.form-wrap.horario .input').click(function(event) {
		$('.form-wrap.horario .input input').focus();
		$('.form-wrap.horario .input').removeClass('place-mobile');
	});

	$('.form-wrap.horario .input input').focusout(function(event) {
		if($(this).val() == ""){
			$('.form-wrap.horario .input').addClass('place-mobile');
		}
	});
}

/*$.fn.shuffle = function() { 
    var allElems = this.get(),
        getRandom = function(max) {
            return Math.floor(Math.random() * max);
        },
        shuffled = $.map(allElems, function(){
            var random = getRandom(allElems.length),
                randEl = $(allElems[random]).clone(true)[0];
            allElems.splice(random, 1);
            return randEl;
       });

    this.each(function(i){
        $(this).replaceWith($(shuffled[i]));
    });

    return $(shuffled);
};*/


$(".hamburger, a.link-contacto").click(function(){
	$('.hamburger').toggleClass("is-active");
	$('.menu-nav').toggleClass("is-active");
});


window.onresize = function(){

	if( innerWidth >= 1024 ){
		$('.hamburger').removeClass("is-active");
		$('.menu-nav').removeClass("is-active");	
	}

};

// $("#form-contrata").submit(function (e){
// 	e.preventDefault();
// });

window.onload = function(){

	// scroll();
};
window.onscroll = function(){
	//scroll();
};


var map;
var estilos_mapa = 
[
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
];

function initMap() {
	if($('#mapa').length){
		var id        = 'mapa';
		var fonda_ubi = {lat: 19.436937420746293, lng: -99.21216351367053};
		var icon      = '/assets/img/pin_fonda.png';

		map = new google.maps.Map(document.getElementById(id), {
			zoom: 16,
			center: fonda_ubi,
			styles: estilos_mapa
		});

		var marker_fonda = new google.maps.Marker({
			position: fonda_ubi,
			map: map,
			icon: icon
		});
	}
}





// var lastScrollTop = 0;
// function scroll(){

// 	if($('#contacto').length){
// 		var pageY = window.pageYOffset || document.documentElement.scrollTop;
// 		if( pageY + 300 <= $('#contacto').offset().top){
// 			$('.disclaimer-head').removeClass('disclaimer-head--hide');
// 			$('.header-contacto-link').removeClass('active-page');
// 		}else{
// 			$('.disclaimer-head').addClass('disclaimer-head--hide');
// 			$('.header-contacto-link').addClass('active-page');
// 		}
// 	}

// 	if($('.carta-platillos').length){

// 		var st = window.pageYOffset || document.documentElement.scrollTop;
// 		if (st > lastScrollTop) {
// 			$('.disclaimer-head').addClass('disclaimer-head--hide');
// 			// console.log('para abajo');
// 		} else {
// 			$('.disclaimer-head').removeClass('disclaimer-head--hide');
// 			// console.log('para arriba');
// 		}
// 		lastScrollTop = st;

// 	}
// }


/*======================================
=            Crea tu evento            =
======================================*/

$('.wrap-salon').click( function (){
	if(!$(this).hasClass('wrap-salon--active') && !$(this).hasClass('wrap-salon--disable')){
		$('.wrap-salon').removeClass('wrap-salon--active');
		$(this).toggleClass('wrap-salon--active');
		var _salon = $(this).attr('data-salon');
		$('#input-hidden-salon').val(_salon);
		$('.revisa-salon').html('<span>SALÓN:</span> '+_salon.toUpperCase()).removeClass('revisa-error');
	}
});

var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

if($('.contrata-eventos').length){

	$('.validate-api-salones').focusout(function(event) {

        $('.wrap-salon').addClass('wrap-salon--disable');

		$('.validate-api-salones').each(function(index, el) {
			if(el.value == ""){
				$(el).addClass('error');

				if(el.name == "tipo_evento") {
					$('.revisa-evento').html('ESCRIBE EL TIPO DE EVENTO').addClass('revisa-error');
				}

				if(el.name == "personas") {
					$('.revisa-invitados').html('ESCRIBE TU NÚMERO DE INVITADOS').addClass('revisa-error');
				}

				if(el.name == "fecha") {
					$('.revisa-fecha').html('SELECCIONA UNA FECHA').addClass('revisa-error');
				}

				if(el.name == "hora_ini" || el.name == "hora_fin") {
					$('.revisa-horario').html('SELECCIONA UN HORARIO').addClass('revisa-error');
				}

				if(el.name == "salon") {
					$('.revisa-salon').html('ELIGE UN SALÓN').addClass('revisa-error');
				}


			}else{
				$(el).removeClass('error');

				if(el.name == "tipo_evento") {
					$('.revisa-evento').html('<span>EVENTO:</span> '+ el.value).removeClass('revisa-error');
				}

				if(el.name == "personas") {
					$('.revisa-invitados').html(el.value + ' INVITADOS').removeClass('revisa-error');
				}

				if(el.name == "fecha") {
					var date = new Date(el.value+ 'T12:00:00Z');
					// console.log(date);
					$('.revisa-fecha').html('<span>FECHA:</span> ' + date.getDate() + ' ' + meses[date.getMonth()] + ' ' + date.getFullYear() ).removeClass('revisa-error');
				}

				if(el.name == "hora_fin" && $('select[name="hora_ini"]').val() != "") {
					$('.revisa-horario').html('DE '+ $('select[name="hora_ini"]').val() +' H A '+el.value+' H').removeClass('revisa-error');
				}

				if(el.name == "salon") {
					$('.revisa-salon').html('<span>SALÓN:</span> '+el.value.toUpperCase()).removeClass('revisa-error');
				}			
			}

		});
		
		if( $('.validate-api-salones[name="personas"]').val() != "" && $('.validate-api-salones[name="fecha"]').val() != "" && $('.validate-api-salones[name="hora_ini"]').val() != "" && $('.validate-api-salones[name="hora_fin"]').val() != ""	){
			//console.log("si hizo algo");
	  	 	$.ajax({
				 url  : "api/eventos/validar",
				 type : 'POST',
				 data : {
				 	
				 	fecha    : $('.validate-api-salones[name="fecha"]').val(),
				 	hora_ini : $('.validate-api-salones[name="hora_ini"]').val(),
				 	hora_fin : $('.validate-api-salones[name="hora_fin"]').val(),
				 	personas : $('.validate-api-salones[name="personas"]').val(),
				 },
                 success : function(data) {
                 	//console.log(data);
                 	var json = JSON.parse(data);

             		$(".disponibilidad").show();
					$('.wrap-salon').removeClass('wrap-salon--active');
					$('#input-hidden-salon').val('');
					$('.revisa-salon').html('ELIGE UN SALÓN').addClass('revisa-error');


             		$('.wrap-salon').each(function(index, el) {
	                 	for(var k in json){
	                 		// console.log(json[k]);
             				// console.log($(el).attr('data-salon'));
             				// console.log(json[k].salon.salon);

             				if($(el).attr('data-salon') == json[k].salon.salon){
             					$(el).removeClass('wrap-salon--disable');
             					$(el).find('.disponibilidad').hide();
             				}
             				
	                 		
	                 	}
             		});
                 }
             })
             .fail(function() {
             	console.log("error");
             });
			

		}
	});

	$("#form-contrata").validate({
		ignore: "",
		rules: {
			salon: "required",
			tipo_evento: "required",
			personas : {
				required: true,
				number: true
			},
			fecha : {
				required : true,
				dateISO : true
				 
			},
			hora_ini : "required",
			hora_fin : "required",
			nombre: "required",
			email: "required",
			telefono: "required",
			aceptar: "required",

		},
		errorPlacement: function(error, element) {
			if (element.attr('name') == "salon") {
				$('.revisa-salon').html('ELIGE UN SALÓN').addClass('revisa-error');
			} else {

			}
			return;
		},
	  	submitHandler: function(form) {
	  		$('#form-contrata button').prop('disabled', true).addClass('active');
	  		$.ajax({
                url  : "api/eventos/enviar",
                type : 'POST',
                data : {
					fecha: form.fecha.value,
					hora_ini : form.hora_ini.value,
					hora_fin : form.hora_fin.value,
					personas : form.personas.value,
					nombre : form.nombre.value,
					email : form.email.value,
					telefono : form.telefono.value,
					salon : form.salon.value,
					tipo_evento : form.tipo_evento.value,
					captcha: grecaptcha.getResponse()
                },

                success : function(response) {
                	var obj = JSON.parse(response);
            		// console.log(obj);
					if (obj.mensaje == "success")
					{
						$('.revisa-datos').html(obj.data);
					} else if (obj.mensaje == "fail") {
						$('.revisa-datos').html(obj.data);
	  					$('#form-contrata button').prop('disabled', false).removeClass('active');
					}
                }
            })
            .fail(function() {
				console.log("error");
            });
	    	
	  	},
	});
}


/*=====  End of Crea tu evento  ======*/

/*============================================
=            Form contacto inicio            =
============================================*/

if($('#form-inicio-contacto').length){
	// console.log('inicio');
	$("#form-inicio-contacto").validate({
		rules: {
			nombre: "required",
			correo: {
				required: true,
				email: true
			},
			telefono: {
				required: true,
				number: true
			},
			comentario: "required"
		},
		errorPlacement: function(error, element) {
			// console.log(element.text());
			if (element.attr('name') == "") {
				//error.insertAfter("#lastname");
			} else {
				//error.insertAfter(element);
			}
			return;
		},
	  	submitHandler: function(form) {
	  		$('#form-inicio-contacto button').prop('disabled', true);
	  		$.ajax({
                url  : "api/contacto/enviar-formulario",
                type : 'POST',
                data : {
                    nombre     : form.nombre.value,
                    telefono   : form.telefono.value,
                    correo     : form.correo.value,
                    comentario : form.comentario.value
                },

                success : function(data) {
                	// console.log(data);
                  if (data == "success")
                  {
					$('.msg-form').fadeIn('fast');
                  }
                }
            })
            .fail(function() {
				console.log("error");
            });
	    	
	  	},
	});
}

/*=====  End of Form contacto inicio  ======*/
