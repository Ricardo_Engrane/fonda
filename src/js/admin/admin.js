$(document).ready(function (){
    $('#tabla_almendros').DataTable({
	    "language": {
            "url": "/assets/spanish.json"
        }
    });
});


    // $(".form_datetime").datepicker({
    //     format: "yyyy-mm-dd"
    // });

    //         $('#cambio_horaini').timepicker(
    //         	{

    //         		minuteStep: 1,
    //             template: 'modal',
    //             appendWidgetTo: 'body',
    //             showSeconds: true,
    //             showMeridian: false,
    //             defaultTime: false
    //         	});

    //         $('#cambio_horafin').timepicker(
    //         	{
  
    //         		minuteStep: 1,
    //             template: 'modal',
    //             appendWidgetTo: 'body',
    //             showSeconds: true,
    //             showMeridian: false,
    //             defaultTime: false
    //         	});

	$('#nueva_fecha').on( 'change', function() {
    if( $(this).is(':checked') ) {
        // Hacer algo si el checkbox ha sido seleccionado
        $('#select_salon').prop( "disabled", false );
        //$('#lugar').prop( "disabled", false );
        $('#cambio_horafin').prop( "disabled", false );
        $('#cambio_horaini').prop( "disabled", false );
        $('#cambio_fecha').prop( "disabled", false );
        $('#nuevas_personas').prop( "disabled", false );
     
    } else {
        $('#select-salon').prop( "disabled", true );
       // $('#lugar').prop( "disabled", true );
        $('#cambio_horafin').prop( "disabled", true );
        $('#cambio_horaini').prop( "disabled", true );
        $('#cambio_fecha').prop( "disabled", true );
        $('#nuevas_personas').prop( "disabled", true );
        
        
    }
});

	$('#form_evento').change(function (event){
			var form = event.currentTarget;
			console.log(form.nuevas_personas.value);
                    $.ajax({
                        url  : "/api/eventos/validarBack",
                        type : 'POST',
                        data : {
                        	id:     form.id.value,
                           // lugar   : form.lugar.value,
                            fecha    : form.cambio_fecha.value,
                            hora_ini : form.cambio_horaini.value,
                            hora_fin : form.cambio_horafin.value,
                            personas : form.nuevas_personas.value,
                            salon    :  form.select_salon.value,
                            nombre_responsable : form.nombre_responsable.value,
                            correo_responsable : form.correo_responsable.value,

                        },
                        success : function(data) {

                          console.log(data);

                          if (data == "success")
                          {
                            $('#fecha_exito').css('display','initial');
                            $('#fecha_falla').css('display','none');
                          }


                          if(data == "fail")
                          {
                            $('#fecha_falla').css('display','initial');
                            $('#fecha_exito').css('display','none');
                          }

                        }
                    })
                    .fail(function() {
                        console.log("error");
                    });
                
	});


if($('.select-icons').length){
    switch ($('.select-icons option:selected').val()) {
        case 'pendiente':
            $('.select-icons').addClass('yellow');
            $('.icon').addClass('icon--yellow');
            $('.icon span').attr('data-glyph','eye');
            break;
        case 'confirmado':
            $('.select-icons').addClass('blue');
            $('.icon').addClass('icon--blue');
            $('.icon span').attr('data-glyph','check');
            break;
        case 'cancelado':
            $('.select-icons').addClass('black');
            $('.icon').addClass('icon--black');
            $('.icon span').attr('data-glyph','ban');
            break;
        case 'eliminado':
            $('.select-icons').addClass('red');
            $('.icon').addClass('icon--red');
            $('.icon span').attr('data-glyph','trash');
            break;
        default:
            // statements_def
            break;
    }    
}

$('.select-icons').change(function(event){
    
    $(this).removeClass('yellow blue black red');
    $('.icon').removeClass('icon--yellow icon--blue icon--black icon--red');

    switch (event.target.value) {
        case 'pendiente':
            event.target.classList.add('yellow');
            $('.icon').addClass('icon--yellow');
            $('.icon span').attr('data-glyph','eye');
            break;
        case 'confirmado':
            event.target.classList.add('blue');
            $('.icon').addClass('icon--blue');
            $('.icon span').attr('data-glyph','check');
            break;
        case 'cancelado':
            event.target.classList.add('black');
            $('.icon').addClass('icon--black');
            $('.icon span').attr('data-glyph','ban');
            break;
        case 'eliminado':
            event.target.classList.add('red');
            $('.icon').addClass('icon--red');
            $('.icon span').attr('data-glyph','trash');
            break;
        default:
            // statements_def
            break;
    }
});

if($('#lightbox-aceptar').length){

    $('#guardar-lightbox').click(function(event) {
        /* Act on the event */
        event.preventDefault();
        $('#lightbox-aceptar').fadeIn('fast');
    });

    $('#cancel-lightbox').click(function(event) {
        /* Act on the event */
        event.preventDefault();
        $('#lightbox-aceptar').fadeOut('fast');
    });
}