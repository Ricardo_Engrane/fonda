module.exports = function(grunt){

	grunt.initConfig({
		jshint: {
			options:{
				esnext : true
			},
			files : 'js/front/main.js'
		},

		uglify: {	
			files: {
				'../js/scripts.min.js': [
					'../js/scripts.all.js'
				]
			}
		},

		concat: {
			dist : {
	            src: [
					'js/front/vendors/*.js',
					'js/front/lib/*.js',
					'js/front/main.js'
	            ],
	            dest: '../js/scripts.all.js'
			},
			admin : {
				src: [
					'js/admin/vendors/*.js',
					'js/admin/lib/*.js',
					'js/admin/admin.js'
				],
            	dest : '../js/admin.js'
			}
		},

		inline: {
			estilos: {
				options: {
					exts: ['php', 'html'],
					tag: '_inlinecss'
				},
				src: '../dist/templates/header.php'
			},
			scripts: {
				options: {
					exts: ['php', 'html'],
					tag: '_inlinejs'
				},
				src: '../dist/templates/footer.php'
			}
		},

		copy: {
			dist : {
				expand: true,
				flatten: false,
				cwd: '../',
				src: [
					'*.html',
					'*.php',
					'templates/*'
				],
				dest: '../dist/',
			},
			estilos: {
				expand: true,
				flatten: false,
				cwd: '../',
				src: [
					'css/*',
					'templates/header.php',
				],
				dest: '../dist/',
				options: {
					process: function (content, srcpath) {
						return content.replace(/css\/estilos\.min\.css/g, "\.\.\/css\/estilos\.min\.css\?\_inlinecss");
					}
				},	
			},
			scripts: {
				expand: true,
				flatten: false,
				cwd: '../',
				src: [
					'js/*',
					'templates/footer.php',
				],
				dest: '../dist/',
				options: {
					process: function (content, srcpath) {
						return content.replace(/js\/scripts\.all\.js/g, "\.\.\/js\/scripts\.min\.js\?\_inlinejs");
					}
				},	
			},
			assets : {
				expand: true,
				flatten: false,
				cwd: '../',
				src: ['assets/**'],
				dest: '../dist/'
			}
		},

		sprite:{
			all: {
				src: 'sprites/*.png',
				dest: '../assets/img/sprites.png',
				imgPath: '../../assets/img/sprites.png',
				destCss: 'sass/front/helpers/_mixin-sprt.scss',
				padding: 1,
				algorithm: 'binary-tree'
			}
		},

		sass: {
			dist : {
				options: {
					style: 'compressed',
				},
				files: {
					'../css/estilos.min.css': 'sass/front/main.scss'
				}
			},
			admin : {
				options : {
					style : 'compressed',
				},
				files : {
					'../css/admin.css' : 'sass/admin/admin.scss'
				}
			}
		},

		autoprefixer: {
			dist: {
				src: '../css/estilos.min.css'
			},
			admin : {
				src : '../css/admin.css'
			}
		},

		watch: {

			scriptsConfig: {
				files: ['Gruntfile.js','js/*.js','js/**/*.js'],
				tasks: ['jshint', 'concat'],
				options: {
					spawn: false,
				}
			},

			//scripts : {
			//	files : ['js/*.js','js/**/*.js'],
			//	tasks : ['jshint', 'concat']
			//},
			
			html: {
				files: ['../**/*.php', '../**/*.html'] //CodeIgniter
			},

			estilos: {
				files: ['sass/**/*.scss'],
				tasks: ['sass','autoprefixer'],
				options: {
					interrupt: true
				}
			},
			
			options: {
				livereload: true
			}

		},
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-spritesmith');
	grunt.loadNpmTasks('grunt-inline');
	grunt.loadNpmTasks('grunt-replace');

	grunt.registerTask('default', ['sass', 'autoprefixer', 'concat', 'jshint']);
	grunt.registerTask('build', ['default', 'uglify', 'copy:dist', 'copy:estilos', 'copy:scripts', 'copy:assets', 'inline']);
};