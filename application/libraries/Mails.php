<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mails
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
		// $this->ci->load->model('registro');
	}

	//formulario de contacto que le llega al asesor dependiendo de la sucursal
	public function mail_contacto($data)
	{
		$msg = '
		<!doctype html>
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
			<head><meta name="description" content="Fonda Mexicana">
				<!-- NAME: 1 COLUMN -->
				<!--[if gte mso 15]>
				<xml>
					<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
				<![endif]-->
				<meta charset="UTF-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<title>Nuevo mensaje de losalmendros.com</title>
				
				<style type="text/css">

				@font-face {
			      font-family: "AbsaraSansTF-Thin";
			      src: url("http://fondamexicana.com/assets/fonts/AbsaraSansTF-Medium.woff");
			    }

			    @font-face {
			      font-family: "AbsaraSansTF-Medium";
			      src: url("http://fondamexicana.com/assets/fonts/AbsaraSansTF-Thin.woff2");
			    }
						p{
							margin:10px 0;
							padding:0;
							color: #5d1d47;
						}
						table{
							border-collapse:collapse;
						}
						h1,h2,h3,h4,h5,h6{
							display:block;
							margin:0;
							padding:0;
							color: #5d1d47;
						}
						img,a img{
							border:0;
							height:auto;
							outline:none;
							text-decoration:none;
						}
						body,#bodyTable,#bodyCell{
							height:100%;
							margin:0;
							padding:0;
							width:100%;
							font-family: "AbsaraSansTF-Thin";
						}
						#outlook a{
							padding:0;
						}
						img{
							-ms-interpolation-mode:bicubic;
						}
						table{
							mso-table-lspace:0pt;
							mso-table-rspace:0pt;
						}
						.ReadMsgBody{
							width:100%;
						}
						.ExternalClass{
							width:100%;
						}
						p,a,li,td,blockquote{
							mso-line-height-rule:exactly;
						}
						a[href^=tel],a[href^=sms]{
							color:inherit;
							cursor:default;
							text-decoration:none;
						}
						p,a,li,td,body,table,blockquote{
							-ms-text-size-adjust:100%;
							-webkit-text-size-adjust:100%;
						}
						.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
							line-height:100%;
						}
						a[x-apple-data-detectors]{
							color:inherit !important;
							text-decoration:none !important;
							font-size:inherit !important;
							font-family:inherit !important;
							font-weight:inherit !important;
							line-height:inherit !important;
						}
						#bodyCell{
							padding:10px;
						}
						.templateContainer{
							max-width:600px !important;
						}
						a.mcnButton{
							display:block;
						}
						.mcnImage{
							vertical-align:bottom;
						}
						.mcnTextContent{
							word-break:break-word;
						}
						.mcnTextContent img{
							height:auto !important;
						}
						.mcnDividerBlock{
							table-layout:fixed !important;
						}
						body,#bodyTable{
							background-color:#FAFAFA;
						}
						#bodyCell{
							border-top:0;
						}
						.templateContainer{
							border:0;
						}
						h1{
							color:#202020;
							font-family:Helvetica;
							font-size:26px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h2{
							color:#202020;
							font-family:Helvetica;
							font-size:22px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h3{
							color:#202020;
							font-family:Helvetica;
							font-size:20px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h4{
							color: #5d1d47;
							font-family:inherit;
							font-size:18px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						#templatePreheader{
							background-color:#FAFAFA;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:9px;
						}
						#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
							color:#656565;
							font-family:Helvetica;
							font-size:12px;
							line-height:150%;
							text-align:left;
						}
						#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
							color:#656565;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateHeader{
							background-color:#FFFFFF;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:0;
						}
						#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
							color:#202020;
							font-family:Helvetica;
							font-size:16px;
							line-height:150%;
							text-align:left;
						}
						#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
							color:#2BAADF;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateBody{
							background-color:#FFFFFF;
							border-top:0;
							border-bottom:2px solid #EAEAEA;
							padding-top:0;
							padding-bottom:9px;
						}
						#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
							color:#202020;
							font-family:Helvetica;
							font-size:16px;
							line-height:150%;
							text-align:left;
						}
						#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
							color:#2BAADF;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateFooter{
							background-color:#FAFAFA;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:9px;
						}
						#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
							color:#656565;
							font-family:Helvetica;
							font-size:12px;
							line-height:150%;
							text-align:center;
						}
						#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
							color:#656565;
							font-weight:normal;
							text-decoration:underline;
						}


						@media only screen and (min-width:768px){
							.templateContainer{
								width:600px !important;
							}
						}	@media only screen and (max-width: 480px){
							body,table,td,p,a,li,blockquote{
								-webkit-text-size-adjust:none !important;
							}
						}	@media only screen and (max-width: 480px){
							body{
								width:100% !important;
								min-width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							#bodyCell{
								padding-top:10px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImage{
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
								max-width:100% !important;
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnBoxedTextContentContainer{
								min-width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupContent{
								padding:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
								padding-top:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
								padding-top:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardBottomImageContent{
								padding-bottom:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupBlockInner{
								padding-top:0 !important;
								padding-bottom:0 !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupBlockOuter{
								padding-top:9px !important;
								padding-bottom:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnTextContent,.mcnBoxedTextContentColumn{
								padding-right:18px !important;
								padding-left:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
								padding-right:18px !important;
								padding-bottom:0 !important;
								padding-left:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcpreview-image-uploader{
								display:none !important;
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							h1{
								font-size:22px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h2{
								font-size:20px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h3{
								font-size:18px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h4{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templatePreheader{
								display:block !important;
							}
						}	@media only screen and (max-width: 480px){
							#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}

						footer .wrap-footer{
								position: relative;
								width: 100%;
								height: 72px;
								background: #5d1d47;
								display: flex;
								justify-content: space-between;
								align-items: center;

								margin: 0 auto;
							}

							.logo-engrane
							{
								margin: 20px;
							}
						
							footer.logo-engrane,
								footer.legales {
								
										text-align: center;
										width: 100%;
										margin: 10px 0;
								}	
								.legales
								{
									text-align: right;
								}
							.legales p{
										display: inline;
										font-size: 12px;
										font-family: AbsaraSansTF-Thin;
										color: #fff;
										margin-right: 10px;
										text-align: right;
									}
								
				</style>
			</head>
			<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;"><div itemscope itemtype="http://schema.org/EmailMessage"><div itemprop="publisher" itemscope itemtype="http://schema.org/Organization"><meta itemprop="name" content="*|LIST:COMPANY|*"><link itemprop="url" content="*|LIST:URL|*"></div><div itemprop="about" itemscope itemtype="http://schema.org/Offer"><link itemprop="image" href="https://gallery.mailchimp.com/e55512039fb8fcbea8f1003e1/images/60ec81c5-4a49-4ab2-a074-54834093143d.jpg"></div></div>
				
				<div style="text-align:left; margin: 55px;">
					<h4> Se ha recibido un nuevo mensaje.<br>A continuación se muestran los datos:</h4>
					<br>
					<br>
					<p>Nombre: ' .$data['nombre'] . ' </p>
					<p>Correo electronico: ' . $data['correo']. '</p>
					<p>Comentario: ' . $data['comentario']. ' </p> 
				</div>
			
			<footer>
			<div class="wrap-footer">
				<div class="logo-engrane">
					<img style="width: 80%;" src="http://fondamexicana.com/assets/img/admin/logo_fonda_mexicana_blanco.png" alt="">
				</div>
			<div class="legales">
				<p style="font-size: 14px;"> http://fondamexicana.com/</p>
				<br>
				<p>© Restaurante Fonda Mexicana. 2018 Todos los derechos reservados.</p>
				
				
			</div>
		</div>
	</footer>
		</body>
		</html>	
		';
		
		/*
		if($data['sucursal'] == 'insurgentes')
		{
			$this->ci->email->to('ealmendrossur@cmr.mx');
		}
		if($data['sucursal'] == 'polanco')
		{
			$this->ci->email->to('ealmendrospol@cmr.mx');
		}
		*/
		
		// $this->ci->email->to('iescalona@cmr.mx');
		//$this->ci->email->to('alejandra@engranestudio.com');

		$this->ci->email->from('contacto@fondamexicana.com');
		$this->ci->email->to('fondapolanco@cmr.mx');
		$this->ci->email->cc('iescalona@cmr.mx, ricardo@engranestudio.com, alejandra@engranestudio.com');
		$this->ci->email->subject('Nuevo mensaje fondamexicana.com');
		$this->ci->email->message($msg);

		if($this->ci->email->send())
		{
			return true;
		}
		return false;

	}

	//mensaje que llega al asesor en caso de que un usuario use el servicio de reservas
	public function mail_asesor($data)
	{
		$msg = '
		<!doctype html>
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
			<head><meta name="description" content="Fonda Mexicana">
				<!-- NAME: 1 COLUMN -->
				<!--[if gte mso 15]>
				<xml>
					<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
				<![endif]-->
				<meta charset="UTF-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<title>Nuevo mensaje de fondamexicana.com</title>
				
				<style type="text/css">

				@font-face {
			      font-family: "AbsaraSansTF-Thin";
			      src: url("http://fondamexicana.com/assets/fonts/AbsaraSansTF-Medium.woff");
			    }

			    @font-face {
			      font-family: "AbsaraSansTF-Medium";
			      src: url("http://fondamexicana.com/assets/fonts/AbsaraSansTF-Thin.woff2");
			    }
						p{
							margin:10px 0;
							padding:0;
							color: #5d1d47;
						}
						table{
							border-collapse:collapse;
						}
						h1,h2,h3,h4,h5,h6{
							display:block;
							margin:0;
							padding:0;
							color: #5d1d47;
						}
						img,a img{
							border:0;
							height:auto;
							outline:none;
							text-decoration:none;
						}
						body,#bodyTable,#bodyCell{
							height:100%;
							margin:0;
							padding:0;
							width:100%;
							font-family: "AbsaraSansTF-Thin";
						}
						#outlook a{
							padding:0;
						}
						img{
							-ms-interpolation-mode:bicubic;
						}
						table{
							mso-table-lspace:0pt;
							mso-table-rspace:0pt;
						}
						.ReadMsgBody{
							width:100%;
						}
						.ExternalClass{
							width:100%;
						}
						p,a,li,td,blockquote{
							mso-line-height-rule:exactly;
						}
						a[href^=tel],a[href^=sms]{
							color:inherit;
							cursor:default;
							text-decoration:none;
						}
						p,a,li,td,body,table,blockquote{
							-ms-text-size-adjust:100%;
							-webkit-text-size-adjust:100%;
						}
						.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
							line-height:100%;
						}
						a[x-apple-data-detectors]{
							color:inherit !important;
							text-decoration:none !important;
							font-size:inherit !important;
							font-family:inherit !important;
							font-weight:inherit !important;
							line-height:inherit !important;
						}
						#bodyCell{
							padding:10px;
						}
						.templateContainer{
							max-width:600px !important;
						}
						a.mcnButton{
							display:block;
						}
						.mcnImage{
							vertical-align:bottom;
						}
						.mcnTextContent{
							word-break:break-word;
						}
						.mcnTextContent img{
							height:auto !important;
						}
						.mcnDividerBlock{
							table-layout:fixed !important;
						}
						body,#bodyTable{
							background-color:#FAFAFA;
						}
						#bodyCell{
							border-top:0;
						}
						.templateContainer{
							border:0;
						}
						h1{
							color:#202020;
							font-family:Helvetica;
							font-size:26px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h2{
							color:#202020;
							font-family:Helvetica;
							font-size:22px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h3{
							color:#202020;
							font-family:Helvetica;
							font-size:20px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h4{
							color: #5d1d47;
							font-family:inherit;
							font-size:18px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						#templatePreheader{
							background-color:#FAFAFA;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:9px;
						}
						#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
							color:#656565;
							font-family:Helvetica;
							font-size:12px;
							line-height:150%;
							text-align:left;
						}
						#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
							color:#656565;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateHeader{
							background-color:#FFFFFF;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:0;
						}
						#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
							color:#202020;
							font-family:Helvetica;
							font-size:16px;
							line-height:150%;
							text-align:left;
						}
						#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
							color:#2BAADF;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateBody{
							background-color:#FFFFFF;
							border-top:0;
							border-bottom:2px solid #EAEAEA;
							padding-top:0;
							padding-bottom:9px;
						}
						#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
							color:#202020;
							font-family:Helvetica;
							font-size:16px;
							line-height:150%;
							text-align:left;
						}
						#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
							color:#2BAADF;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateFooter{
							background-color:#FAFAFA;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:9px;
						}
						#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
							color:#656565;
							font-family:Helvetica;
							font-size:12px;
							line-height:150%;
							text-align:center;
						}
						#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
							color:#656565;
							font-weight:normal;
							text-decoration:underline;
						}


						@media only screen and (min-width:768px){
							.templateContainer{
								width:600px !important;
							}
						}	@media only screen and (max-width: 480px){
							body,table,td,p,a,li,blockquote{
								-webkit-text-size-adjust:none !important;
							}
						}	@media only screen and (max-width: 480px){
							body{
								width:100% !important;
								min-width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							#bodyCell{
								padding-top:10px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImage{
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
								max-width:100% !important;
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnBoxedTextContentContainer{
								min-width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupContent{
								padding:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
								padding-top:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
								padding-top:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardBottomImageContent{
								padding-bottom:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupBlockInner{
								padding-top:0 !important;
								padding-bottom:0 !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupBlockOuter{
								padding-top:9px !important;
								padding-bottom:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnTextContent,.mcnBoxedTextContentColumn{
								padding-right:18px !important;
								padding-left:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
								padding-right:18px !important;
								padding-bottom:0 !important;
								padding-left:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcpreview-image-uploader{
								display:none !important;
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							h1{
								font-size:22px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h2{
								font-size:20px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h3{
								font-size:18px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h4{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templatePreheader{
								display:block !important;
							}
						}	@media only screen and (max-width: 480px){
							#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}

						footer .wrap-footer{
								position: relative;
								width: 100%;
								height: 72px;
								background: #5d1d47;
								display: flex;
								justify-content: space-between;
								align-items: center;

								margin: 0 auto;
							}

							.logo-engrane
							{
								margin: 20px;
							}
						
							footer.logo-engrane,
								footer.legales {
								
										text-align: center;
										width: 100%;
										margin: 10px 0;
								}	
								.legales
								{
									text-align: right;
								}
							.legales p{
										display: inline;
										font-size: 12px;
										font-family: AbsaraSansTF-Thin;
										color: #fff;
										margin-right: 10px;
										text-align: right;
									}
								
				</style>
			</head>
			<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;"><div itemscope itemtype="http://schema.org/EmailMessage"><div itemprop="publisher" itemscope itemtype="http://schema.org/Organization"><meta itemprop="name" content="*|LIST:COMPANY|*"><link itemprop="url" content="*|LIST:URL|*"></div><div itemprop="about" itemscope itemtype="http://schema.org/Offer"><link itemprop="image" href="https://gallery.mailchimp.com/e55512039fb8fcbea8f1003e1/images/60ec81c5-4a49-4ab2-a074-54834093143d.jpg"></div></div>
				
				<div style="text-align:left; margin: 55px;">
					<p> Has recibibo una solicitud de evento.</p>
					<p>En breve deberas ponerte en contacto, para revisar la información del solicitante.</p>
					<br>
					<br>
					<br>
					<p>Ingresa a:</p>
					<a href="http://fondamexicana.com/admin/eventos/'.$data['id'].'">fondamexicana.com/admin/eventos/'.$data['id'].'</a>
				</div>
			
			<footer>
			<div class="wrap-footer">
				<div class="logo-engrane">
					<img style="width: 80%;" src="http://fondamexicana.com/assets/img/admin/logo_fonda_mexicana_blanco.png" alt="">
				</div>
			<div class="legales">
				<p style="font-size: 14px;"> http://fondamexicana.com/</p>
				<br>
				<p>© Restaurante Fonda Mexicana. 2018 Todos los derechos reservados.</p>
				
				
			</div>
		</div>
	</footer>
		</body>
		</html>		
		';

		

		
		// $this->ci->email->to('alejandra@engranestudio.com');
		//$this->ci->email->to('iescalona@cmr.mx');

		$this->ci->email->from('no-reply@fondamexicana.com');

		/*if($data['lugar'] == 'almendros insurgentes')
		{
			$this->ci->email->to('ealmendrossur@cmr.mx');
		}
		if($data['lugar'] == 'polanco')
		{
			$this->ci->email->to('ealmendrospol@cmr.mx');
		}*/

		//Este es el correo del asesor de fonda mexicana
		$this->ci->email->to('efondapol@cmr.mx');
		
		$this->ci->email->cc('ricardo@engranestudio.com, alejandra@engranestudio.com');
		$this->ci->email->subject('Solicitud de evento');
		$this->ci->email->message($msg);

		if($this->ci->email->send())
		{
			return true;
		}
		return false;

	}

	//correo de confirmacion que le llega al usuario cuando realiza la solicitud de reserva a traves del sitio
	public function mail_nueva_reserva($data)
	{
		$msg = '
		<!doctype html>
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
			<head><meta name="description" content="Fonda Mexicana">
				<!-- NAME: 1 COLUMN -->
				<!--[if gte mso 15]>
				<xml>
					<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
				<![endif]-->
				<meta charset="UTF-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<title>Nuevo mensaje de fondamexicana.com</title>
				
				<style type="text/css">

				@font-face {
			      font-family: "AbsaraSansTF-Thin";
			      src: url("http://fondamexicana.com/assets/fonts/AbsaraSansTF-Medium.woff");
			    }

			    @font-face {
			      font-family: "AbsaraSansTF-Medium";
			      src: url("https://fondamexicana.com/assets/fonts/AbsaraSansTF-Thin.woff2");
			    }
						p{
							margin:10px 0;
							padding:0;
							color: #5d1d47;
						}
						table{
							border-collapse:collapse;
						}
						h1,h2,h3,h4,h5,h6{
							display:block;
							margin:0;
							padding:0;
							color: #5d1d47;
						}
						img,a img{
							border:0;
							height:auto;
							outline:none;
							text-decoration:none;
						}
						body,#bodyTable,#bodyCell{
							height:100%;
							margin:0;
							padding:0;
							width:100%;
							font-family: "AbsaraSansTF-Thin";
						}
						#outlook a{
							padding:0;
						}
						img{
							-ms-interpolation-mode:bicubic;
						}
						table{
							mso-table-lspace:0pt;
							mso-table-rspace:0pt;
						}
						.ReadMsgBody{
							width:100%;
						}
						.ExternalClass{
							width:100%;
						}
						p,a,li,td,blockquote{
							mso-line-height-rule:exactly;
						}
						a[href^=tel],a[href^=sms]{
							color:inherit;
							cursor:default;
							text-decoration:none;
						}
						p,a,li,td,body,table,blockquote{
							-ms-text-size-adjust:100%;
							-webkit-text-size-adjust:100%;
						}
						.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
							line-height:100%;
						}
						a[x-apple-data-detectors]{
							color:inherit !important;
							text-decoration:none !important;
							font-size:inherit !important;
							font-family:inherit !important;
							font-weight:inherit !important;
							line-height:inherit !important;
						}
						#bodyCell{
							padding:10px;
						}
						.templateContainer{
							max-width:600px !important;
						}
						a.mcnButton{
							display:block;
						}
						.mcnImage{
							vertical-align:bottom;
						}
						.mcnTextContent{
							word-break:break-word;
						}
						.mcnTextContent img{
							height:auto !important;
						}
						.mcnDividerBlock{
							table-layout:fixed !important;
						}
						body,#bodyTable{
							background-color:#FAFAFA;
						}
						#bodyCell{
							border-top:0;
						}
						.templateContainer{
							border:0;
						}
						h1{
							color:#202020;
							font-family:Helvetica;
							font-size:26px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h2{
							color:#202020;
							font-family:Helvetica;
							font-size:22px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h3{
							color:#202020;
							font-family:Helvetica;
							font-size:20px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h4{
							color: #5d1d47;
							font-family:inherit;
							font-size:18px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						#templatePreheader{
							background-color:#FAFAFA;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:9px;
						}
						#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
							color:#656565;
							font-family:Helvetica;
							font-size:12px;
							line-height:150%;
							text-align:left;
						}
						#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
							color:#656565;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateHeader{
							background-color:#FFFFFF;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:0;
						}
						#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
							color:#202020;
							font-family:Helvetica;
							font-size:16px;
							line-height:150%;
							text-align:left;
						}
						#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
							color:#2BAADF;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateBody{
							background-color:#FFFFFF;
							border-top:0;
							border-bottom:2px solid #EAEAEA;
							padding-top:0;
							padding-bottom:9px;
						}
						#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
							color:#202020;
							font-family:Helvetica;
							font-size:16px;
							line-height:150%;
							text-align:left;
						}
						#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
							color:#2BAADF;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateFooter{
							background-color:#FAFAFA;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:9px;
						}
						#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
							color:#656565;
							font-family:Helvetica;
							font-size:12px;
							line-height:150%;
							text-align:center;
						}
						#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
							color:#656565;
							font-weight:normal;
							text-decoration:underline;
						}


						@media only screen and (min-width:768px){
							.templateContainer{
								width:600px !important;
							}
						}	@media only screen and (max-width: 480px){
							body,table,td,p,a,li,blockquote{
								-webkit-text-size-adjust:none !important;
							}
						}	@media only screen and (max-width: 480px){
							body{
								width:100% !important;
								min-width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							#bodyCell{
								padding-top:10px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImage{
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
								max-width:100% !important;
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnBoxedTextContentContainer{
								min-width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupContent{
								padding:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
								padding-top:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
								padding-top:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardBottomImageContent{
								padding-bottom:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupBlockInner{
								padding-top:0 !important;
								padding-bottom:0 !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupBlockOuter{
								padding-top:9px !important;
								padding-bottom:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnTextContent,.mcnBoxedTextContentColumn{
								padding-right:18px !important;
								padding-left:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
								padding-right:18px !important;
								padding-bottom:0 !important;
								padding-left:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcpreview-image-uploader{
								display:none !important;
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							h1{
								font-size:22px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h2{
								font-size:20px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h3{
								font-size:18px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h4{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templatePreheader{
								display:block !important;
							}
						}	@media only screen and (max-width: 480px){
							#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}

						footer .wrap-footer{
								position: relative;
								width: 100%;
								height: 72px;
								background: #5d1d47;
								display: flex;
								justify-content: space-between;
								align-items: center;

								margin: 0 auto;
							}

							.logo-engrane
							{
								margin: 20px;
							}
						
							footer.logo-engrane,
								footer.legales {
								
										text-align: center;
										width: 100%;
										margin: 10px 0;
								}	
								.legales
								{
									text-align: right;
								}
							.legales p{
										display: inline;
										font-size: 12px;
										font-family: AbsaraSansTF-Thin;
										color: #fff;
										margin-right: 10px;
										text-align: right;
									}
								
				</style>
			</head>
			<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;">
				<div itemscope itemtype="http://schema.org/EmailMessage">
					<div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
						<meta itemprop="name" content="">
						<link itemprop="url" content="">
					</div>
					<div itemprop="about" itemscope itemtype="http://schema.org/Offer">
						<link itemprop="image" href="">
					</div>
				</div>
				
				<div style="text-align:left; margin: 55px;">
					<p> Hola. <br>La solicitud de tu evento ha sido recibida exitosamente, muy pronto nos pondremos en contacto contigo.</p>
					<br>
					<br>
					<h4>Datos de solicitud:</h4>
					<p>Nombre: ' .$data['nombre'] . '</p>
					<p>Telefono: ' .$data['telefono']. '</p>
					<p>Correo: ' .$data['email']. '</p>
					<p>Salón: '.ucwords($data['salon']).'</p>
					<p>Tipo de evento: ' .$data['tipo_evento'].'</p>
					<p>Fecha de reserva: '.$data['fecha'].' </p>
					<p>Hora de reserva: '.$data['hora_ini'].' h. - ' .$data['hora_fin']. ' h.</p>
					<br>
					<br>
					<p>Si tienes alguna duda comunicate con tu asesor:
						<br>'.$data['asesor_nombre'].'
						<br>'.$data['asesor_mail'].'
						'.$data['asesor_tel'].'
					</p>
					<p>Te recomendamos visitar la siguiente direccion: <a href="#">fondamexicana.com/nuestras-opciones</a>
					<br>para conocer nuestras opciones de platillos, equipo y todo el servicio para tu evento.</p>
					<br>
					<div>
						<h4>Agradecemos tu preferencia</h4>
					</div>
				</div>
				<footer>
					<div class="wrap-footer">
						<div class="logo-engrane">
							<img style="width: 80%;" src="http://fondamexicana.com/assets/img/admin/logo_fonda_mexicana_blanco.png" alt="">
						</div>
						<div class="legales">
							<p style="font-size: 14px;"> fondamexicana.com</p>
							<br>
							<p>© Restaurante Fonda Mexicana. 2018 Todos los derechos reservados.</p>
						</div>
					</div>
				</footer>
		</body>
		</html>		
		';

		$this->ci->email->from('no-reply@fondamexicana.com');	
		//$this->ci->email->to('alejandra@engranestudio.com');
		$this->ci->email->to($data['email']);
		$this->ci->email->cc('ricardo@engranestudio.com, alejandra@engranestudio.com');
		$this->ci->email->subject('Solicitud de evento Fonda Mexicana');
		$this->ci->email->message($msg);

		if($this->ci->email->send())
		{
			return true;
		}
		return false;


	}


	//correo que le llega al asesor al momento que confirmo un evento
	public function mail_confirm($data)
	{
		$msg = '
		 <!doctype html>
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
			<head><meta name="description" content="Fonda Mexicana">
				<!-- NAME: 1 COLUMN -->
				<!--[if gte mso 15]>
				<xml>
					<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
				<![endif]-->
				<meta charset="UTF-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<title>Nuevo mensaje de fondamexicana.com</title>
				
				<style type="text/css">

				@font-face {
			      font-family: "AbsaraSansTF-Thin";
			      src: url("http://fondamexicana.com/assets/fonts/AbsaraSansTF-Medium.woff");
			    }

			    @font-face {
			      font-family: "AbsaraSansTF-Medium";
			      src: url("https://fondamexicana.com/assets/fonts/AbsaraSansTF-Thin.woff2");
			    }
						p{
							margin:10px 0;
							padding:0;
							color: #5d1d47;
						}
						table{
							border-collapse:collapse;
						}
						h1,h2,h3,h4,h5,h6{
							display:block;
							margin:0;
							padding:0;
							color: #5d1d47;
						}
						img,a img{
							border:0;
							height:auto;
							outline:none;
							text-decoration:none;
						}
						body,#bodyTable,#bodyCell{
							height:100%;
							margin:0;
							padding:0;
							width:100%;
							font-family: "AbsaraSansTF-Thin";
						}
						#outlook a{
							padding:0;
						}
						img{
							-ms-interpolation-mode:bicubic;
						}
						table{
							mso-table-lspace:0pt;
							mso-table-rspace:0pt;
						}
						.ReadMsgBody{
							width:100%;
						}
						.ExternalClass{
							width:100%;
						}
						p,a,li,td,blockquote{
							mso-line-height-rule:exactly;
						}
						a[href^=tel],a[href^=sms]{
							color:inherit;
							cursor:default;
							text-decoration:none;
						}
						p,a,li,td,body,table,blockquote{
							-ms-text-size-adjust:100%;
							-webkit-text-size-adjust:100%;
						}
						.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
							line-height:100%;
						}
						a[x-apple-data-detectors]{
							color:inherit !important;
							text-decoration:none !important;
							font-size:inherit !important;
							font-family:inherit !important;
							font-weight:inherit !important;
							line-height:inherit !important;
						}
						#bodyCell{
							padding:10px;
						}
						.templateContainer{
							max-width:600px !important;
						}
						a.mcnButton{
							display:block;
						}
						.mcnImage{
							vertical-align:bottom;
						}
						.mcnTextContent{
							word-break:break-word;
						}
						.mcnTextContent img{
							height:auto !important;
						}
						.mcnDividerBlock{
							table-layout:fixed !important;
						}
						body,#bodyTable{
							background-color:#FAFAFA;
						}
						#bodyCell{
							border-top:0;
						}
						.templateContainer{
							border:0;
						}
						h1{
							color:#202020;
							font-family:Helvetica;
							font-size:26px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h2{
							color:#202020;
							font-family:Helvetica;
							font-size:22px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h3{
							color:#202020;
							font-family:Helvetica;
							font-size:20px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h4{
							color: #5d1d47;
							font-family:inherit;
							font-size:18px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						#templatePreheader{
							background-color:#FAFAFA;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:9px;
						}
						#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
							color:#656565;
							font-family:Helvetica;
							font-size:12px;
							line-height:150%;
							text-align:left;
						}
						#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
							color:#656565;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateHeader{
							background-color:#FFFFFF;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:0;
						}
						#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
							color:#202020;
							font-family:Helvetica;
							font-size:16px;
							line-height:150%;
							text-align:left;
						}
						#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
							color:#2BAADF;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateBody{
							background-color:#FFFFFF;
							border-top:0;
							border-bottom:2px solid #EAEAEA;
							padding-top:0;
							padding-bottom:9px;
						}
						#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
							color:#202020;
							font-family:Helvetica;
							font-size:16px;
							line-height:150%;
							text-align:left;
						}
						#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
							color:#2BAADF;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateFooter{
							background-color:#FAFAFA;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:9px;
						}
						#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
							color:#656565;
							font-family:Helvetica;
							font-size:12px;
							line-height:150%;
							text-align:center;
						}
						#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
							color:#656565;
							font-weight:normal;
							text-decoration:underline;
						}


						@media only screen and (min-width:768px){
							.templateContainer{
								width:600px !important;
							}
						}	@media only screen and (max-width: 480px){
							body,table,td,p,a,li,blockquote{
								-webkit-text-size-adjust:none !important;
							}
						}	@media only screen and (max-width: 480px){
							body{
								width:100% !important;
								min-width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							#bodyCell{
								padding-top:10px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImage{
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
								max-width:100% !important;
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnBoxedTextContentContainer{
								min-width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupContent{
								padding:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
								padding-top:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
								padding-top:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardBottomImageContent{
								padding-bottom:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupBlockInner{
								padding-top:0 !important;
								padding-bottom:0 !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupBlockOuter{
								padding-top:9px !important;
								padding-bottom:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnTextContent,.mcnBoxedTextContentColumn{
								padding-right:18px !important;
								padding-left:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
								padding-right:18px !important;
								padding-bottom:0 !important;
								padding-left:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcpreview-image-uploader{
								display:none !important;
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							h1{
								font-size:22px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h2{
								font-size:20px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h3{
								font-size:18px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h4{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templatePreheader{
								display:block !important;
							}
						}	@media only screen and (max-width: 480px){
							#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}

						footer .wrap-footer{
								position: relative;
								width: 100%;
								height: 72px;
								background: #5d1d47;
								display: flex;
								justify-content: space-between;
								align-items: center;

								margin: 0 auto;
							}

							.logo-engrane
							{
								margin: 20px;
							}
						
							footer.logo-engrane,
								footer.legales {
								
										text-align: center;
										width: 100%;
										margin: 10px 0;
								}	
								.legales
								{
									text-align: right;
								}
							.legales p{
										display: inline;
										font-size: 12px;
										font-family: AbsaraSansTF-Thin;
										color: #fff;
										margin-right: 10px;
										text-align: right;
									}
								
				</style>
			</head>
			<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;"><div itemscope itemtype="http://schema.org/EmailMessage"><div itemprop="publisher" itemscope itemtype="http://schema.org/Organization"><meta itemprop="name" content="*|LIST:COMPANY|*"><link itemprop="url" content="*|LIST:URL|*"></div><div itemprop="about" itemscope itemtype="http://schema.org/Offer"><link itemprop="image" href="https://gallery.mailchimp.com/e55512039fb8fcbea8f1003e1/images/60ec81c5-4a49-4ab2-a074-54834093143d.jpg"></div></div>
				
				<div style="text-align:left; margin: 55px;">
					<p> Has realizado la confirmacion de una reserva con el folio ' .$data['folio'] .'. A continuacion se muestran los datos de dicha reserva: </p>
																			<br>
																			<br>
																			<br>
																			<p>Nombre: ' .$data['nombre'] . ' </p>
																			<p>Fecha de reserva: '.$data['fecha'].' </p>
																			<p>Hora de reserva:'.$data['hora_ini'].' </p>
																			<p>Salon:'.$data['salon'].' </p>
				</div>
			
			<footer>
			<div class="wrap-footer">
				<div class="logo-engrane">
					<img style="width: 80%;" src="http://fondamexicana.com/assets/img/admin/logo_fonda_mexicana_blanco.png" alt="">
				</div>
			<div class="legales">
				<p style="font-size: 14px;"> fondamexicana.com</p>
				<br>
				<p>© Restaurante Fonda Mexicana. 2018 Todos los derechos reservados.</p>
				
				
			</div>
		</div>
	</footer>
		</body>
		</html>		
		';
		$this->ci->email->from('no-reply@fondamexicana.com');
		$this->ci->email->to($data['correo_responsable']);
		//$this->ci->email->to('iescalona@cmr.mx');
		$this->ci->email->cc('ricardo@engranestudio.com, alejandra@engranestudio.com');
		$this->ci->email->subject('Fonda Mexicana - Confirmacion de reserva');
		$this->ci->email->message($msg);

		if($this->ci->email->send())
		{
			return true;
		}
		return false;

	}

	//correo que le llega al usuario luego de haber confirmado un evento junto al asesor
	public function mail_to_contact($data)
	{
		$msg = '
		<!doctype html>
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
			<head><meta name="description" content="Fonda Mexicana">
				<!-- NAME: 1 COLUMN -->
				<!--[if gte mso 15]>
				<xml>
					<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
				<![endif]-->
				<meta charset="UTF-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<title>Nuevo mensaje de fondamexicana.com</title>
				
				<style type="text/css">

				@font-face {
			      font-family: "AbsaraSansTF-Thin";
			      src: url("http://fondamexicana.com/assets/fonts/AbsaraSansTF-Medium.woff");
			    }

			    @font-face {
			      font-family: "AbsaraSansTF-Medium";
			      src: url("http://fondamexicana.com/assets/fonts/AbsaraSansTF-Thin.woff2");
			    }
						p{
							margin:10px 0;
							padding:0;
							color: #5d1d47;
						}
						table{
							border-collapse:collapse;
						}
						h1,h2,h3,h4,h5,h6{
							display:block;
							margin:0;
							padding:0;
							color: #5d1d47;
						}
						img,a img{
							border:0;
							height:auto;
							outline:none;
							text-decoration:none;
						}
						body,#bodyTable,#bodyCell{
							height:100%;
							margin:0;
							padding:0;
							width:100%;
							font-family: "AbsaraSansTF-Thin";
						}
						#outlook a{
							padding:0;
						}
						img{
							-ms-interpolation-mode:bicubic;
						}
						table{
							mso-table-lspace:0pt;
							mso-table-rspace:0pt;
						}
						.ReadMsgBody{
							width:100%;
						}
						.ExternalClass{
							width:100%;
						}
						p,a,li,td,blockquote{
							mso-line-height-rule:exactly;
						}
						a[href^=tel],a[href^=sms]{
							color:inherit;
							cursor:default;
							text-decoration:none;
						}
						p,a,li,td,body,table,blockquote{
							-ms-text-size-adjust:100%;
							-webkit-text-size-adjust:100%;
						}
						.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
							line-height:100%;
						}
						a[x-apple-data-detectors]{
							color:inherit !important;
							text-decoration:none !important;
							font-size:inherit !important;
							font-family:inherit !important;
							font-weight:inherit !important;
							line-height:inherit !important;
						}
						#bodyCell{
							padding:10px;
						}
						.templateContainer{
							max-width:600px !important;
						}
						a.mcnButton{
							display:block;
						}
						.mcnImage{
							vertical-align:bottom;
						}
						.mcnTextContent{
							word-break:break-word;
						}
						.mcnTextContent img{
							height:auto !important;
						}
						.mcnDividerBlock{
							table-layout:fixed !important;
						}
						body,#bodyTable{
							background-color:#FAFAFA;
						}
						#bodyCell{
							border-top:0;
						}
						.templateContainer{
							border:0;
						}
						h1{
							color:#202020;
							font-family:Helvetica;
							font-size:26px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h2{
							color:#202020;
							font-family:Helvetica;
							font-size:22px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h3{
							color:#202020;
							font-family:Helvetica;
							font-size:20px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h4{
							color: #5d1d47;
							font-family:inherit;
							font-size:18px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						#templatePreheader{
							background-color:#FAFAFA;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:9px;
						}
						#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
							color:#656565;
							font-family:Helvetica;
							font-size:12px;
							line-height:150%;
							text-align:left;
						}
						#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
							color:#656565;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateHeader{
							background-color:#FFFFFF;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:0;
						}
						#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
							color:#202020;
							font-family:Helvetica;
							font-size:16px;
							line-height:150%;
							text-align:left;
						}
						#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
							color:#2BAADF;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateBody{
							background-color:#FFFFFF;
							border-top:0;
							border-bottom:2px solid #EAEAEA;
							padding-top:0;
							padding-bottom:9px;
						}
						#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
							color:#202020;
							font-family:Helvetica;
							font-size:16px;
							line-height:150%;
							text-align:left;
						}
						#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
							color:#2BAADF;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateFooter{
							background-color:#FAFAFA;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:9px;
						}
						#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
							color:#656565;
							font-family:Helvetica;
							font-size:12px;
							line-height:150%;
							text-align:center;
						}
						#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
							color:#656565;
							font-weight:normal;
							text-decoration:underline;
						}


						@media only screen and (min-width:768px){
							.templateContainer{
								width:600px !important;
							}
						}	@media only screen and (max-width: 480px){
							body,table,td,p,a,li,blockquote{
								-webkit-text-size-adjust:none !important;
							}
						}	@media only screen and (max-width: 480px){
							body{
								width:100% !important;
								min-width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							#bodyCell{
								padding-top:10px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImage{
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
								max-width:100% !important;
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnBoxedTextContentContainer{
								min-width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupContent{
								padding:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
								padding-top:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
								padding-top:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardBottomImageContent{
								padding-bottom:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupBlockInner{
								padding-top:0 !important;
								padding-bottom:0 !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupBlockOuter{
								padding-top:9px !important;
								padding-bottom:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnTextContent,.mcnBoxedTextContentColumn{
								padding-right:18px !important;
								padding-left:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
								padding-right:18px !important;
								padding-bottom:0 !important;
								padding-left:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcpreview-image-uploader{
								display:none !important;
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							h1{
								font-size:22px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h2{
								font-size:20px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h3{
								font-size:18px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h4{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templatePreheader{
								display:block !important;
							}
						}	@media only screen and (max-width: 480px){
							#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}

						footer .wrap-footer{
								position: relative;
								width: 100%;
								height: 72px;
								background: #5d1d47;
								display: flex;
								justify-content: space-between;
								align-items: center;

								margin: 0 auto;
							}

							.logo-engrane
							{
								margin: 20px;
							}
						
							footer.logo-engrane,
								footer.legales {
								
										text-align: center;
										width: 100%;
										margin: 10px 0;
								}	
								.legales
								{
									text-align: right;
								}
							.legales p{
										display: inline;
										font-size: 12px;
										font-family: AbsaraSansTF-Thin;
										color: #fff;
										margin-right: 10px;
										text-align: right;
									}
								
				</style>
			</head>
			<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;"><div itemscope itemtype="http://schema.org/EmailMessage"><div itemprop="publisher" itemscope itemtype="http://schema.org/Organization"><meta itemprop="name" content="*|LIST:COMPANY|*"><link itemprop="url" content="*|LIST:URL|*"></div><div itemprop="about" itemscope itemtype="http://schema.org/Offer"><link itemprop="image" href="https://gallery.mailchimp.com/e55512039fb8fcbea8f1003e1/images/60ec81c5-4a49-4ab2-a074-54834093143d.jpg"></div></div>
				
				<div style="text-align:left; margin: 55px;">
					<p> La reservación de evento ha sido confirmada exitosamente con el folio ' .$data['folio'] .' con los siguientes datos: </p>
																			<br>
																			<br>
																			<h4> Datos de solicitud </h4>
																			<br>
																			<p>Nombre: ' .$data['nombre'] . ' </p>
																			<p>Fecha de reserva: '.$data['fecha'].' </p>
																			<p>Hora de reserva:'.$data['hora_ini'].' </p>
																			<p>Salon:'.$data['salon'].' </p>

																			<br>
																			<br>
																			<br>
																			<p> <strong> Si tienes alguna duda comunícate con tu asesor </strong> </p>
																			<p> Nombre de la persona responsable de tu reserva:' .$data['nombre_responsable'] . '</p>
																			<p> Correo electronico de responsable: ' . $data['correo_responsable']. '</p>
																			<br>
																			<br>
																			<br>
																			<h2> AGRADECEMOS TU PREFERENCIA </h2>
				</div>
			
			<footer>
			<div class="wrap-footer">
				<div class="logo-engrane">
					<img style="width: 80%;" src="http://fondamexicana.com/assets/img/admin/logo_fonda_mexicana_blanco.png" alt="">
				</div>
			<div class="legales">
				<p style="font-size: 14px;"> fondamexicana.com</p>
				<br>
				<p>© Restaurantes Fonda Mexicana. 2018 Todos los derechos reservados.</p>
				
				
			</div>
		</div>
	</footer>
		</body>
		</html>	
		';
		$this->ci->email->from('no-reply@fondamexicana.com');
		//$this->ci->email->to('alejandra@engranestudio.com');
		$this->ci->email->to($data['email']);
		$this->ci->email->cc('ricardo@engranestudio.com, alejandra@engranestudio.com');
		$this->ci->email->subject('Fonda Mexicana');
		$this->ci->email->message($msg);

		if($this->ci->email->send())
		{
			return true;
		}
		return false;

	}

	//mail que llega al usuario tras cancelar su evento
	public function mail_cancel($data)
	{
		$msg = '
		<!doctype html>
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
			<head><meta name="description" content="Fonda Mexicana">
				<!-- NAME: 1 COLUMN -->
				<!--[if gte mso 15]>
				<xml>
					<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
				<![endif]-->
				<meta charset="UTF-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<title>Nuevo mensaje de fondamexicana.com</title>
				
				<style type="text/css">

				@font-face {
			      font-family: "AbsaraSansTF-Thin";
			      src: url("http://fondamexicana.com/assets/fonts/AbsaraSansTF-Medium.woff");
			    }

			    @font-face {
			      font-family: "AbsaraSansTF-Medium";
			      src: url("http://fondamexicana.com/assets/fonts/AbsaraSansTF-Thin.woff2");
			    }
						p{
							margin:10px 0;
							padding:0;
							color: #5d1d47;
						}
						table{
							border-collapse:collapse;
						}
						h1,h2,h3,h4,h5,h6{
							display:block;
							margin:0;
							padding:0;
							color: #5d1d47;
						}
						img,a img{
							border:0;
							height:auto;
							outline:none;
							text-decoration:none;
						}
						body,#bodyTable,#bodyCell{
							height:100%;
							margin:0;
							padding:0;
							width:100%;
							font-family: "AbsaraSansTF-Thin";
						}
						#outlook a{
							padding:0;
						}
						img{
							-ms-interpolation-mode:bicubic;
						}
						table{
							mso-table-lspace:0pt;
							mso-table-rspace:0pt;
						}
						.ReadMsgBody{
							width:100%;
						}
						.ExternalClass{
							width:100%;
						}
						p,a,li,td,blockquote{
							mso-line-height-rule:exactly;
						}
						a[href^=tel],a[href^=sms]{
							color:inherit;
							cursor:default;
							text-decoration:none;
						}
						p,a,li,td,body,table,blockquote{
							-ms-text-size-adjust:100%;
							-webkit-text-size-adjust:100%;
						}
						.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
							line-height:100%;
						}
						a[x-apple-data-detectors]{
							color:inherit !important;
							text-decoration:none !important;
							font-size:inherit !important;
							font-family:inherit !important;
							font-weight:inherit !important;
							line-height:inherit !important;
						}
						#bodyCell{
							padding:10px;
						}
						.templateContainer{
							max-width:600px !important;
						}
						a.mcnButton{
							display:block;
						}
						.mcnImage{
							vertical-align:bottom;
						}
						.mcnTextContent{
							word-break:break-word;
						}
						.mcnTextContent img{
							height:auto !important;
						}
						.mcnDividerBlock{
							table-layout:fixed !important;
						}
						body,#bodyTable{
							background-color:#FAFAFA;
						}
						#bodyCell{
							border-top:0;
						}
						.templateContainer{
							border:0;
						}
						h1{
							color:#202020;
							font-family:Helvetica;
							font-size:26px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h2{
							color:#202020;
							font-family:Helvetica;
							font-size:22px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h3{
							color:#202020;
							font-family:Helvetica;
							font-size:20px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h4{
							color: #5d1d47;
							font-family:inherit;
							font-size:18px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						#templatePreheader{
							background-color:#FAFAFA;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:9px;
						}
						#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
							color:#656565;
							font-family:Helvetica;
							font-size:12px;
							line-height:150%;
							text-align:left;
						}
						#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
							color:#656565;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateHeader{
							background-color:#FFFFFF;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:0;
						}
						#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
							color:#202020;
							font-family:Helvetica;
							font-size:16px;
							line-height:150%;
							text-align:left;
						}
						#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
							color:#2BAADF;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateBody{
							background-color:#FFFFFF;
							border-top:0;
							border-bottom:2px solid #EAEAEA;
							padding-top:0;
							padding-bottom:9px;
						}
						#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
							color:#202020;
							font-family:Helvetica;
							font-size:16px;
							line-height:150%;
							text-align:left;
						}
						#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
							color:#2BAADF;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateFooter{
							background-color:#FAFAFA;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:9px;
						}
						#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
							color:#656565;
							font-family:Helvetica;
							font-size:12px;
							line-height:150%;
							text-align:center;
						}
						#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
							color:#656565;
							font-weight:normal;
							text-decoration:underline;
						}


						@media only screen and (min-width:768px){
							.templateContainer{
								width:600px !important;
							}
						}	@media only screen and (max-width: 480px){
							body,table,td,p,a,li,blockquote{
								-webkit-text-size-adjust:none !important;
							}
						}	@media only screen and (max-width: 480px){
							body{
								width:100% !important;
								min-width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							#bodyCell{
								padding-top:10px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImage{
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
								max-width:100% !important;
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnBoxedTextContentContainer{
								min-width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupContent{
								padding:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
								padding-top:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
								padding-top:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardBottomImageContent{
								padding-bottom:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupBlockInner{
								padding-top:0 !important;
								padding-bottom:0 !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupBlockOuter{
								padding-top:9px !important;
								padding-bottom:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnTextContent,.mcnBoxedTextContentColumn{
								padding-right:18px !important;
								padding-left:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
								padding-right:18px !important;
								padding-bottom:0 !important;
								padding-left:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcpreview-image-uploader{
								display:none !important;
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							h1{
								font-size:22px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h2{
								font-size:20px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h3{
								font-size:18px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h4{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templatePreheader{
								display:block !important;
							}
						}	@media only screen and (max-width: 480px){
							#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}

						footer .wrap-footer{
								position: relative;
								width: 100%;
								height: 72px;
								background: #5d1d47;
								display: flex;
								justify-content: space-between;
								align-items: center;

								margin: 0 auto;
							}

							.logo-engrane
							{
								margin: 20px;
							}
						
							footer.logo-engrane,
								footer.legales {
								
										text-align: center;
										width: 100%;
										margin: 10px 0;
								}	
								.legales
								{
									text-align: right;
								}
							.legales p{
										display: inline;
										font-size: 12px;
										font-family: AbsaraSansTF-Thin;
										color: #fff;
										margin-right: 10px;
										text-align: right;
									}
								
				</style>
			</head>
			<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;"><div itemscope itemtype="http://schema.org/EmailMessage"><div itemprop="publisher" itemscope itemtype="http://schema.org/Organization"><meta itemprop="name" content="*|LIST:COMPANY|*"><link itemprop="url" content="*|LIST:URL|*"></div><div itemprop="about" itemscope itemtype="http://schema.org/Offer"><link itemprop="image" href="https://gallery.mailchimp.com/e55512039fb8fcbea8f1003e1/images/60ec81c5-4a49-4ab2-a074-54834093143d.jpg"></div></div>
				
				<div style="text-align:left; margin: 55px;">
					<p> Tu reserva se ha cancelado con exito. </p>
				</div>
			
			<footer>
			<div class="wrap-footer">
				<div class="logo-engrane">
					<img style="width: 80%;" src="http://fondamexicana.com/assets/img/admin/logo_fonda_mexicana_blanco.png" alt="">
				</div>
			<div class="legales">
				<p style="font-size: 14px;"> fondamexicana.com</p>
				<br>
				<p>© Restaurantes Fonda Mexicana. 2018 Todos los derechos reservados.</p>
				
				
			</div>
		</div>
	</footer>
		</body>
		</html>	
		';
		$this->ci->email->from('no-reply@fondamexicana.com');
		$this->ci->email->to($data['email']);
		//$this->ci->email->to('alejandra@engranestudio.com');
		$this->ci->email->cc('ricardo@engranestudio.com, alejandra@engranestudio.com');
		$this->ci->email->subject('Fonda Mexicana - Cancelacion de reserva');
		$this->ci->email->message($msg);

		if($this->ci->email->send())
		{
			return true;
		}
		return false;

	}

	//mensaje que le llega al asesor tras cancelar un evento
	public function mail_cancel_admin($data)
	{
		$msg = '
		<!doctype html>
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
			<head><meta name="description" content="Fonda Mexicana">
				<!-- NAME: 1 COLUMN -->
				<!--[if gte mso 15]>
				<xml>
					<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
				<![endif]-->
				<meta charset="UTF-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<title>Nuevo mensaje de fondamexicana.com</title>
				
				<style type="text/css">

				@font-face {
			      font-family: "AbsaraSansTF-Thin";
			      src: url("http://fondamexicana.com/assets/fonts/AbsaraSansTF-Medium.woff");
			    }

			    @font-face {
			      font-family: "AbsaraSansTF-Medium";
			      src: url("http://fondamexicana.com/assets/fonts/AbsaraSansTF-Thin.woff2");
			    }
						p{
							margin:10px 0;
							padding:0;
							color: #5d1d47;
						}
						table{
							border-collapse:collapse;
						}
						h1,h2,h3,h4,h5,h6{
							display:block;
							margin:0;
							padding:0;
							color: #5d1d47;
						}
						img,a img{
							border:0;
							height:auto;
							outline:none;
							text-decoration:none;
						}
						body,#bodyTable,#bodyCell{
							height:100%;
							margin:0;
							padding:0;
							width:100%;
							font-family: "AbsaraSansTF-Thin";
						}
						#outlook a{
							padding:0;
						}
						img{
							-ms-interpolation-mode:bicubic;
						}
						table{
							mso-table-lspace:0pt;
							mso-table-rspace:0pt;
						}
						.ReadMsgBody{
							width:100%;
						}
						.ExternalClass{
							width:100%;
						}
						p,a,li,td,blockquote{
							mso-line-height-rule:exactly;
						}
						a[href^=tel],a[href^=sms]{
							color:inherit;
							cursor:default;
							text-decoration:none;
						}
						p,a,li,td,body,table,blockquote{
							-ms-text-size-adjust:100%;
							-webkit-text-size-adjust:100%;
						}
						.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
							line-height:100%;
						}
						a[x-apple-data-detectors]{
							color:inherit !important;
							text-decoration:none !important;
							font-size:inherit !important;
							font-family:inherit !important;
							font-weight:inherit !important;
							line-height:inherit !important;
						}
						#bodyCell{
							padding:10px;
						}
						.templateContainer{
							max-width:600px !important;
						}
						a.mcnButton{
							display:block;
						}
						.mcnImage{
							vertical-align:bottom;
						}
						.mcnTextContent{
							word-break:break-word;
						}
						.mcnTextContent img{
							height:auto !important;
						}
						.mcnDividerBlock{
							table-layout:fixed !important;
						}
						body,#bodyTable{
							background-color:#FAFAFA;
						}
						#bodyCell{
							border-top:0;
						}
						.templateContainer{
							border:0;
						}
						h1{
							color:#202020;
							font-family:Helvetica;
							font-size:26px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h2{
							color:#202020;
							font-family:Helvetica;
							font-size:22px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h3{
							color:#202020;
							font-family:Helvetica;
							font-size:20px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						h4{
							color: #5d1d47;
							font-family:inherit;
							font-size:18px;
							font-style:normal;
							font-weight:bold;
							line-height:125%;
							letter-spacing:normal;
							text-align:left;
						}
						#templatePreheader{
							background-color:#FAFAFA;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:9px;
						}
						#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
							color:#656565;
							font-family:Helvetica;
							font-size:12px;
							line-height:150%;
							text-align:left;
						}
						#templatePreheader .mcnTextContent a,#templatePreheader .mcnTextContent p a{
							color:#656565;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateHeader{
							background-color:#FFFFFF;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:0;
						}
						#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
							color:#202020;
							font-family:Helvetica;
							font-size:16px;
							line-height:150%;
							text-align:left;
						}
						#templateHeader .mcnTextContent a,#templateHeader .mcnTextContent p a{
							color:#2BAADF;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateBody{
							background-color:#FFFFFF;
							border-top:0;
							border-bottom:2px solid #EAEAEA;
							padding-top:0;
							padding-bottom:9px;
						}
						#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
							color:#202020;
							font-family:Helvetica;
							font-size:16px;
							line-height:150%;
							text-align:left;
						}
						#templateBody .mcnTextContent a,#templateBody .mcnTextContent p a{
							color:#2BAADF;
							font-weight:normal;
							text-decoration:underline;
						}
						#templateFooter{
							background-color:#FAFAFA;
							border-top:0;
							border-bottom:0;
							padding-top:9px;
							padding-bottom:9px;
						}
						#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
							color:#656565;
							font-family:Helvetica;
							font-size:12px;
							line-height:150%;
							text-align:center;
						}
						#templateFooter .mcnTextContent a,#templateFooter .mcnTextContent p a{
							color:#656565;
							font-weight:normal;
							text-decoration:underline;
						}


						@media only screen and (min-width:768px){
							.templateContainer{
								width:600px !important;
							}
						}	@media only screen and (max-width: 480px){
							body,table,td,p,a,li,blockquote{
								-webkit-text-size-adjust:none !important;
							}
						}	@media only screen and (max-width: 480px){
							body{
								width:100% !important;
								min-width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							#bodyCell{
								padding-top:10px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImage{
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnCaptionTopContent,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
								max-width:100% !important;
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnBoxedTextContentContainer{
								min-width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupContent{
								padding:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
								padding-top:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
								padding-top:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardBottomImageContent{
								padding-bottom:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupBlockInner{
								padding-top:0 !important;
								padding-bottom:0 !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageGroupBlockOuter{
								padding-top:9px !important;
								padding-bottom:9px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnTextContent,.mcnBoxedTextContentColumn{
								padding-right:18px !important;
								padding-left:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
								padding-right:18px !important;
								padding-bottom:0 !important;
								padding-left:18px !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcpreview-image-uploader{
								display:none !important;
								width:100% !important;
							}
						}	@media only screen and (max-width: 480px){
							h1{
								font-size:22px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h2{
								font-size:20px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h3{
								font-size:18px !important;
								line-height:125% !important;
							}
						}	@media only screen and (max-width: 480px){
							h4{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templatePreheader{
								display:block !important;
							}
						}	@media only screen and (max-width: 480px){
							#templatePreheader .mcnTextContent,#templatePreheader .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateHeader .mcnTextContent,#templateHeader .mcnTextContent p{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateBody .mcnTextContent,#templateBody .mcnTextContent p{
								font-size:16px !important;
								line-height:150% !important;
							}
						}	@media only screen and (max-width: 480px){
							#templateFooter .mcnTextContent,#templateFooter .mcnTextContent p{
								font-size:14px !important;
								line-height:150% !important;
							}
						}

						footer .wrap-footer{
								position: relative;
								width: 100%;
								height: 72px;
								background: #5d1d47;
								display: flex;
								justify-content: space-between;
								align-items: center;

								margin: 0 auto;
							}

							.logo-engrane
							{
								margin: 20px;
							}
						
							footer.logo-engrane,
								footer.legales {
								
										text-align: center;
										width: 100%;
										margin: 10px 0;
								}	
								.legales
								{
									text-align: right;
								}
							.legales p{
										display: inline;
										font-size: 12px;
										font-family: AbsaraSansTF-Thin;
										color: #fff;
										margin-right: 10px;
										text-align: right;
									}
								
				</style>
			</head>
			<body style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;"><div itemscope itemtype="http://schema.org/EmailMessage"><div itemprop="publisher" itemscope itemtype="http://schema.org/Organization"><meta itemprop="name" content="*|LIST:COMPANY|*"><link itemprop="url" content="*|LIST:URL|*"></div><div itemprop="about" itemscope itemtype="http://schema.org/Offer"><link itemprop="image" href="https://gallery.mailchimp.com/e55512039fb8fcbea8f1003e1/images/60ec81c5-4a49-4ab2-a074-54834093143d.jpg"></div></div>
				
				<div style="text-align:left; margin: 55px;">
					<p> La reserva de la persona ' . $data['nombre']. ' se ha cancelado. </p>
				</div>
			
			<footer>
			<div class="wrap-footer">
				<div class="logo-engrane">
					<img style="width: 80%;" src="http://fondamexicana.com/assets/img/admin/logo_fonda_mexicana_blanco.png" alt="">
				</div>
			<div class="legales">
				<p style="font-size: 14px;"> fondamexicana.com</p>
				<br>
				<p>© Restaurantes Fonda Mexicana. 2018 Todos los derechos reservados.</p>
				
				
			</div>
		</div>
	</footer>
		</body>
		</html>	
		';
		$this->ci->email->from('no-reply@fondamexicana.com');
		//$this->ci->email->to('iescalona@cmr.mx');
		$this->ci->email->to($data['correo_responsable']);
		//$this->ci->email->to('alejandra@engranestudio.com');
		$this->ci->email->cc('ricardo@engranestudio.com, alejandra@engranestudio.com');
		$this->ci->email->subject('Fonda Mexicana - Cancelacion de reserva');
		$this->ci->email->message($msg); 

		if($this->ci->email->send())
		{
			return true;
		}
		return false;

	}

}