<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Util
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	public function val_login($var = 'default')
	{	
		if($var == 'default')
		{
			if(!$this->ci->session->userdata('logged'))
				redirect(base_url(),'refresh');
		}
		if($var == 'post')
		{
			if(!$_POST)
				redirect(base_url(),'refresh');
			if(!$this->ci->session->userdata('logged'))
				redirect(base_url(),'refresh');
		}
		if($var == 'log')
		{
			if($this->ci->session->userdata('logged'))
				redirect('admin/dashboard','refresh');
		}
	}

	public function sanear($string)
	{

	    $string = trim($string);

	    $string = str_replace(
	        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
	        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
	        $string
	    );

	    $string = str_replace(
	        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
	        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
	        $string
	    );

	    $string = str_replace(
	        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
	        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
	        $string
	    );

	    $string = str_replace(
	        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
	        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
	        $string
	    );

	    $string = str_replace(
	        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
	        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
	        $string
	    );

	    $string = str_replace(
	        array('ñ', 'Ñ', 'ç', 'Ç',),
	        array('n', 'N', 'c', 'C',),
	        $string
	    );

	    //Esta parte se encarga de eliminar cualquier caracter extraño
	    $string = str_replace(
	        array("\\", "¨", "º", "~",
	             "#", "@", "|", "!", "\"",
	             "·", "$", "%", "&", "/",
	             "(", ")", "?", "'", "¡",
	             "¿", "[", "^", "`", "]",
	             "+", "}", "{", "¨", "´",
	             ">", "< ", ";", ",", ":",
	              "."),
	        '',
	        $string
	    );

	    $string = str_replace(" ", "-", $string);

	    $string =  strtolower($string);


	    return $string;
	}

	public function set_folio($length) {
	    $pool  = array_merge(range(0,9), range('a', 'z'));
	    $folio = "alm-";

	    for($i=0; $i < $length; $i++) {
	        $folio .= $pool[mt_rand(0, count($pool) - 1)];
	    }

	    return $folio;
	}

	public function comp_folio($folio)
	{
		$comp = $this->ci->crud_model->get_data("eventos", array("folio" => $folio));

		return count($comp);
	}

	public function get_folio()
	{
		$folio = $this->set_folio(5);

		while ($this->comp_folio($folio) > 0) {
			$folio = $this->set_folio(5);
		}

		return strtoupper($folio);
	}


	public function disponibilidad($data)
	{
		$disponible = true;
		$eventos = $this->ci->crud_model->get_data("eventos");

		foreach ($eventos as $evento) 
		{
			if($data['nombre'] != $evento['nombre'])
			{
				if ($data['fecha'] == $evento['fecha'] && $data['horario'] == $evento['horario'] && $data['salon'] == $evento['salon'])
				{
					$disponible = false;
				}
			}

			
		}

		return $disponible;
	}
}

/* End of file Util.php */
/* Location: ./application/libraries/Util.php */
