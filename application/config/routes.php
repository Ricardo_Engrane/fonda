<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'Fonda_pages';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*CMS*/
$route['admin']                               = 'Fonda_cms/admin';
$route['admin/verify-user']                   = 'Fonda_cms/login';

$route['admin/dashboard']                     = 'Fonda_cms/dashboard';
$route['admin/eventos']                       = 'Fonda_cms/eventos';
$route['admin/eventos/(:num)']                = 'Fonda_cms/eventos/$1';
$route['admin/eventos/confirm']               = 'Fonda_cms/eventos_confirm';


$route['api/eventos/validar']                 = 'Fonda_cms/disponibilidad_front';
$route['api/eventos/enviar']                  = 'Fonda_cms/guardarDB_front';
$route['api/eventos/validarBack']             = 'Fonda_cms/validarFechasCMS';
$route['api/eventos/verificarNotificaciones'] = 'Fonda_cms/notificaciones';

$route['admin/filtred/(:any)']                = 'Fonda_cms/filtred/$1';

$route['admin/salir']                         = 'Fonda_cms/salir';

$route['api/contacto/enviar-formulario']      = 'Fonda_cms/formulario_contacto';
/*CMS*/

/*================================
=            Frontend            =
================================*/

$route['nuestros-platillos']             = 'Fonda_pages/carta';
$route['historia-de-tradicion']          = 'Fonda_pages/somos';
$route['organiza-tu-evento']             = 'Fonda_pages/eventos';
$route['aviso-de-privacidad']            = 'Fonda_pages/aviso';
$route['terminos-y-condiciones']         = 'Fonda_pages/terminos';


/*=====  End of Frontend  ======*/


