<main class="detalle-evento">
	<p><?php echo $this->session->flashdata('error_confirmar');?></p>
	<div class="header-eventos">
	    <!-- <p style="text-transform: uppercase;"> <?php echo $this->session->userdata['sucursal'];?> </p> -->
		<p style="text-transform: uppercase;"><?php echo $this->session->userdata['nombre'];?> </p>
	</div>
	<div class="header-search">
		<div class="back-link">
			<div class="link-btn">
				<a href="/admin/eventos" class="small"><span class="oi" data-glyph="chevron-left"></span> LISTA DE EVENTOS</a>
			</div> 
		</div>
		<div class="title">
			<p>DETALLE DE EVENTO</p>
		</div>
		<div class="search">
		</div>
	</div>
	<form action="<?php echo base_url(); ?>admin/eventos/confirm" method="POST" id="form_evento">
	<div class="status-bar">
		
		<p>DATOS PERSONALES</p>
		<p>FOLIO: <strong> <?php echo $prod['folio']; ?>  </strong></p>
		<p>NO. <strong> <?php echo $prod['id']; ?>  </strong></p>
		<p>STATUS:</p>
		<div class="icon">
			<span class="oi" data-glyph="eye"></span>
		</div>
		<select class="select-icons" name="select-status" id="select-status">
			<option <?php echo ($prod['status']=='pendiente'? 'selected' : '') ?> value="pendiente" class="yellow">PENDIENTE</option>
			<option <?php echo ($prod['status']=='confirmado'? 'selected' : '') ?> value="confirmado" class="blue">CONFIRMADO</option>
			<option <?php echo ($prod['status']=='cancelado'? 'selected' : '') ?> value="cancelado" class="black">CANCELADO</option>
			<option <?php echo ($prod['status']=='eliminado'? 'selected' : '') ?> value="eliminado" class="red">ELIMINADO</option>
		</select>
	</div>
	<div class="info-evento">
		<div class="info-usuario">
			<p class="nombre"><?php echo $prod["nombre"]; ?></p>
			<p class="telefono"><?php echo $prod['telefono']; ?></p>
			<p class="email"><?php echo $prod['email']; ?></p>
		</div>
		<div class="info-ubicacion">
			<!-- <p><span>LUGAR:</span> <?php echo $prod['lugar']; ?></p> -->
			<p><span>SALÓN:</span> <?php echo strtoupper($prod['salon']); ?></p>
			<p><span>INVITADOS:</span> <?php echo $prod['personas']; ?></p>
		</div>
		<div class="info-fecha">
			<p><span>FECHA:</span> <?php echo $prod['fecha']; ?></p>
			<p><span>HORARIO:</span> <?php echo $prod['hora_ini']; ?> a <?php echo $prod['hora_fin']; ?></p>
		</div>
	</div>

	<input type="hidden" name="id" value="<?php echo $prod['id'];?>">
	<input type="hidden" name="nombre_original" value="<?php echo $prod["nombre"]; ?>">
	<input type="hidden" name="telefono_original" value="<?php echo $prod["telefono"]; ?>">
	<input type="hidden" name="email_original" value="<?php echo $prod["email"]; ?>">
	<!-- <input type="hidden" name="lugar_original" value="<?php echo $prod["lugar"]; ?>"> -->
	<input type="hidden" name="salon_original" value="<?php echo $prod["salon"]; ?>">
	<input type="hidden" name="personas_original" value="<?php echo $prod["personas"]; ?>">
	<input type="hidden" name="fecha_original" value="<?php echo $prod["fecha"]; ?>">
	<input type="hidden" name="horaini_original" value="<?php echo $prod["hora_ini"]; ?>">
	<input type="hidden" name="horafin_original" value="<?php echo $prod["hora_fin"]; ?>">

	<div class="form-search">
		<!--
		<div class="group-input" style="margin-bottom: 48px;">
			<div class="label label-lugar">
				<p>PRECIO:</p>
			</div>
			<div class="input">
				<input type="text" class="validate-api-salones" placeholder="$000.00" name="precio" id="precio" value="">
			</div>
		</div> -->

		<div class="group-check">
			<div class="check-wrap">
				<input type="checkbox" name="nueva_fecha" id="nueva_fecha">
				<div class="icon-check" >
					<span  class="oi" data-glyph="check"></span>
				</div>
			</div>
			<div class="legal-check">
					<p>ACTIVAR<br>PARA MODIFICAR DATOS</p>
			</div>
		</div>

			

		<!--	<div class="group-input">
				<div class="label label-lugar">
					<p>LUGAR:</p>
				</div>
				<div class="select" >
					<select name="lugar" id="lugar" disabled="true">
						<option value="Almendros insurgentes">Insurgentes Sur</option>
						<option value="Almendros polanco">Polanco</option>
					</select>
				</div>
			</div> -->


		<div class="group-input">
			<div class="label label-lugar">
				<p>FECHA:</p>
			</div>
			<div class="input">
				<input type="date" class="validate-api-salones" placeholder="25 / 05 / 2018" disabled="true" name="cambio_fecha" id="cambio_fecha" value="">
			</div>
		</div>

			<div class="group-input">
				<div class="label label-lugar">
					<p>INVITADOS:</p>
				</div>
				<div class="input input--medium">
					<input type="text" disabled="true" placeholder="100" value="" name="nuevas_personas" id="nuevas_personas">
				</div>

			</div>

	
			<div class="group-input">
				<div class="label label-lugar">
					<p>SALÓN:</p>
				</div>
				<div class="select">

					<select disabled="true" name="select_salon" id="select_salon">
						<?php foreach ($salon as $salones) :?>
							<option <?php if ($prod["salon"] == $salones['salon']) echo "selected";?> value="<?php echo $salones['salon'];?>"><?php echo $salones['salon'];?> </option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>


			<div class="group-input">
				<div class="label label-lugar">
					<p>HORA:</p>
				</div>
				<div class="input input--df input--small">

					<select disabled="true" value="" placeholder="HORA INICIO" name="cambio_horaini" id="cambio_horaini">
						<option value="">HORA INICIO</option>
						<option value="7:00">7:00 am</option>
						<option value="7:30">7:30 am</option>
						<option value="8:00">8:00 am</option>
						<option value="8:30">8:30 am</option>
						<option value="9:00">9:00 am</option>
						<option value="9:30">9:30 am</option>
						<option value="10:00">10:00 am</option>
						<option value="10:30">10:30 am</option>
						<option value="11:00">11:00 am</option>
						<option value="11:30">11:30 am</option>
						<option value="12:00">12:00 pm</option>
						<option value="12:30">12:30 pm</option>
						<option value="13:00">1:00 pm</option>
						<option value="13:30">1:30 pm</option>
						<option value="14:00">2:00 pm</option>
						<option value="14:30">2:30 pm</option>
						<option value="15:00">3:00 pm</option>
						<option value="15:30">3:30 pm</option>
						<option value="16:00">4:00 pm</option>
						<option value="16:30">4:30 pm</option>
						<option value="17:00">5:00 pm</option>
						<option value="17:30">5:30 pm</option>
						<option value="18:00">6:00 pm</option>
						<option value="18:30">6:30 pm</option>
						<option value="19:00">7:00 pm</option>
						<option value="19:30">7:30 pm</option>
						<option value="20:00">8:00 pm</option>
						<option value="20:30">8:30 pm</option>
						<option value="21:00">9:00 pm</option>
						<option value="21:30">9:30 pm</option>
						<option value="22:00">10:00 pm</option>
						<option value="22:30">10:30 pm</option>
						<option value="23:00">11:00 pm</option>
					</select>

					<select disabled="true" value="" placeholder="HORA FINAL" name="cambio_horafin" id="cambio_horafin">
						<option value="">HORA FINAL</option>
						<option value="7:00">7:00 am</option>
						<option value="7:30">7:30 am</option>
						<option value="8:00">8:00 am</option>
						<option value="8:30">8:30 am</option>
						<option value="9:00">9:00 am</option>
						<option value="9:30">9:30 am</option>
						<option value="10:00">10:00 am</option>
						<option value="10:30">10:30 am</option>
						<option value="11:00">11:00 am</option>
						<option value="11:30">11:30 am</option>
						<option value="12:00">12:00 pm</option>
						<option value="12:30">12:30 pm</option>
						<option value="13:00">1:00 pm</option>
						<option value="13:30">1:30 pm</option>
						<option value="14:00">2:00 pm</option>
						<option value="14:30">2:30 pm</option>
						<option value="15:00">3:00 pm</option>
						<option value="15:30">3:30 pm</option>
						<option value="16:00">4:00 pm</option>
						<option value="16:30">4:30 pm</option>
						<option value="17:00">5:00 pm</option>
						<option value="17:30">5:30 pm</option>
						<option value="18:00">6:00 pm</option>
						<option value="18:30">6:30 pm</option>
						<option value="19:00">7:00 pm</option>
						<option value="19:30">7:30 pm</option>
						<option value="20:00">8:00 pm</option>
						<option value="20:30">8:30 pm</option>
						<option value="21:00">9:00 pm</option>
						<option value="21:30">9:30 pm</option>
						<option value="22:00">10:00 pm</option>
						<option value="22:30">10:30 pm</option>
						<option value="23:00">11:00 pm</option>
					</select>

					<!-- <input type="text" disabled="true" value="" placeholder="HORA INICIO" name="cambio_horaini" id="cambio_horaini"> -->
					<!-- <input type="text" disabled="true" value="" placeholder="HORA FINAL" name="cambio_horafin" id="cambio_horafin"> -->
				</div>

			</div>


		<div class="submit-button-wrap">
			<div id="fecha_exito" style="display:none;">
				<p class="sucess">DISPONIBLE</p>
			</div>

			<input type="hidden" name="correo_responsable" value="<?php echo $this->session->userdata['email']; ?>">
			<input type="hidden" name="nombre_responsable" value="<?php echo $this->session->userdata['nombre']; ?>">

			<div id="fecha_falla" style="display:none;">
				<p>NO DISPONIBLE</p>
			</div>
		
			<div>
				<button id="guardar-lightbox">GUARDAR</button>
			</div>


		</div>

		<div id="lightbox-aceptar">
			<div class="wrap-lightbox">
				<p>ESTAS SEGURO DE QUIERER MODIFICAR LA INFORMACIÓN DEL EVENTO</p>
				<div>
					<a href="#" id="cancel-lightbox">CANCELAR</a>
					<button>ACEPTAR</button>
				</div>
			</div>
		</div>
	</div>

	</form>
</main>


<!-- <section id="prod-detalle">
	<div class="container mt-5 pb-5">
		<?php if (!is_null($this->session->flashdata("prod_add"))) { ?>
		<div class="alert alert-<?php echo $this->session->flashdata('status'); ?> mb-5" role="alert">
		  	<?php echo $this->session->flashdata("prod_add") ?>
		</div>
		<?php } ?>
		<div class="d-flex justify-content-between">
			<div>
				<a href="<?php echo base_url(); ?>admin/eventos" class="btn btn-secondary"><span class="oi oi-chevron-left"></span> Lista de eventos</a>
			</div>
	
		</div>
			
			<div class="card mt-5">
			<div class="card-body">
				<p>Nombre:</p>
				<p class="h5"> <?php echo $prod["nombre"]; ?> </p>
				 
			</div>
		</div>

		<div class="row">
			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Fecha:</p>
						<p class="h5"><?php echo $prod['fecha']; ?></p>
						
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Hora de inicio:</p>
						<p class="h5"><?php echo $prod['hora_ini']; ?></p>
						
					</div>
				</div>
			</div>

			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Hora de fin:</p>
						<p class="h5"><?php echo $prod['hora_fin']; ?></p>
						
					</div>
				</div>
			</div>

			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Salon:</p>
						<p class="h5"><?php echo $prod['salon']; ?></p>
						
					</div>
				</div>
			</div>

			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Número de personas:</p>
						<p class="h5"><?php echo $prod['personas']; ?></p>
						
					</div>
				</div>
			</div>
			
		</div>


		 <input type="checkbox" name="nueva_fecha" id="nueva_fecha" value="Fecha">Cambiar la fecha seleccionada<br>

		 <p>*Si habilitas esta opcion, los datos a continuacion son los que se guardarán.</p>
		<form action="<?php echo base_url(); ?>admin/eventos/confirm" method="POST" class="form" id="form_evento">
			
			<input type="hidden" name="nombre_reserva" id="nombre_reserva" value="<?php echo $prod["nombre"]; ?>">
			<input type="hidden" name="personas" value="<?php echo $prod['personas']; ?>">

		<div class="row">
			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Lugar:</p>
						<select disabled="true" name="lugar" id="lugar" class="custom-select alert-<?php echo $status_color;?>" >
							<option value="almendros insurgentes" <?php if ($prod["lugar"] == "almendros insurgentes") echo "selected";?>>los almendros insurgentes</option>
							<option value="almendros polanco" <?php if ($prod["lugar"] == "almendros polanco") echo "selected";?>>los almendros polanco</option>
								
						</select>
					</div>
				</div>
			</div>


			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Nueva fecha:</p>
						<div class="input-append date form_datetime">
						    <input disabled="true" size="16" type="text" name="cambio_fecha" id="cambio_fecha" value="" >
						    <span class="add-on"><i class="icon-th"></i></span>
						</div>		
					</div>
				</div>
			</div>

			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Nuevo salon:</p>

						<select disabled="true" name="select_salon" id="select_salon" class="custom-select alert-<?php echo $status_color;?>" >
	
								<option <?php if ($prod["salon"] == "salon 1") echo "selected";?> value="salon 1">salon 1</option>
								<option <?php if ($prod["salon"] == "salon 2") echo "selected";?>  value="salon 2">salon 2</option>
								<option <?php if ($prod["salon"] == "salon 3") echo "selected";?>  value="salon 3">salon 3</option>
		
					</select>
					</div>

				</div>
			</div>


			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Nueva hora de inicio:</p>
						<div class="input-group bootstrap-timepicker timepicker">
				            <input disabled="true"  type="text" name="cambio_horaini" id="cambio_horaini" value="" class="form-control input-small" >
				            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
		        		</div>
		        	</div>
				</div>
			</div>

			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Nueva hora de fin:</p>
						<div class="input-group bootstrap-timepicker timepicker">
				            <input type="text" id="cambio_horafin" name="cambio_horafin" value="" class="form-control input-small" disabled="true">
				            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
		        		</div>
		        	</div>
				</div>
			</div>

			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Número de personas:</p>
						<div class="input-group bootstrap-timepicker timepicker">
				            <input type="text" id="nuevas_personas" name="nuevas_personas" class="form-control input-small" disabled="true" value="<?php echo $prod['personas']; ?>">
				            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
		        		</div>
		        	</div>
				</div>
			</div>

		</div>


		<div class="card mt-5">

			<div class="alert alert-success" id="fecha_exito" style="display:none;">La fecha y hora se encuentran disponibles
			</div>
			<div class="alert alert-danger" id="fecha_falla" style="display:none;">La fecha y hora deseadas no estan disponibles</div>
		
		</div>

		<div class="row">

			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Lugar:</p>
						<p class="h5"><?php echo $prod['lugar']; ?></p>
						
					</div>
				</div>
			</div>

			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Telefono:</p>
						<p class="h5"><?php echo $prod['telefono']; ?></p>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Correo electronico:</p>
						<p class="h5" ><?php echo $prod['email']; ?></p>
						<input type="hidden" name="correo_reserva" value="<?php echo $prod['email']; ?>">
					</div>
				</div>
			</div>
		</div> 


		<div class="row">
			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Nombre del responsable:</p>
						
						<p class="h5" ><?php echo $this->session->userdata['nombre']; ?></p>
						<input type="hidden" name="nombre_responsable" value="<?php echo $this->session->userdata['nombre']; ?>">
					</div>
				</div>
			</div>


			<div class="col-12 col-sm-4 mt-4">
				<div class="card">
					<div class="card-body">
						<p>Correo electronico de responsable:</p>
						<p class="h5" ><?php echo $this->session->userdata['email']; ?></p>
						<input type="hidden" name="correo_responsable" value="<?php echo $this->session->userdata['email']; ?>">
					</div>
				</div>
			</div>
		</div>

		<div class="mt-5 pb-5">
			
				<p class="h4">Status:</p>
				<div class="input-group input-group-lg">
					<select name="select-status" id="select-status" class="custom-select alert-<?php echo $status_color;?>">
						<option value="confirmado" <?php if ($prod["status"] == "confirmado") echo "selected";?>>Confirmado</option>
						<option value="pendiente" <?php if ($prod["status"] == "pendiente") echo "selected";?>>Pendiente</option>
						<option value="cancelado" <?php if ($prod["status"] == "cancelado") echo "selected";?>>Cancelado</option>
					</select>
				</div>
				<div class="text-right mt-4">
					<button type="submit" class="btn btn-primary" id="btn-edit-status">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</section> -->
