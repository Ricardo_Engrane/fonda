<main class="login">
	<div class="login-bg"></div>
	<div class="head-login">
		<div class="logo">
			<img src="/assets/img/admin/login/logo_login.png" alt="">
		</div>
		
	</div>
	<div class="form-login">
		<div class="form">
			<form action="/admin/verify-user" method="POST" id="form-login">
				<div class="form-group">
					<input type="email" class="form-control" id="usuario" name="usuario" placeholder="Nombre de usuario">
				</div>
				<div class="form-group">
					<input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
				</div>
				<button type="submit" class="btn-naranja">ENTRAR</button>
				<div class="msg">
					<p><?php echo $this->session->flashdata('user'); ?></p>
				</div>
			</form>
		</div>
	</div>
	<footer class="footer-login">
		<div class="left">
			<p>© Restaurantes Fonda Mexicana. 2018 Todos los derechos reservados.</p>
		</div>
		<div class="right">
			<a href="https://www.cmr.mx/" target="_blank"><img src="/assets/img/cmr_ico_small.png" alt=""></a>
		</div>
	</footer>
</main>