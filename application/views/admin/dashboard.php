<main class="dashboard">
	<div class="title">
		<p>BIENVENIDO AL SISTEMA DE FONDA MEXICANA</p>
	</div>
	<div class="wrap-items">
		<div class="item">
			<div class="desc">
				<p>CONSULTA LA INFORMACIÓN DE LAS RESERVACIONES DE EVENTOS</p>
			</div>
			<div class="link-btn">
				<a href="/admin/eventos">VER EVENTOS</a>
			</div>
		</div>
	</div>
</main>