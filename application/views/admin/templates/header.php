<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="theme-color" content="#8dc53f">
	<title>Fonda Mexicana - CMS</title>
	<meta name="robots" content="noindex, follow">
	<link rel="shortcut icon" href="" type="image/x-icon">
	<link rel="icon" href="" type="image/x-icon">
	<link rel="stylesheet" href="/css/admin.css">
</head>
<body>

<nav class="nav-header">
	<div class="nav-wrap">
		<div class="logo-nav">
			<a href="/admin/dashboard">
				<img src="/assets/img/admin/logo_fonda_mexicana_blanco.png" alt="">
			</a>
		</div>
		<div class="actions-nav">
			<div>
				<span class="oi" data-glyph="bell"></span>
				<p id="numero_notificaciones"></p>
			</div>
			<a href="/admin/salir"><span class="oi" data-glyph="account-logout"></span></a>
		</div>
	</div>
</nav>