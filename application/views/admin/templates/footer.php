	<footer>
		<div class="wrap-footer">
			<div class="logo-engrane">
				<img src="/assets/img/admin/footer/logo_foot.png" alt="">
			</div>
			<div class="legales">
				<p>© Restaurantes Fonda Mexicana. 2018 Todos los derechos reservados.</p>
				<img src="/assets/img/admin/footer/logo_cmr.png" alt="">
			</div>
		</div>
	</footer>

	<script type="text/javascript">
		var baseUrl = '<?php echo base_url(); ?>';
	</script>
	<script type="text/javascript" src="/js/admin.js"></script>
	

	<script type="text/javascript">
		$(document).ready(function() 
		{
			 $.ajax({
	                        url  : "/api/eventos/verificarNotificaciones",
	                        type : 'GET',
	                        data : {

	                        },
	                        success : function(data) {
	                          if (data > 0)
	                          {
	                          	$('#tooltip_notificaciones').attr('title','');
	                            $('#tooltip_notificaciones').attr('title','Tienes ' + data + ' eventos sin leer');
	                            $('#numero_notificaciones').text(data);
	                          }
	                          else
	                          {
	                            $('#tooltip_notificaciones').attr('title','No tienes eventos sin leer');
	                            $('#numero_notificaciones').text(data);
	                          }

	                        }
	                    })
	                    .fail(function() {
	                        console.log("error");
	                    });
	    
		});
	</script>
</body>
</html>