<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="theme-color" content="#8dc53f">
	<title>Fonda Mexicana - CMS</title>
	<meta name="robots" content="noindex, follow">
	<link rel="shortcut icon" href="" type="image/x-icon">
	<link rel="icon" href="" type="image/x-icon">
	<link rel="stylesheet" href="/css/admin.css">
</head>
<body>