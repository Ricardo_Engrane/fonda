<main class="main-eventos">
	<div class="header-eventos">
	<!--	<p style="text-transform: uppercase;"> <?php echo $this->session->userdata['sucursal'];?> </p> -->
		<p style="text-transform: uppercase;"><?php echo $this->session->userdata['nombre'];?> </p>
	</div>
	
	<div class="header-search">
		<div class="back-link">
			<div class="link-btn">
				<a href="/admin/dashboard" class="small"><span class="oi" data-glyph="chevron-left"></span> INICIO</a>
			</div>
		</div>
		<div class="title">
			<p>LISTA DE EVENTOS</p>
		</div>
		<div class="search">
		</div>
	</div>
	<div class="header-filters">
		<a href="/admin/filtred/pendient" class="filtro <?php echo ($this->uri->segment(3) == 'pendient' ? 'filtro--active' : '') ?>">
			<div class="icon icon--yellow">
				<span class="oi" data-glyph="eye"></span>
			</div>
			<div class="desc">
				<p>PENDIENTES</p>
			</div>
		</a>
		<a href="/admin/filtred/confirm" class="filtro <?php echo ($this->uri->segment(3) == 'confirm' ? 'filtro--active' : '') ?>">
			<div class="icon icon--blue">
				<span class="oi" data-glyph="check"></span>
			</div>
			<div class="desc">
				<p>CONFIRMADOS</p>
			</div>
		</a>
		<a href="/admin/filtred/cancel" class="filtro <?php echo ($this->uri->segment(3) == 'cancel' ? 'filtro--active' : '') ?>">
			<div class="icon icon--black">
				<span class="oi" data-glyph="ban"></span>
			</div>
			<div class="desc">
				<p>CANCELADOS</p>
			</div>
		</a>
		<a href="/admin/filtred/deleted" class="filtro <?php echo ($this->uri->segment(3) == 'deleted' ? 'filtro--active' : '') ?>">
			<div class="icon icon--red">
				<span class="oi" data-glyph="trash"></span>
			</div>
			<div class="desc">
				<p>ELIMINADOS</p>
			</div>
		</a>
	</div>
	<div class="data-table-eventos">
		<table id="tabla_almendros" class="display">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Folio</th>
					<th scope="col">Nombre</th>
					<th scope="col">Fecha</th>
					<th scope="col">Hora de inicio</th>
					<th scope="col">Hora de fin</th>
					<th scope="col">Salon</th>
					<th scope="col">Personas</th>
					<th scope="col">Telefono</th>
					<th scope="col">Correo electronico</th>
					<th scope="col">Status</th>
					<th scope="col">Editar</th>
				</tr>
			</thead>
			<tbody>
				<?php $i=1; foreach ($pedidos as $key => $pedido) { ?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $pedido["folio"]; ?></td>
					<td><?php echo $pedido["nombre"]; ?></td>
					<td><?php echo $pedido["fecha"]; ?></td>
					<td><?php echo $pedido["hora_ini"]; ?></td>
					<td><?php echo $pedido["hora_fin"]; ?></td>
					<td><?php echo $pedido["salon"]; ?></td>
					<td><?php echo $pedido["personas"]; ?></td>
					<td><?php echo $pedido["telefono"]; ?></td>
					<td><?php echo $pedido["email"]; ?></td>
					<td class="status-wrap">
						<!-- <?php echo $pedido["status"]; ?> -->
						<div class="icon <?php echo ($pedido["status"] == 'pendiente' ? 'icon--yellow' : '') ?>">
							<span class="oi" data-glyph="eye"></span>
						</div>
						<div class="icon <?php echo ($pedido["status"] == 'confirmado' ? 'icon--blue' : '') ?>">
							<span class="oi" data-glyph="check"></span>
						</div>
						<div class="icon <?php echo ($pedido["status"] == 'cancelado' ? 'icon--black' : '') ?>">
							<span class="oi" data-glyph="ban"></span>
						</div>
						<div class="icon <?php echo ($pedido["status"] == 'eliminado' ? 'icon--red' : '') ?>">
							<span class="oi" data-glyph="trash"></span>
						</div>
					</td>
					<td>
						<div class="icon icon--green">
							<a href="/admin/eventos/<?php echo $pedido['id']; ?>">
								<span class="oi" data-glyph="pencil"></span>
							</a>
						</div>
					</td>
				</tr>
				<?php $i++; } ?>
			</tbody>
		</table>
	</div>
</main>
<!-- <section id="admin-pedidos" class="datatable">
	<div class="container mw-100 px-5 pt-5 pb-5">
		<?php if (!is_null($this->session->flashdata("evento_edit"))) { ?>
		<div class="alert alert-success mb-5" role="alert">
		  	<?php echo $this->session->flashdata("evento_edit") ?>
		</div>
		<?php } ?>
		<a href="<?php echo base_url(); ?>admin" class="btn btn-secondary"><span class="oi oi-chevron-left"></span> Inicio</a>
		<h1 class="text-center mt-3 mb-5">Lista de eventos</h1>
		<ul class="list-inline" style="text-align: center;">
		  <a href="<?php echo base_url(); ?>admin/filtred/confirm"> <li class="list-inline-item">Confirmados<br> </li> </a>
		  <a href="<?php echo base_url(); ?>admin/filtred/pendient"> <li class="list-inline-item">Pendientes</li> </a>
		  <a href="<?php echo base_url(); ?>admin/filtred/cancel"> <li class="list-inline-item">Cancelados</li> </a>
		  <a href="<?php echo base_url(); ?>admin/filtred/deleted"> <li class="list-inline-item">Eliminados</li> </a>
		</ul>
		<div class="card border-0">

			<table id="tabla_almendros" class="display" >
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Folio</th>
						<th scope="col">nombre</th>
						<th scope="col">fecha</th>
						<th scope="col">hora de inicio</th>
						<th scope="col">hora de fin</th>
						<th scope="col">lugar</th>
						<th scope="col">salon</th>
						<th scope="col">personas</th>
						<th scope="col">telefono</th>
						<th scope="col">correo electronico</th>
						<th scope="col">status</th>
						<th scope="col">ver</th>
						<th scope="col">eliminar </th>
					</tr>
				</thead>
				<tbody>
					<?php $i=1; foreach ($pedidos as $key => $pedido) { ?>
					<tr>
						<td> <?php echo $i; ?></td>
						<td><?php echo $pedido["folio"]; ?></td>
						<td><?php echo $pedido["nombre"]; ?></td>
						<td><?php echo $pedido["fecha"]; ?></td>
						<td><?php echo $pedido["hora_ini"]; ?></td>
						<td><?php echo $pedido["hora_fin"]; ?></td>
						<td><?php echo $pedido["lugar"]; ?></td>
						<td><?php echo $pedido["salon"]; ?></td>
						<td><?php echo $pedido["personas"]; ?></td>
						<td><?php echo $pedido["telefono"]; ?></td>
						<td><?php echo $pedido["email"]; ?></td>
						<td><?php echo $pedido["status"]; ?></td>
						<td>
							<a href="<?php echo base_url(); ?>admin/eventos/<?php echo $pedido['id']; ?>" class="btn btn-primary mr-2"><span class="oi oi-eye"></span></a>

							
						</td>
						<td>
							<a href="<?php echo base_url(); ?>admin/eventos/delete/<?php echo $pedido['id']; ?>" class="btn btn-primary mr-2"><span class="oi oi-trash"></span></a>
						</td>
						
					</tr>
					<?php $i++; } ?>
				</tbody>
			</table>
		</div>
	</div>
</section> -->


