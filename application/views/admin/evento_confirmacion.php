<main class="main-eventos">
	<div class="header-search">
		<div class="back-link">
			<div class="link-btn">
				<a href="<?php echo base_url(); ?>admin/eventos" class="small"><span class="oi" data-glyph="chevron-left"></span> REGRESA A EVENTOS</a>
			</div>
		</div>
	</div>
	<div class="info-aviso">
		<h2>La reservación con el folio #<strong><?php echo $usuario['folio'] ?></strong> ha sido confirmada exitosamente. A continuación se muestran los datos de la reserva: </h2>
		<ul>
			<li><strong>Nombre: </strong> <?php echo $usuario['nombre'];?> </li>
			<li><strong>Correo electronico: </strong> <?php echo $usuario['email'];?></li>
			<li><strong>Salon: </strong> <?php echo $usuario['salon'];?></li>
			<li><strong>Fecha de reservación: </strong> <?php echo $usuario['fecha'];?> </li>
			<li><strong>Hora de reservación: </strong> <?php echo $usuario['hora_ini'];?></li>
			<li><strong>Hora de fin del evento: </strong> <?php echo $usuario['hora_fin'];?></li>
			<li><strong>Personas: </strong> <?php echo $usuario['personas'];?> </li>
		</ul>

		<h2> Se envió un mensaje a la dirección de correo electronico ingresada por la persona.</h2>
	</div>
</main>

<!-- <section id="admin-pedidos" class="datatable">
	<div class="container mw-100 px-5 pt-5 pb-5">

		<div class="d-flex justify-content-between">

			<div class="back-link">
				<div class="link-btn">
					<a href="<?php echo base_url(); ?>admin/eventos" class="btn btn-secondary"><span class="oi oi-chevron-left"></span> Lista de eventos</a>
				</div>
			</div>	
		</div>
		<div class="container">
				<div class="justify-content-center">
					<?php if (!is_null($this->session->flashdata("user"))) { ?>
					<?php } ?>
				</div>
					<div class="border border-primary pt-3 pb-5 px-5 mt-5">
							<h5>La reservación con el folio #<strong><?php echo $usuario['folio'] ?></strong> ha sido confirmada exitosamente. A continuación se muestran los datos de la reserva: </h5>
							<br>
							<br>

							<ul>
								<li> <strong>Nombre: </strong> <?php echo $usuario['nombre'];?> </li>
								<li> <strong>Correo electronico: </strong> <?php echo $usuario['email'];?></li>
								<li> <strong>Lugar de reservación: </strong> <?php echo $usuario['lugar'];?> </li>
								<li> <strong>Salon: </strong> <?php echo $usuario['salon'];?></li>
								<li> <strong>Fecha de reservación: </strong> <?php echo $usuario['fecha'];?> </li>
								<li> <strong>Hora de reservación: </strong> <?php echo $usuario['hora_ini'];?></li>
								<li> <strong>Hora de fin del evento: </strong> <?php echo $usuario['hora_fin'];?></li>
								<li> <strong>Personas: </strong> <?php echo $usuario['personas'];?> </li>
							</ul>

							<h5> Se envió un mensaje a la dirección de correo electronico ingresada por la persona.</h5>

					</div>
		</div>

	</div>
</section>
 -->
