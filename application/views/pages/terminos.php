<main class="aviso-privacidad">
	<div class="container">
		<div class="header-aviso">
			<div class="title">
				<h1><b>TÉRMINOS Y CONDICIONES</b></h1>
				<h2><b>TÉRMINOS Y CONDICIONES DEL SITIO WEB CMR.MX</b></h2>
				<p>En vigor desde el 00 de mes de 0000.</p>
			</div>
			<div class="grec">
				<img src="/assets/img/aviso/grec.png" alt="">
			</div>
		</div>

		<div class="content">
			<p><b>POR FAVOR REVISE CUIDADOSAMENTE LOS TERMINOS DE USO DE ESTE SITIO.</b> Al usar este sitio, usted está aceptando los términos y condiciones estipulados aquí. Periódicamente podemos cambiar los términos, por favor verifique estos cambios de vez en cuando, así el continuar usando este sitio indica su aceptación de cualquier cambio de los términos aquí establecidos.</p>
			<br>
			<p><b>MIENTRAS QUE REALIZAMOS UN ESFUERZO RAZONABLE PARA PROVEER DE UNA INFORMACIÓN VERAZ Y A TIEMPO ACERCA DE GRUPO LOS AMENDROS, LA COOMPAÑIA Y SUS AFILIADAS EN ESTE SITIO, USTED NO DEBE ASUMIR QUE ESTA INFORMACIÓN PROPORCIONADA ESTA SIEMPRE AL DIA O QUE ESTE SITIO CONTIENE TODA LA INFORMACIÓN DISPONIBLE ACERCA DE LOS ALMENDROS. EN PARTICULAR SI USTED ESTA HACIENDO UNA DECISION DE INVERSION ACERCA DE LOS ALMENDROS, POR FAVOR CONSULTE DIFERENTES RECURSOS, INCLUYENDO ORGANISMOS FINANCIEROS Y GUBERNAMENTALES.</b></p>
			<br>
			<p><b>DERECHOS RESERVADOS Y MARCA REGISTRADA.</b> A menos que se establezca lo contrario, todo los materiales en este sitio están protegidos por derechos de autor, marcas u otra propiedad intelectual y son propiedad de Los Almendros o sus afiliadas o por otras partes que hayan licenciado sus materiales a Los Almendros.</p>
			<br>
			<p>Las marcas Los Almendros en este sitio representan parte de las marcas que en la actualidad posee o controla en México o en otros países por Los Almendros o el Instituto Mexicano de la Propiedad Industrial bajo licencia de Los Almendros. La publicación de estas marcas o noticias asociadas con estas marcas no intenta ser una recopilación coherente de todas las propiedades de Los Almendros alrededor del mundo sobre propiedad de derechos de autor, y Los Almendros puede poseer o controlar otros derechos de propiedad en un o mas países fuera de México.</p>
			<br>
			<p>Todos los derechos no mencionados explícitamente están reservados.</p>
			<br>
			<p><b>USO PERSONAL.</b> El uso que usted pueda dar al material incluido en este sitio es exclusivamente para publicidad y mercadeo. Esta usted de acuerdo en no distribuir, publicar, transmitir, modificar, exhibir o crear trabajos derivados de la explotación de los contenidos de este sitio en cualquier forma. Esta usted de acuerdo en indemnizar, defender y mantener a salvo a Los Almendros de cualquier uso no autorizado que haga de estos contenidos en este sitio. Su conocimiento del uso no autorizado de estos contenidos pueden causar un daño irreparable a Los Almendros, y en el caso de que se realice un uso no autorizado, Los Almendros podrá promover un procedimiento judicial sea a través de requerimientos o mediante cualquier otro medio disponible por ley o en equidad.</p>
			<br>
			<p><b>RESPUESTA Y RESPOSABILIDAD.</b> Está usted de acuerdo que permanecerá como el único responsable por el contenido de cualquier presentación que haga y no nos presentará ningún material que sea desleal, difamatorio, abusivo u obsceno. Usted está de acuerdo en que no presentará nada a este sitio que viole ningún derecho de ningún tercero, incluyendo derechos de autor, marcas registradas u otras derechos personales o de propiedad.</p>
			<br>
			<p>Mientras que apreciamos su interés en Los Almendros, no queremos y no podemos aceptar ninguna idea que pudiera usted considerar  como propiedad de  derechos sobre diseños, productos, tecnología u otras sugerencias que puede haber desarrollado. Consecuentemente, cualquier material que usted presente a este sitio será tomado como exento  de pago de regalías de licencia y derechos de uso, modificación, publicación, transmisión, adaptación, exhibición , traslado, trabajos derivados a través del universo por cualquier medio y a través de cualquier medio de distribución, transmisión y exhibición conocido o por conocer. En adición, usted garantiza que ha renunciado a los llamados "derechos morales".</p>
			<br>
			<p><b>VENTA DE PRODUCTOS Y DISPONIBILIDAD.</b> Todos los productos exhibidos en este sitio serán entregados solamente dentro de México y, cuando sea posible, en Estados Unidos y América Latina.  Por favor verifique la reglamentación correspondiente. Todos Los precios en el sitio están en pesos mexicanos y son validos solo en México. Los Almendros se reserva el derechos sin aviso de quitar o cambiar especificaciones sobre productos y servicios ofrecidos en este sitio sin incurrir en ninguna obligación.</p>
			<br>
			<p><b>VINCULOS.</b> Algunos vínculos del sitio le permitirán conectarse con otros sitios que o están bajo nuestro control. Los Almendros provee estos vínculos solo como un servicio. La aparición de un vínculo no implica que Los Almendros los apoya, ni que sea  responsable por los contenidos de cualquiera de los sitios vinculados. Usted accede a estos sitios bajo su responsabilidad.</p>
			<p>Los Almendros no garantiza que las funciones de este sitio no sean interrumpidas o que estén libres de error. De que este sitio o servidor este libre de virus u otros componentes dañinos, en el caso serán corregidos en el momento que Los Almendros se percate de ellos.</p>
			<br>
			<p><b>MENORES.</b> Los Almendros pide a los padres supervisión de sus hijos mientras estén en línea. Ninguna información agregada por un menor será publicada si el consentimiento de los padres.</p>
			<br>
			<p><b>JURISDICCIÓN.</b> Cualquier disputa que surja de estos términos será resuelta exclusivamente en los juzgados en el estado de Yucatán, México. Los Almendros no asegura que los materiales en este sitio son apropiados o disponibles para otras ubicaciones. Si usted accede a este sitio desde fuera de México, tenga en cuanta que ese sitio puede contener referencias a productos y servicios que no están disponibles o prohibidos en su país.</p>
			<br>
			<p>Politica de Privacidad    El nombre y la reputación de Los Almendros se crearon con base a la confianza. Esta política de privacidad en línea describe la información que Los Almendros recopila a través de los servicios disponibles en nuestro sitio: www.losalmendros.com.mx (el “Sitio”) así como con la manera en que usamos esta información. Nuestra política también describe las opciones que puede elegir para recopilar y utilizar su información.</p>
			<br>

			<p>¿Qué tipo de información recopila Los Almendros acerca de mí? Los Almendros recopila información personal. Información personal significa información de identificación individual que puede incluir:</p>
			<br>
			
			<p>• Información de identificación, tal como nombre, domicilio, dirección de correo electrónico</p>
			<p>• Información financiera, tal como número de tarjeta de crédito, e</p>
			<p>• Información relacionada con el empleo, tal como información de currículo, solicitudes de empleo, verificación de antecedentes, o referencias de empleos.</p>
			<br>
			<p>La información personal no incluye información de contacto en oficinas (por ejemplo, domicilio del centro de trabajo, número de teléfono) y aquellas jurisdicciones que consideren dicha información como información no personal.</p>
			<br>

			<p>Nosotros podemos recibir información del sitio web de cualquier marca Los Almendros, incluyendo “Chichibas”, “Casa Almendros”, “Parrilla Achiote” o “Aromas y Sabores de Yucatán ™.</p>
			<br>
			<p>La información personal será recopilada al máximo posible, directamente de la persona interesada.</p>
			<br>
			<p>Además de la información personal que activamente nos proporcione, nosotros también recopilamos información de uso del sitio web, la cual incluye información de su proveedor de Internet, tipo de navegador (browser), nombre del dominio, dirección IP, sitio web que le refirió a nosotros, páginas web que solicitó, la fecha y hora de estas solicitudes, así como los puntos de entrada y salida. Nuestra colección de información de uso del sitio web puede incluir el uso de cookies y pixel tags.</p>
			<br>
			<p>Los Almendros puede también recopilar datos agregados, que son los datos con los cuales no puede determinarse la identidad de una persona. Debido a que ésta no es información personal, Los Almendros puede utilizar datos agregados en la manera que considere conveniente.</p>
			<br>
			<p>¿Cómo utiliza Los Almendros la información personal? Los Almendros puede utilizar la información proporcionada por los clientes y empleados potenciales, de manera verbal o escrita (incluyendo medios electrónicos) para:</p>
			<br>
			<p>• Procesar y manejar sus compras de productos y servicios Los Almendros.</p>
			<p>• Comunicarse con usted acerca de su pedido o compra, su cuenta Los Almendros.com.mx, o algún concurso en el que haya participado.</p>
			<p>• Responder a sus solicitudes de servicio al cliente</p>
			<p>• Comunicarse con usted acerca de:</p>
			<p>• Empleos específicos, oportunidades de trabajo o información general de empleo en Los Almendros, en el caso de que haya creado un perfil personal o enviado su currículum en línea.</p>
			<p>• Nuestras marcas, productos, eventos u otros propósitos promocionales, incluyendo ofertas de nuestras marcas, si ha optado por recibir estas comunicaciones ya sea porque nos ha proporcionado su dirección de correo electrónico en la página losalmendros.com.mx o de acuerdo con las preferencias de comunicación que indicó al registrarse para una cuenta de losalmendros.com.mx</p>
			<p>• Comunicarse, al máximo requerido, y manejar  nuestras relaciones con consultores LosAlmendros, socios estratégicos, agentes, distribuidores, proveedores, contratistas y otras compañías afiliadas con LosAlmendros, con el objeto de mejorar la oferta de productos y servicios LosAlmendros y</p>
			<p>• Cumplir cualquier requerimiento legal y regulatorio aplicable.</p>
			<br>
			<p>¿Mi información personal es compartida con terceros? Nosotros le podemos enviar información de mercadeo y promociones de otras compañías que le puedan interesar, únicamente si nos indica que le gustaría recibir este tipo de información en sus preferencias de comunicación.</p>
			<br>
			<p>Nuestro sitio web también puede compartir información con compañías que nos proporcionan servicios (tales como procesadores de tarjetas de crédito, servidores de correo o sitios web) o compañías que nos ayudan a promocionar nuestros productos y servicios (tales como vendedores de cuentas de correo electrónico). Estas compañías pueden necesitar información sobre usted para poder realizar sus funciones. Estas compañías no están autorizadas a utilizar la información que compartimos con ellas para ningún otro propósito.</p>
			<br>
			<p>Podemos publicar información específica a solicitud del gobierno, como respuesta a una orden judicial, o cuando lo requiera la ley para hacer cumplir nuestra políticas corporativas, o proteger nuestros derechos, propiedad o seguridad, o los de otros. Nosotros no proporcionamos información a estas agencias o compañías para mercadeo o propósitos comerciales.</p>
			<br>
			<p>En el caso de que se venda parte o todo nuestro negocio, Los Almendros puede publicar la información personal a aquellas personas involucradas en la transferencia de todos o de parte de los activos o del negocio.</p>
			<br>
			<p>A menos que la ley lo permita, no se recopilará información personal a menos que se obtenga su consentimiento para la recopilación, uso y autorización para compartir esta información. La cláusula de información personal significa para Los Almendros que usted está de acuerdo y acepta que podemos recopilar, utilizar y compartir su información personal de acuerdo con lo establecido en esta política de privacidad.</p>
			<br>
			<p>Adicionalmente, cuando creamos conveniente, pueden obtenerse autorizaciones específicas o de aceptación eventualmente. Por ejemplo, podemos buscar la aceptación para utilizar y compartir información personal después de recopilarla en aquellos casos donde Los Almendros desee utilizar la información para un propósito no identificado previamente o para el cual la persona en cuestión no ha proporcionado previamente su consentimiento.</p>
			<br>
			<p>En la mayoría de los casos, y sujetos a restricciones legales y contractuales, una persona es libre de negarse o retirar su consentimiento en cualquier momento mediante un aviso previo con tiempo razonable. Debe notarse que en algunas circunstancias, ciertos productos y servicios Los Almendros pueden ser ofrecidos únicamente si la persona proporciona su información personal a Los Almendros. Si opta por no proporcionarnos ninguna información personal, no estaremos en condiciones de ofrecerle los productos o servicios requeridos. Nosotros le informaremos de las consecuencias del retiro de su consentimiento.</p>
			<br>
			<p>¿Cómo puedo cambiar las comunicaciones promocionales que recibo? Nosotros podemos utilizar su dirección de correo electrónico u otra información proporcionada a través del Sitio para enviarle nuestro material promocional y, si usted lo optó, de terceros. Si usted no quiere recibir esta información promocional, puede cambiar sus preferencias de comunicación y retirarse de nuestra lista de correos electrónicos en cualquier momento.</p>
			<br>
			<p>Por favor note que incluso si optó por no recibir nuestros correos electrónicos promocionales, podemos utilizar su información para comunicarnos con usted para otros propósitos, tales como pedidos / compras, respuestas de servicio al cliente y comunicaciones relacionadas con empleos.</p>
			<br>
			<p>¿Cómo puedo retirar o modificar mi información personal? Los Almendros se esfuerza en asegurar que cualquier información personal proporcionada y en su posesión sea precisa, actual y completa para los propósitos para los cuales utilizamos esta información.</p>
			<br>
			<p>Si usted se ha registrado para una cuenta LosAlmendros.com.mx ya sea registrando su realizando alguna compra, nosotros le proporcionamos los medios para acceder su cuenta y editar su información personal incluyendo nombre, domicilio y número de teléfono, para asegurar que siempre sea exacta.</p>
			<br>
			<p>Si usted optó por recibir comunicaciones vía correo electrónico, proporcionando su dirección de correo electrónico en la página principal de LosAlmendros.com.mx, puede contactar a nuestra línea de atención al cliente al 01 52 999 9227844 para dar de baja su dirección.</p>
			<br>

			<p>Si envió una solicitud de empleo para una posición en Los Almendros, puede acceder al perfil personal que usted creó y realizar cualquier cambio o actualización que sea necesaria.</p>
			<br>
			<p>¿Por qué Los Almendros recopila información de uso del sitio web? Durante su visita a LosAlmendros.com.mx, recopilamos cierta información de uso del sitio web para diversos propósitos, tales como:</p>
			<br>
			<p>• Hacer que su acceso y compras sean más convenientes y eficientes.</p>
			<p>• Establecer tendencias de actividad del Sitio</p>
			<p>• Monitorear el desempeño del Sitio y</p>
			<p>• Mejorar el diseño y funcionalidad del Sitio.</p>
			<br>
			<p>Nuestra recopilación de información de uso del sitio web puede incluir la utilización de cookies. Una cookie es un archivo de datos pequeño que puede eliminarse y que se graba en el navegador de su computadora. Las cookies le permiten poner una orden en nuestro sitio web y nos permite mejorar y personalizar su navegación en línea y su experiencia de compra. Utilizamos cookies para recordar las compras en su carrito de compras, reconocerle cada vez que acceda a nuestro sitio web y analizar el comportamiento del visitante.</p>
			<br>
			<p>Puede elegir que su computadora le advierta cada vez que una cookie es enviada o puede seleccionar que se desactiven todas las cookies. Esto se hace a través de la configuración de su navegador (como Netscape Navigator o Internet Explorer). Cada navegador es un poco diferente, así que busque en el menú de ayuda de su navegador la configuración correcta de los parámetros de activación de cookies. Si desactiva las cookies, no tendrá acceso a muchas características que hacen más eficiente su experiencia en el sitio y algunos de nuestros servicios no funcionarán adecuadamente.</p>
			<br>
			<p>También utilizamos pixel tags – imágenes gráficas minúsculas – que nos ayudan a analizar su comportamiento en línea, pero no enlazamos este comportamiento a su información personal.</p>
			<br>
			<p>¿Cómo aseguran mi información personal? LosAlmendros se esfuerza en mantener una seguridad física apropiada, de procedimientos y técnica con respecto a sus oficinas e infraestructura de almacenamiento para prevenir cualquier pérdida, uso no adecuado, acceso no autorizado, divulgación, o modificación de información personal. Esto también se aplica a la eliminación o destrucción de información personal.</p>
			<br>
			<p>Información de niños LosAlmendros.com,mx es un sitio de audiencia general que no está diseñado ni tiene la intención de recopilar información personal de niños menores de 13 años. LosAlmendros no recolecta intencionalmente información de personas menores de 13 años de edad, así como tampoco utiliza esta información si descubre que ha sido proporcionada. Para respetar la privacidad de los niños y cumplir con la Ley de Protección de Privacidad de Niños En Línea, los niños menores de 13 años no deben proporcionar ninguna información personal a este Sitio. Solicitamos a los padres que supervisen a sus hijos cuando estén en línea.</p>
			<br>
			<p>¿Qué hay acerca de enlaces a otros sitios? Por favor note que nosotros proporcionamos enlaces o links a sitios web de terceros, como un servicio a nuestros invitados o clientes y que no somos responsables por el contenido o prácticas de recopilación de información de estas páginas. Por favor tenga en cuenta que las políticas de privacidad de estos sitios web pueden diferir de esta política de privacidad. Le recomendamos revisar y entender sus prácticas de privacidad antes de proporcionarles cualquier información.</p>
			<br>
			<p>Cambios en la política de privacidad. Si decidimos cambiar nuestra política de privacidad, colocaremos un nuevo anuncio en nuestro sitio y cambiaremos la fecha en la parte superior de nuestra política. Por lo tanto, le invitamos a verificar la fecha de nuestra política de privacidad cuando visite el Sitio, para conocer cualquier actualización o cambio. Si nos ha proporcionado información personal, le enviaremos por correo electrónico cualquier cambio de la política que pudiera afectar a la forma en la que utilizamos su información personal.</p>
			<br>
			<p>Preguntas y comentarios. Agradecemos nos envie sus preguntas, comentarios, y dudas acerca de la política de privacidad. Por favor envíenos cualquier pregunta o comentario concerniente a ella, o de cualquier otro tema.</p>
			<br>
			<p>En el caso de preguntas acerca de: (i) acceso a su información personal; (ii) nuestra recopilación, uso, manejo, o divulgación de información personal; (iii) política de privacidad, por favor contacte LosAlmendros enviando un correo electrónico a paracontactarnos@losalmendros.com.mx También puede contactar al departamento de Atención a Clientes llamando al 01 999 9227844 en México. Los Almendros investigará cualquier queja y si su queja está justificada, tomaremos las medidas necesarias para resolver su problema.</p>
			<br>
			<p>Términos y Condiciones. Por favor, visite nuestra sección de que explican otros términos que rigen el uso de este Sitio.</p>
		</div>
	</div>
</main>