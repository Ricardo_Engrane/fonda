<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Fonda Mexicana 2018</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" href="/css/estilos.min.css">
</head>
<body>

<header class="head-fonda">
	<div class="bg-head-fonda"></div>
	<nav class="head-nav">

		<div class="button-nav-mobile">
	        <div class="hamburger" id="hamburger-1">
				<span class="line"></span>
				<span class="line"></span>
				<span class="line"></span>
	        </div>			
		</div>

		<div class="menu-nav">
			<div class="menu-left">
				<a href="/nuestros-platillos">PLATILLOS</a>
				<a href="/historia-de-tradicion">HISTORIA DE TRADICIÓN</a>
			</div>
			<div class="menu-right">
				<a href="/organiza-tu-evento">EVENTOS</a>

				<a href="/#contacto" class="link-contacto">CONTACTO</a>

				<a href="https://api.whatsapp.com/send?phone=52155393340" target="_blank" class="reserva">RESERVA TU MESA<br><span>55 39 33 40</span></a>

				<div class="head-redes">
					<a href="#"><span class="ico-face"></span></a>
					<a href="#"><span class="ico-tw"></span></a>
				</div>
			</div>
		</div>
		
		<div class="logo-center">
			<div class="bg-logo">
				<img src="/assets/img/bg_logo_fonda.png" alt="Fonda Mexicana">
			</div>
			<a href="/">
				<img src="/assets/img/logo_fonda_mexicana_blanco.png" alt="Fonda Mexicana">	
			</a>
		</div>
		
	</nav>
</header>