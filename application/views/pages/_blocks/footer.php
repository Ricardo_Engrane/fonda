	<footer class="footer">
		<div class="left">
			<p>© Restaurante Fonda Mexicana. 2018 Todos los derechos reservados.</p>
		</div>
		<div class="right">
			<div>
				<a href="/aviso-de-privacidad">AVISO DE PRIVACIDAD</a>
			</div>
			<div>
				<a href="/terminos-y-condiciones">TERMINOS Y CONDICIONES</a>
			</div>
			<div>
				<a href="https://www.cmr.mx/" target="_blank"><img src="/assets/img/cmr_ico_small.png" alt=""></a>
			</div>
			
		</div>
	</footer>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	
	<script type="text/javascript" src="/js/scripts.all.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBx2L4B7_FTk4hkR0poC5u9r5PYpSvJZCE&callback=initMap"></script>
</body>
</html>