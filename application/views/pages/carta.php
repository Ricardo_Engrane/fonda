<main class="nuestros-platillos">
	<div class="platillos-intro">
		<div class="wrap-intro">			
			<div class="intro-left">
				<div class="icon"><img src="/assets/img/historia/ico_oaxaca.png" alt="Fonda Mexicana oaxaca"></div>
				<div class="icon"><img src="/assets/img/historia/ico_puebla.png" alt="Fonda Mexicana puebla"></div>
				<div class="icon"><img src="/assets/img/historia/ico_sinaloa.png" alt="Fonda Mexicana sinaloa"></div>
				<div class="icon"><img src="/assets/img/historia/ico_michoacan.png" alt="Fonda Mexicana michoacan"></div>
				<div class="icon"><img src="/assets/img/historia/ico_tabasco.png" alt="Fonda Mexicana tabasco"></div>
			</div>
			<div class="intro-center">
				<h1>SABOR CON ESENCIA<br>DE MÉXICO</h1>
				<div class="wrap-text">
					<p>FONDA MEXICANA reúne la riqueza gastronómica nacional, con innovadores platillos e ingredientes que enaltecen la variedad y sabor de nuestra cultura.</p>
					<p>En Fonda Mexicana retomamos la esencia de cada región mediante un recorrido culinario por la República Mexicana, generando grandes experiencias, que al fusionarlas con novedosas creaciones, deleitan el paladar.</p>
					<p>En Fonda Mexicana preparamos platillos de calidad con ingredientes de origen 100% mexicano que brindan en cada visita una explosión de aromas, texturas, colores y sabores.</p>
				</div>
				<div class="foot-intro">
					<div class="icon"><img src="/assets/img/historia/ico_chiapas.png" alt="Fonda Mexicana chiapas"></div>
					<div class="icon"><img src="/assets/img/historia/ico_guerrero.png" alt="Fonda Mexicana guerrero"></div>
					<div class="icon"><img src="/assets/img/historia/ico_veracruz.png" alt="Fonda Mexicana veracruz"></div>
				</div>
			</div>
			<div class="intro-right">
				<div class="icon"><img src="/assets/img/historia/ico_yucatan.png" alt="Fonda Mexicana yucatan"></div>
				<div class="icon"><img src="/assets/img/historia/ico_tlaxcala.png" alt="Fonda Mexicana tlaxcala"></div>
				<div class="icon"><img src="/assets/img/historia/ico_morelos.png" alt="Fonda Mexicana morelos"></div>
				<div class="icon"><img src="/assets/img/historia/ico_jalisco.png" alt="Fonda Mexicana jalisco"></div>
				<div class="icon"><img src="/assets/img/historia/ico_bajio.png" alt="Fonda Mexicana bajio"></div>
			</div>
		</div>
	</div>

	<div class="menu">

		<?php if($menu == 'desayuno') { ?>
		<!-- Desayunos -->
		<div class="head-menu">
			<h2>DESAYUNOS</h2>
			<p>HORARIO 7:30AM - 12:00PM</p>
		</div>
		<div class="item-menu">
			<div class="wrap-bg">
				<div class="bg bg--huevosculiacan"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--right"><p>Huevos Culiacán</p></div>
			</div>
			<div class="item-text item-text--rosa">
				<h2>HUEVOS</h2>

				<p><span>Revueltos con rajas poblanas</span><br>
				Servido en cazuela con salsa molcajeteada y granos de elote</p>

				<p><span>En salsa pasilla</span><br>
				Servidos en cazuela con salsa pasilla y cubos de queso panela</p>

				<p><span>Divorciados</span><br>
				Dos huevos fritos sobre chilaquiles bañados en 2 salsas: roja y verde, acompañados de<br>frijoles refritos</p>

				<p><span>Rancheros</span><br>
				Dos huevos fritos sobre una tortilla bañada en salsa ranchera, acompañados de<br> frijoles refritos</p>

				<p><span>Ahogados en salsa de guajillo</span><br>
				A la cazuela, en salsa de guajillo con nopales y cilantro</p>

				<p><span>Mineros</span><br>
				Revueltos con chicharrón y frijoles de la olla</p>

				<p><span>Tacos Revolcados</span><br>
				De huevo revuelto con nuestras deliciosas carnitas picadas con pico de gallo, servidos<br>co</p>

				<p><span>Al gusto</span><br>
				Fritos, tibios, a la mexicana, revueltos<br>
				A elegir con un ingrediente<br>
				Queso panela, Jamón, Jamón de pavo, Tocino, Salchicha </p>

				<p><span>Huevos istmeños</span><br>
				Dos huevos fritos sobre una tortilla bañados en mole negro, acompañado<br>
				de frijoles refritos</p>

				<p><span>Huevos Culiacán</span><br>
				Revueltos con chilorio a la mexicana, frijoles refritos y queso panela asado</p>


			</div>
		</div>
		<div class="item-menu">
			<div class="item-text item-text--pl">
				<h2>LIGEROS<br>Y SALUDABLES</h2>

				<p><span>Omelette de claras con rajas y queso panela</span><br>
				Sobre nuestra especial salsa molcajeteada, acompañado<br>
				de jitomate y rajas poblanas</p>

				<p><span>Quesadilla en comal</span><br>
				En tortilla de maíz, preparadas con queso Oaxaca con relleno<br>
				de flor de calabaza, chicharrón, champiñones o rajas</p>

				<h2>OMELETTES</h2>

				<p><span>Gratinado con champiñones y flor de calabaza</span><br>
				Sobre una cremosa salsa poblana, acompañada de frijoles<br>
				y tortillas calientitas</p>

				<p><span>De claras con nopales a la mexicana</span><br>
				Acompañadas de salsa molcajeteada y frijoles refritos</p>

				<p><span>Omelette de pollo con mole</span><br>
				Se sirve con frijoles refritos</p>

				<h2>CHILAQUILES</h2>

				<p><span>Chilaquiles verdes o rojos</span><br>
				Con totopos horneados acompañados de pollo<br>
				Con huevo<br>
				Con pollo gratinados</p>

			</div>
			<div class="bg-content">
				<div class="wrap-bg">
					<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
					<div class="bg bg--omelettemole"></div>
					<div class="caption caption--left"><p>Omelette de Pollo con Mole</p></div>
				</div>
				<div class="info-add">
					<h2>NATAS Y PAN DE LA CASA</h2>
					<p><span>Pan dulce recién horneado<br>
					Orden de nata<br>
					Cuerno o concha con nata</span></p>
				</div>
			</div>
		</div>
		<div class="item-menu">
			<div class="wrap-bg">
				<div class="bg bg--tacospipan"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--right"><p>Taquitos de Pollo Bañados en Pipián Verde</p></div>
			</div>
			<div class="item-text item-text--morado">
				<h2>ESPECIALIDADES</h2>
				
				<p><span>Taco pueblero (1 PIEZA)</span><br>
				Con arrachera picada, frijoles refritos, chicharrón revuelto en salsa molcajeteada,<br>
				acompañado de guacamole</p>

				<p><span>Enmoladas con pollo ( 3 PIEZAS)</span><br>
				Tres tortillas de maíz rellenas con pollo, bañadas con mole de la casa<br>
				y queso fresco espolvoreado</p>

				<p><span>Enfrijoladas</span><br>
				Rellenas de pollo o huevo a la mexicana, servidas con chorizo, chile serrano y crema.<br>
				Pídelas con queso fresco o gratinadas</p>

				<p><span>Molletes (4 PIEZAS)</span><br>
				Untados con frijoles refritos y queso gratinado, acompañados de salsa mexicana</p>

				<p><span>Molletes mixtos (4 PIEZAS)</span><br>
				Rajas, jamón, chorizo y queso, acompañados de salsa mexicana</p>

				<p><span>Pastel Azteca</span><br>
				Gratinado, preparado con tortillas de maíz recién hechas, relleno de pollo, rajas<br>
				poblanas y elote. Servido sobre una salsa de chile chipotle</p>

				<p><span>Cecina de res Yecapixtla</span><br>
				Acompañados de guacamole, frijoles refritos, crema y tortillas de comal</p>

				<p><span>Taquitos de pollo bañados en pipián verde (3 PIEZAS)</span><br>
				Tacos dorados de pollo bañados con pipián verde y queso acompañados de frijoles de<br>
				la olla y cebollita morada en escabeche</p>

				<p><span>Sopes el fuerte</span><br>
				De chilorio, acompañados con un tamal de elote frito y gratinado ocn con rajas, huevos<br>
				estrellados con salsa molcajeteada y frijoles refritos</p>

				<p><span>Enfrijoladas</span><br>
				Rellenas de huevo a la mexicana y queso fresco<br>
				Con huevo gratinadas<br>
				Con pollo gratinadas</p>

			</div>			
		</div>
		<div class="item-menu">
			<div class="item-text-column">
				<div class="head-item head-item--pl">
					<div>
						<img src="/assets/img/platillos/recomendacion_mayora_morado.png" alt="Fonda Mexicana Recomendaciones de la Mayora">
					</div>
				</div>

				<div class="item-columns item-columns--pl">
					<div class="column">
						<p><span>Nopal asado con queso panela</span><br>
						Acompañado de jamón de pavo asado<br>
						y rebanadas de jitomate.</p>

						<p><span>Enchiladas de Pollo</span><br>
						Sin freír, bañadas en salsa verde o rojo<br>
						Pídelas gratinadas o con queso fresco.</p>

						<p><span>Omelette de huitlacoche</span><br>
						con salsa poblana<br>
						Acompañado de frijoles refritos.</p>

						<p><span>Omelette Veracruz</span><br>
						Relleno de chorizo y queso fresco, sobre<br> 
						salsa de frijol  con chile serrano y queso<br>
						panela asado.</p>

						<p><span>La tradicional Machaca</span><br>
						con Huevo<br>
						Con tortilla de harina calientitas<br>
						acompañadas de frijoles y salsa<br>
						molcajeteada.</p>

					</div>
					<div class="column">
						<p><span>Tacos de huevo con tiritas</span><br>
						de cecina en salsa morita<br>
						Acompañados de frijoles refritos</p>

						<p><span>Chalupas</span><br>
						Dos huevos fritos acompañados<br>
						acompañados de nuestras famosas<br>
						chalupas, una salsa verde<br>
						y otra con salsa roja</p>

						<p><span>Enchiladas rojas estilo<br>San Luis</span><br>
 						Rellenas de papa con queso Oaxaca,<br>
 						bañadas en salsa de guajillo; servidas<br>
 						con lechuga, queso fresco, crema<br>
 						y rábano</p> 

						<p><span>Tacos de cochinita Pibil</span><br>
						En tortillas de maíz recién hechas,<br>
						servidas con cebolla morada y chiles<br>
						habernos, acompañados de frijoles<br>
						refritos y queso fresco</p>

						<p><span>Café lechero</span><br>
						El clásico café lechero de Veracruz</p>
					</div>
				</div>
			</div>
			<div class="wrap-bg">
				<div class="bg bg--huevoschorizo"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--left"><p>Huevos al Gusto</p></div>
			</div>
		</div>
		<div class="item-menu">
			<div class="bg-content">
				<div class="info-add info-add--pl">
					<h2>FRUTAS<br>Y CEREALES</h2>
					<p>Plato de fruta<br>
					Melón, sandía, piña y papaya<br>
					Con yogurt, granola o queso cottage<br>
					Con queso cottage y granola o yogurt y granola</p>
				</div>
				<div class="wrap-bg">
					<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
					<div class="bg bg--chocolate"></div>
					<div class="caption caption--left"><p>Chocolate Caliente</p></div>
				</div>
			</div>
			<div class="item-text">

				<h2>JUGOS<br>
				NATURALES</h2>

				<p>Naranja, toronja, zanahoria y verde.<br>
				Vaso (300 ml.) Jarrita (480 ml.)</p>

				<p>Jugos combinados<br>
				Fresa-Naranja, Piña-Fresa, Toronja-Piña,<br>
				Zahoria-Naranja, Verde-Toronja<br>
				Vaso (300 ml.) Jarrita (480 ml.)</p>

				<h2>BEBIDAS<br>
				CALIENTES</h2>

				<p>Chocolate caliente (300 ml.)<br>
				Batido con molinillo<br>
				Café de olla (310 ml.)<br>
				Café americano (180 ml.)<br>
				Café express (60 ml.)<br>
				Té (240 ml.) Pregunta por nuestra cajita de tés <br>
				Café capuchino (180 ml.)<br>
				Café capuchino Baileys (180 ml.)<br>
				Capuchino de sabores (180 ml.)<br>
				Chocolate, cajeta y rompope<br>
				Vaso de leche (300 ml.)<br>
				Pídelo frío o caliente</p>
			</div>
		</div>
		<!-- end Desayunos -->
		<!-- Comidas -->
		<div class="head-menu">
			<h2>COMIDAS</h2>
			<p>HORARIO 12:00PM - 22:30PM</p>
		</div>
		<div class="item-menu">
			<div class="item-text item-text--pl">
				<h2>MOLES</h2>
				<p><span>Nuestras deliciosas opciones<br>
				para disfrutar la mejor combinación<br>
				Platillos para acompañar tu mole:</span></p>

				<p class="medium">
				<span>- Pechuga de pollo<br>
				- Pierna y muslo de pollo</span></p>
				 
				<p><span>Elige el mole de tu preferencia:</span></p>

				<p class="medium">
				<span>- Mole de huitlacoche<br>
				- Mole negro<br>
				- Mole de ciruela<br> 
				- Mole verde <br>
				- Mole de pistache<br>
				- Mole poblano</span></p>

				<p><span>Acompaña tu platillo con arroz blanco<br>
				o rojo y frijoles de la olla o refritos.</span></p>
			</div>
			<div class="wrap-bg">
				<div class="bg bg--mole"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--left"><p>Mole Poblano</p></div>
			</div>
		</div>
		<div class="item-menu">
			<div class="wrap-bg">
				<div class="bg bg--botanero"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--right"><p>Plato Botanero</p></div>
			</div>
			<div class="item-text item-text--rosa">
				<h2>ANTOJITOS</h2>

				<p><span>Plato botanero  (PARA COMPARTIR)</span><br>
				Esplendor de botanas mexicanas, 1 tlacoyo de frijol, 1 gordita de chicharrón,<br>
				1 sope,1 empanadita de pollo con mole, 1 tortita de plátano macho</p>

				<p><span>Elige cualquier opción de nuestro Plato botanero por pieza o por orden:</span></p>

				<p>Tortita de plátano macho (1 PIEZA)<br>
				Empanaditas de pollo con mole (2 PIEZAS)<br> 
				Tlacoyo de frijol (1 PIEZA)<br>
				Gordita de chicharrón (1 PIEZA)<br>
				Sope (1 PIEZA) </p>

				<p><span>La patrona (1 PIEZA)</span><br>
				Tortilla de maíz servida con carne de res, pollo o cerdo; gratinada.<br>
				A elegir con salsa roja o verde</p>

				<p><span>Chalupas de la fonda (2 PIEZAS)</span><br>
				De carne deshebrada de res, cerdo o pollo con salsa roja o verde</p>

				<p><span>Quesadillas de cazón (3 PIEZAS)</span><br>
				¡Doraditas! Rellenas de cazón, preparadas con cebolla, jitomate y epazote,<br> acompañadas con guacamole</p>

				<p><span>Quesadilla al comal (1 PIEZA)</span><br>
				En tortilla de maíz, preparadas con queso Oaxaca con relleno de flor de<br> calabaza, chicharrón, champiñones o rajas</p>

				<p><span>Queso fundido con chorizo</span><br>
				Con un toque de vino blanco, servido con tortillas recién hechas de maíz o harina</p>

				<p><span>Tacos de lengua (2 PIEZAS)</span><br>
				Con cilantro y cebolla, acompañados de salsa verde cocida</p>

				<p><span>Taco pueblero (1 PIEZA)</span><br>
				Con arrachera picada, frijoles refritos y chicharrón revuelto en<br> salsa molcajeteada, acompañado de guacamole</p>

				<p><span>Tostada de pata (1 PIEZA)</span><br>
				Con frijoles, lechuga, crema y queso servida con salsa verde cocida</p>

				<p><span>Taco Gober de camarón</span><br>
				En tortilla de harina con quesillo y machaca de camarón</p>

				<p><span>Taco Gober de marlín</span><br>
				En tortilla de maíz con quesillo y machaca de marlín ahumado</p>

				<p><span>Panucho (1 PIEZA)</span><br>
				Tortilla de maíz rellena de frijoles, con nuestra cochinita pibil, hecha con<br>ingredientes traídos directo de la península yucateca; servido con cebolla<br> morada y chile habanero</p>
			</div>
		</div>
		<div class="item-menu">
			<div class="item-text item-text--pl">
				<h2>SOPAS</h2>
				<p><span>Crema de chile poblano</span><br>
				Con tiritas de queso Oaxaca, granos de elote y rajas</p>

				<p><span>Fideo seco al chipotle</span><br>
				Con aguacate, crema y queso fresco rallado</p>

				<p><span>Consomé de pollo</span><br>
				Con arroz y verduras</p>

				<p><span>Arroz con mole<br>
				Pide tu arroz con cualquiera<br>
				de nuestros moles:</span></p>

				<p>
				Mole de huitlacoche<br>
				Mole de ciruela<br>
				Mole de pistache<br>
				Mole negro<br>
				Mole verde<br>
				Mole poblano<br>
				</p>

				<p><span>Crema de esquites</span><br>
				Con quesillo, juliana de tortilla y esencia de epazote</p>

				<p><span>Sopa de hongos</span><br>
				¡Ligera y picosita! Con un poco de requesón y flor de calabaza</p>

				<h2>ENSALADAS</h2>

				<p><span>Ensalada de berros</span><br>
				Berros, jitomate, tocino, cebolla, aguacate y aderezo de miel<br>
				con cítricos</p>

				<p><span>Ensalada Azteca</span><br>
				Mezcla de lechugas con juliana de tortilla, jitomate y queso fresco;<br>
				con aderezo de miel con cítricos y destellos de betabel</p>

				<p><span>Ensalada de nopales</span><br>
				Nopales con queso panela, jitomate, aguacate y cebolla, servida<br>
				con vinagreta de chile costeño</p>
			</div>
			<div class="bg-content">
				<div class="wrap-bg">
					<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
					<div class="bg bg--guacamole"></div>
					<div class="caption caption--left"><p>Guacamole con Chapulines</p></div>
				</div>
				<div class="info-add">
					<h2>GUACAMOLES</h2>
					<p><span>De la casa (con chicharrón)</span><br> 
					<span>Con chapulines</span></p>
				</div>
			</div>
		</div>
		<div class="item-menu">
			<div class="wrap-bg">
				<div class="bg bg--enchiladas"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--right"><p>Enchiladas Rojas estilo San Luis</p></div>
			</div>
			<div class="item-text item-text--morado">
				<h2>ESPECIALIDADES</h2>
				<p><span>Enchiladas de requesón en salsa verde (3 PIEZAS)</span><br>
				Rellenas de requesón al epazote, bañadas con crema y queso rallado,<br>acompañadas de frijoles de la olla</p>

				<p><span>Tacos de arrachera (6 PIEZAS)</span><br>
				Suave y jugosa arrachera picada, servida con guacamole, cebollitas cambray asadas,<br> cilantro y cebolla</p>

				<p><span>Chiles tapatíos</span><br>
				Uno de queso panela y otro de picadillo, bañados con caldillo de jitomate </p>

				<p><span>Chile poblano</span><br>
				 Relleno de plátano macho con queso panela en salsa de chile ancho y ajonjolí</p> 

				<p><span>Tacos de cochinita pibil (3 PIEZAS)</span><br>
				En tortillas de maíz recién hechas, servidos con cebolla morada y chile habanero,<br> acompañados de frijoles refritos y queso rallado</p>


				<p><span>Enchiladas rojas estilo San Luis (3 PIEZAS)</span><br>
				Rellenas de papa con queso Oaxaca, bañadas en salsa de chile guajillo; servidas con<br> lechuga, queso, crema y rábano</p>


				<p><span>Enchiladas 2 moles (3 PIEZAS)</span><br>
				 Lo mejor de 2 mundos. Rellenas de pollo, cubiertas de mole poblano y pipián verde,<br> acompañadas de frijoles de la olla, hechos a diario en casa</p> 
			</div>			
		</div>
		<div class="item-menu">
			<div class="item-text-column item-text-column--rosa">
				<div class="head-item">
					<div>
						<img src="/assets/img/platillos/recomendaciones_mayora.png" alt="Fonda Mexicana Recomendaciones de la Mayora">
					</div>
				</div>

				<div class="item-columns">
					<div class="column">
						<p><span>Atún con costra de chapulines<br>
						oaxaqueños</span><br> 
						Acompañado de ensalada verde con dulce reducción<br>
						de vinagre balsámico</p>

						<p><span>Filete de pescado a la  veracruzana</span><br> 
						Con nuestra salsa estilo Veracruz hecha con jitomate,<br>
						cebolla, pimientos y aceitunas; servida con arroz<br>
						blanco y tortillas recién hechas</p>

						<p><span>Arroz a la tumbada</span><br>
						Delicioso arroz caldoso con mariscos (pulpo, calamar,<br> 
						camarón, pescado y almeja)</p>

						<p><span>Pastel Azteca</span><br>
						Gratinado; preparado con tortillas de maíz recién hechas,<br>
						relleno de pollo, rajas poblanas y elote. Servido sobre una<br>
						salsa de chile chipotle</p>

						<p><span>Rollitos de pechuga en salsa morita</span><br>
						Rellenos de requesón con epazote, servidos sobre salsa de<br>
						chile morita, acompañada de arroz blanco</p>

						<p><span>Sopa de tortilla</span><br>
						La tradicional, con aguacate, chile pasilla frito,<br> 
						chicharrón, crema y queso</p>
					</div>
					<div class="column">
						<p><span>Molcajete de arrachera (200 GRS.)</span><br>
						Se sirve con queso asado, nopales, cebollitas cambray<br>
						y frijoles de la olla, acompañado de tortillas recién hechas</p>

						<p><span>Lengua a la veracruzana (180 GRS.)</span><br>
						Con nuestra salsa estilo Veracruz, hecha con jitomate,<br>
						cebolla, pimientos y aceitunas, servida con arroz blanco<br>
						y tortillas recién hechas</p>

						<p><span>Cecina de res Yecapixtla  (160 GRS.)</span><br>
						Acompañada de nopal asado, cebollitas cambray,<br>
						guacamole, crema y tortillas del comal</p>

						<p><span>Ensalada de jitomate con queso Oaxaca</span><br>
						Lechugas frescas con queso Oaxaca y jitomate,<br>
						bañadas con vinagreta  de chile costeño</p>

						<p><span>Chile queretano</span><br>
						Chile poblano hojaldrado, relleno de pollo con requesón en<br>
						salsa de flor de calabaza y salsa de frijol, espolvoreado con elote<br>
						tatemado y queso fresco</p>

						<p><span>Flautas de jamaica (3 PIEZAS)</span><br>
						Flautas doradas de pollo con salsa de jamaica al chipotle,<br>
						bañadas de queso, crema y frituras de jamaica</p>
					</div>
				</div>
			</div>
			<div class="wrap-bg">
				<div class="bg bg--molcajete"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--left"><p>Molcajete de Arrachera</p></div>
			</div>
		</div>
		<div class="item-menu">
			<div class="wrap-bg">
				<div class="bg bg--camarones"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--right"><p>Camarones con Chipotle de Papantla</p></div>
			</div>
			<div class="item-text">
				<h2>POLLO</h2>

				<p><span>Molcajete de fajitas de pollo (200 g)</span><br>
				Se sirve con queso asado, nopales, cebollitas cambray y frijoles de la olla,<br>
				acompañado de tortillas recién hechas</p>

				<p><span>Pechuga de pollo a la parrilla (200 g)</span><br>
				Servida con nopales preparados, cebollitas cambray y arroz a la mexicana</p>

				<p><span>Milanesa de pollo (120 g)</span><br>
 				Empanizada, acompañada de arroz blanco, frijoles de la olla<br>
 				y tortillas recién hechas</p> 

				<p><span>Pechuga rellena de plátano macho en mole poblano</span><br>
				Un deleite al paladar. Preparada con plátano macho horneado, bañada<br>
				con mole poblano (receta de la casa), servida con frijoles de la olla y arroz blanco</p>

				<h2>PESCADOS</h2>

				<p><span>Filete de pescado al limón (200 g)</span><br>
				A la plancha, bañado con una deliciosa salsa al limón con alcaparras.<br>
				Acompañado de arroz blanco</p>

				<p><span>Filete de huachinango estilo talla (200 g)</span><br>
				Acompañado de elote de la plaza y ensalada de nopales<br>
				con chicharrón fresco</p>

				<p><span>Posta de Huachinango (180 g)</span><br>
				Sobre puré de papa con aceite de flor de albahaca acompañado<br>
				de brochetas de verduras a la parrilla</p>

				<p><span>Mojarra Boca del Río (200 g  A 350 g)</span><br>
				Mojarra entera en salsa de cilantro al limón, acompañada<br>
				de arroz blanco</p>

				<p><span>Camarones al gusto (12 PIEZAS)</span><br>
				Elige cualquiera de nuestras preparaciones veracruzanas.<br>
				Al mojo de ajo, con el sabor único de Tlacotalpan. Enchipoclados,<br>
				con el sabor semidulce con chipotle de Papantla</p>
			</div>		
		</div>
		<div class="item-menu">
			<div class="item-text item-text--pl">
				<h2>CARNES</h2>
				<p><span>Arrachera a la parrilla (200 g) Calidad Choice</span><br>
				Acompañada de arroz a la mexicana y nopales preparados</p>

				<p><span>Tampiqueña de la fonda (200 g)</span><br>
				Asada, servida con una enmolada poblana y otra de pipián,<br>
				acompañada de arroz a la mexicana</p>
					
				<p><span>Milanesa de res (120 g)</span><br>
				Empanizada, acompañada de arroz blanco, frijoles de la olla<br>
				y tortillas recién hechas</p>

				<p><span>Albóndigas al chipotle estilo casero (3 PIEZAS/210 g)</span><br>
				Con nuestra tradicional salsa de chipotle, servidas con arroz blanco<br>
				y frijoles de la olla</p>


				<p><span>Chamorro al pibil (600 g), ideal para compartir</span><br>
				Abundante porción de chamorro picado para taquear, acompañado<br>
				de cebolla morada y chile habanero. Se sirve sobre hoja de plátano</p>


				<p><span>T-Bone (350 g)<br>
				Rib Eye (300 g)</span></p>

				<p><span>*Todos nuestros cortes son Calidad Choice</span></p>

				<p><span>Acompaña tu corte con cualquiera<br>
				de nuestras guarniciones:</span></p>

				<p>Espinacas a la crema<br>
				Papas a la francesa<br>
				Puré de papa con tocino y parmesano</p>
			</div>
			<div class="wrap-bg">
				<div class="bg bg--chamorro"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--left"><p>Chamorro pibil</p></div>
			</div>
		</div>
		<!-- end Comidas -->

		<?php }elseif ($menu == 'comida') { ?>	
		<!-- Comidas -->
		<div class="head-menu">
			<h2>COMIDAS</h2>
			<p>HORARIO 12:00PM - 22:30PM</p>
		</div>
		<div class="item-menu">
			<div class="item-text item-text--pl">
				<h2>MOLES</h2>
				<p><span>Nuestras deliciosas opciones<br>
				para disfrutar la mejor combinación<br>
				Platillos para acompañar tu mole:</span></p>

				<p class="medium">
				<span>- Pechuga de pollo<br>
				- Pierna y muslo de pollo</span></p>
				 
				<p><span>Elige el mole de tu preferencia:</span></p>

				<p class="medium">
				<span>- Mole de huitlacoche<br>
				- Mole negro<br>
				- Mole de ciruela<br> 
				- Mole verde <br>
				- Mole de pistache<br>
				- Mole poblano</span></p>

				<p><span>Acompaña tu platillo con arroz blanco<br>
				o rojo y frijoles de la olla o refritos.</span></p>
			</div>
			<div class="wrap-bg">
				<div class="bg bg--mole"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--left"><p>Mole Poblano</p></div>
			</div>
		</div>
		<div class="item-menu">
			<div class="wrap-bg">
				<div class="bg bg--botanero"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--right"><p>Plato Botanero</p></div>
			</div>
			<div class="item-text item-text--rosa">
				<h2>ANTOJITOS</h2>

				<p><span>Plato botanero  (PARA COMPARTIR)</span><br>
				Esplendor de botanas mexicanas, 1 tlacoyo de frijol, 1 gordita de chicharrón,<br>
				1 sope,1 empanadita de pollo con mole, 1 tortita de plátano macho</p>

				<p><span>Elige cualquier opción de nuestro Plato botanero por pieza o por orden:</span></p>

				<p>Tortita de plátano macho (1 PIEZA)<br>
				Empanaditas de pollo con mole (2 PIEZAS)<br> 
				Tlacoyo de frijol (1 PIEZA)<br>
				Gordita de chicharrón (1 PIEZA)<br>
				Sope (1 PIEZA) </p>

				<p><span>La patrona (1 PIEZA)</span><br>
				Tortilla de maíz servida con carne de res, pollo o cerdo; gratinada.<br>
				A elegir con salsa roja o verde</p>

				<p><span>Chalupas de la fonda (2 PIEZAS)</span><br>
				De carne deshebrada de res, cerdo o pollo con salsa roja o verde</p>

				<p><span>Quesadillas de cazón (3 PIEZAS)</span><br>
				¡Doraditas! Rellenas de cazón, preparadas con cebolla, jitomate y epazote,<br> acompañadas con guacamole</p>

				<p><span>Quesadilla al comal (1 PIEZA)</span><br>
				En tortilla de maíz, preparadas con queso Oaxaca con relleno de flor de<br> calabaza, chicharrón, champiñones o rajas</p>

				<p><span>Queso fundido con chorizo</span><br>
				Con un toque de vino blanco, servido con tortillas recién hechas de maíz o harina</p>

				<p><span>Tacos de lengua (2 PIEZAS)</span><br>
				Con cilantro y cebolla, acompañados de salsa verde cocida</p>

				<p><span>Taco pueblero (1 PIEZA)</span><br>
				Con arrachera picada, frijoles refritos y chicharrón revuelto en<br> salsa molcajeteada, acompañado de guacamole</p>

				<p><span>Tostada de pata (1 PIEZA)</span><br>
				Con frijoles, lechuga, crema y queso servida con salsa verde cocida</p>

				<p><span>Taco Gober de camarón</span><br>
				En tortilla de harina con quesillo y machaca de camarón</p>

				<p><span>Taco Gober de marlín</span><br>
				En tortilla de maíz con quesillo y machaca de marlín ahumado</p>

				<p><span>Panucho (1 PIEZA)</span><br>
				Tortilla de maíz rellena de frijoles, con nuestra cochinita pibil, hecha con<br>ingredientes traídos directo de la península yucateca; servido con cebolla<br> morada y chile habanero</p>
			</div>
		</div>
		<div class="item-menu">
			<div class="item-text item-text--pl">
				<h2>SOPAS</h2>
				<p><span>Crema de chile poblano</span><br>
				Con tiritas de queso Oaxaca, granos de elote y rajas</p>

				<p><span>Fideo seco al chipotle</span><br>
				Con aguacate, crema y queso fresco rallado</p>

				<p><span>Consomé de pollo</span><br>
				Con arroz y verduras</p>

				<p><span>Arroz con mole<br>
				Pide tu arroz con cualquiera<br>
				de nuestros moles:</span></p>

				<p>
				Mole de huitlacoche<br>
				Mole de ciruela<br>
				Mole de pistache<br>
				Mole negro<br>
				Mole verde<br>
				Mole poblano<br>
				</p>

				<p><span>Crema de esquites</span><br>
				Con quesillo, juliana de tortilla y esencia de epazote</p>

				<p><span>Sopa de hongos</span><br>
				¡Ligera y picosita! Con un poco de requesón y flor de calabaza</p>

				<h2>ENSALADAS</h2>

				<p><span>Ensalada de berros</span><br>
				Berros, jitomate, tocino, cebolla, aguacate y aderezo de miel<br>
				con cítricos</p>

				<p><span>Ensalada Azteca</span><br>
				Mezcla de lechugas con juliana de tortilla, jitomate y queso fresco;<br>
				con aderezo de miel con cítricos y destellos de betabel</p>

				<p><span>Ensalada de nopales</span><br>
				Nopales con queso panela, jitomate, aguacate y cebolla, servida<br>
				con vinagreta de chile costeño</p>
			</div>
			<div class="bg-content">
				<div class="wrap-bg">
					<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
					<div class="bg bg--guacamole"></div>
					<div class="caption caption--left"><p>Guacamole con Chapulines</p></div>
				</div>
				<div class="info-add">
					<h2>GUACAMOLES</h2>
					<p><span>De la casa (con chicharrón)</span><br> 
					<span>Con chapulines</span></p>
				</div>
			</div>
		</div>
		<div class="item-menu">
			<div class="wrap-bg">
				<div class="bg bg--enchiladas"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--right"><p>Enchiladas Rojas estilo San Luis</p></div>
			</div>
			<div class="item-text item-text--morado">
				<h2>ESPECIALIDADES</h2>
				<p><span>Enchiladas de requesón en salsa verde (3 PIEZAS)</span><br>
				Rellenas de requesón al epazote, bañadas con crema y queso rallado,<br>acompañadas de frijoles de la olla</p>

				<p><span>Tacos de arrachera (6 PIEZAS)</span><br>
				Suave y jugosa arrachera picada, servida con guacamole, cebollitas cambray asadas,<br> cilantro y cebolla</p>

				<p><span>Chiles tapatíos</span><br>
				Uno de queso panela y otro de picadillo, bañados con caldillo de jitomate </p>

				<p><span>Chile poblano</span><br>
				 Relleno de plátano macho con queso panela en salsa de chile ancho y ajonjolí</p> 

				<p><span>Tacos de cochinita pibil (3 PIEZAS)</span><br>
				En tortillas de maíz recién hechas, servidos con cebolla morada y chile habanero,<br> acompañados de frijoles refritos y queso rallado</p>


				<p><span>Enchiladas rojas estilo San Luis (3 PIEZAS)</span><br>
				Rellenas de papa con queso Oaxaca, bañadas en salsa de chile guajillo; servidas con<br> lechuga, queso, crema y rábano</p>


				<p><span>Enchiladas 2 moles (3 PIEZAS)</span><br>
				 Lo mejor de 2 mundos. Rellenas de pollo, cubiertas de mole poblano y pipián verde,<br> acompañadas de frijoles de la olla, hechos a diario en casa</p> 
			</div>			
		</div>
		<div class="item-menu">
			<div class="item-text-column item-text-column--rosa">
				<div class="head-item">
					<div>
						<img src="/assets/img/platillos/recomendaciones_mayora.png" alt="Fonda Mexicana Recomendaciones de la Mayora">
					</div>
				</div>

				<div class="item-columns">
					<div class="column">
						<p><span>Atún con costra de chapulines<br>
						oaxaqueños</span><br> 
						Acompañado de ensalada verde con dulce reducción<br>
						de vinagre balsámico</p>

						<p><span>Filete de pescado a la  veracruzana</span><br> 
						Con nuestra salsa estilo Veracruz hecha con jitomate,<br>
						cebolla, pimientos y aceitunas; servida con arroz<br>
						blanco y tortillas recién hechas</p>

						<p><span>Arroz a la tumbada</span><br>
						Delicioso arroz caldoso con mariscos (pulpo, calamar,<br> 
						camarón, pescado y almeja)</p>

						<p><span>Pastel Azteca</span><br>
						Gratinado; preparado con tortillas de maíz recién hechas,<br>
						relleno de pollo, rajas poblanas y elote. Servido sobre una<br>
						salsa de chile chipotle</p>

						<p><span>Rollitos de pechuga en salsa morita</span><br>
						Rellenos de requesón con epazote, servidos sobre salsa de<br>
						chile morita, acompañada de arroz blanco</p>

						<p><span>Sopa de tortilla</span><br>
						La tradicional, con aguacate, chile pasilla frito,<br> 
						chicharrón, crema y queso</p>
					</div>
					<div class="column">
						<p><span>Molcajete de arrachera (200 GRS.)</span><br>
						Se sirve con queso asado, nopales, cebollitas cambray<br>
						y frijoles de la olla, acompañado de tortillas recién hechas</p>

						<p><span>Lengua a la veracruzana (180 GRS.)</span><br>
						Con nuestra salsa estilo Veracruz, hecha con jitomate,<br>
						cebolla, pimientos y aceitunas, servida con arroz blanco<br>
						y tortillas recién hechas</p>

						<p><span>Cecina de res Yecapixtla  (160 GRS.)</span><br>
						Acompañada de nopal asado, cebollitas cambray,<br>
						guacamole, crema y tortillas del comal</p>

						<p><span>Ensalada de jitomate con queso Oaxaca</span><br>
						Lechugas frescas con queso Oaxaca y jitomate,<br>
						bañadas con vinagreta  de chile costeño</p>

						<p><span>Chile queretano</span><br>
						Chile poblano hojaldrado, relleno de pollo con requesón en<br>
						salsa de flor de calabaza y salsa de frijol, espolvoreado con elote<br>
						tatemado y queso fresco</p>

						<p><span>Flautas de jamaica (3 PIEZAS)</span><br>
						Flautas doradas de pollo con salsa de jamaica al chipotle,<br>
						bañadas de queso, crema y frituras de jamaica</p>
					</div>
				</div>
			</div>
			<div class="wrap-bg">
				<div class="bg bg--molcajete"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--left"><p>Molcajete de Arrachera</p></div>
			</div>
		</div>
		<div class="item-menu">
			<div class="wrap-bg">
				<div class="bg bg--camarones"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--right"><p>Camarones con Chipotle de Papantla</p></div>
			</div>
			<div class="item-text">
				<h2>POLLO</h2>

				<p><span>Molcajete de fajitas de pollo (200 g)</span><br>
				Se sirve con queso asado, nopales, cebollitas cambray y frijoles de la olla,<br>
				acompañado de tortillas recién hechas</p>

				<p><span>Pechuga de pollo a la parrilla (200 g)</span><br>
				Servida con nopales preparados, cebollitas cambray y arroz a la mexicana</p>

				<p><span>Milanesa de pollo (120 g)</span><br>
 				Empanizada, acompañada de arroz blanco, frijoles de la olla<br>
 				y tortillas recién hechas</p> 

				<p><span>Pechuga rellena de plátano macho en mole poblano</span><br>
				Un deleite al paladar. Preparada con plátano macho horneado, bañada<br>
				con mole poblano (receta de la casa), servida con frijoles de la olla y arroz blanco</p>

				<h2>PESCADOS</h2>

				<p><span>Filete de pescado al limón (200 g)</span><br>
				A la plancha, bañado con una deliciosa salsa al limón con alcaparras.<br>
				Acompañado de arroz blanco</p>

				<p><span>Filete de huachinango estilo talla (200 g)</span><br>
				Acompañado de elote de la plaza y ensalada de nopales<br>
				con chicharrón fresco</p>

				<p><span>Posta de Huachinango (180 g)</span><br>
				Sobre puré de papa con aceite de flor de albahaca acompañado<br>
				de brochetas de verduras a la parrilla</p>

				<p><span>Mojarra Boca del Río (200 g  A 350 g)</span><br>
				Mojarra entera en salsa de cilantro al limón, acompañada<br>
				de arroz blanco</p>

				<p><span>Camarones al gusto (12 PIEZAS)</span><br>
				Elige cualquiera de nuestras preparaciones veracruzanas.<br>
				Al mojo de ajo, con el sabor único de Tlacotalpan. Enchipoclados,<br>
				con el sabor semidulce con chipotle de Papantla</p>
			</div>		
		</div>
		<div class="item-menu">
			<div class="item-text item-text--pl">
				<h2>CARNES</h2>
				<p><span>Arrachera a la parrilla (200 g) Calidad Choice</span><br>
				Acompañada de arroz a la mexicana y nopales preparados</p>

				<p><span>Tampiqueña de la fonda (200 g)</span><br>
				Asada, servida con una enmolada poblana y otra de pipián,<br>
				acompañada de arroz a la mexicana</p>
					
				<p><span>Milanesa de res (120 g)</span><br>
				Empanizada, acompañada de arroz blanco, frijoles de la olla<br>
				y tortillas recién hechas</p>

				<p><span>Albóndigas al chipotle estilo casero (3 PIEZAS/210 g)</span><br>
				Con nuestra tradicional salsa de chipotle, servidas con arroz blanco<br>
				y frijoles de la olla</p>


				<p><span>Chamorro al pibil (600 g), ideal para compartir</span><br>
				Abundante porción de chamorro picado para taquear, acompañado<br>
				de cebolla morada y chile habanero. Se sirve sobre hoja de plátano</p>


				<p><span>T-Bone (350 g)<br>
				Rib Eye (300 g)</span></p>

				<p><span>*Todos nuestros cortes son Calidad Choice</span></p>

				<p><span>Acompaña tu corte con cualquiera<br>
				de nuestras guarniciones:</span></p>

				<p>Espinacas a la crema<br>
				Papas a la francesa<br>
				Puré de papa con tocino y parmesano</p>
			</div>
			<div class="wrap-bg">
				<div class="bg bg--chamorro"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--left"><p>Chamorro pibil</p></div>
			</div>
		</div>
		<!-- end Comidas -->

		<!-- Desayunos -->
		<div class="head-menu">
			<h2>DESAYUNOS</h2>
			<p>HORARIO 7:30AM - 12:00PM</p>
		</div>
		<div class="item-menu">
			<div class="wrap-bg">
				<div class="bg bg--huevosculiacan"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--right"><p>Huevos Culiacán</p></div>
			</div>
			<div class="item-text item-text--rosa">
				<h2>HUEVOS</h2>

				<p><span>Revueltos con rajas poblanas</span><br>
				Servido en cazuela con salsa molcajeteada y granos de elote</p>

				<p><span>En salsa pasilla</span><br>
				Servidos en cazuela con salsa pasilla y cubos de queso panela</p>

				<p><span>Divorciados</span><br>
				Dos huevos fritos sobre chilaquiles bañados en 2 salsas: roja y verde, acompañados de<br>frijoles refritos</p>

				<p><span>Rancheros</span><br>
				Dos huevos fritos sobre una tortilla bañada en salsa ranchera, acompañados de<br> frijoles refritos</p>

				<p><span>Ahogados en salsa de guajillo</span><br>
				A la cazuela, en salsa de guajillo con nopales y cilantro</p>

				<p><span>Mineros</span><br>
				Revueltos con chicharrón y frijoles de la olla</p>

				<p><span>Tacos Revolcados</span><br>
				De huevo revuelto con nuestras deliciosas carnitas picadas con pico de gallo, servidos<br>co</p>

				<p><span>Al gusto</span><br>
				Fritos, tibios, a la mexicana, revueltos<br>
				A elegir con un ingrediente<br>
				Queso panela, Jamón, Jamón de pavo, Tocino, Salchicha </p>

				<p><span>Huevos istmeños</span><br>
				Dos huevos fritos sobre una tortilla bañados en mole negro, acompañado<br>
				de frijoles refritos</p>

				<p><span>Huevos Culiacán</span><br>
				Revueltos con chilorio a la mexicana, frijoles refritos y queso panela asado</p>


			</div>
		</div>
		<div class="item-menu">
			<div class="item-text item-text--pl">
				<h2>LIGEROS<br>Y SALUDABLES</h2>

				<p><span>Omelette de claras con rajas y queso panela</span><br>
				Sobre nuestra especial salsa molcajeteada, acompañado<br>
				de jitomate y rajas poblanas</p>

				<p><span>Quesadilla en comal</span><br>
				En tortilla de maíz, preparadas con queso Oaxaca con relleno<br>
				de flor de calabaza, chicharrón, champiñones o rajas</p>

				<h2>OMELETTES</h2>

				<p><span>Gratinado con champiñones y flor de calabaza</span><br>
				Sobre una cremosa salsa poblana, acompañada de frijoles<br>
				y tortillas calientitas</p>

				<p><span>De claras con nopales a la mexicana</span><br>
				Acompañadas de salsa molcajeteada y frijoles refritos</p>

				<p><span>Omelette de pollo con mole</span><br>
				Se sirve con frijoles refritos</p>

				<h2>CHILAQUILES</h2>

				<p><span>Chilaquiles verdes o rojos</span><br>
				Con totopos horneados acompañados de pollo<br>
				Con huevo<br>
				Con pollo gratinados</p>

			</div>
			<div class="bg-content">
				<div class="wrap-bg">
					<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
					<div class="bg bg--omelettemole"></div>
					<div class="caption caption--left"><p>Omelette de Pollo con Mole</p></div>
				</div>
				<div class="info-add">
					<h2>NATAS Y PAN DE LA CASA</h2>
					<p><span>Pan dulce recién horneado<br>
					Orden de nata<br>
					Cuerno o concha con nata</span></p>
				</div>
			</div>
		</div>
		<div class="item-menu">
			<div class="wrap-bg">
				<div class="bg bg--tacospipan"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--right"><p>Taquitos de Pollo Bañados en Pipián Verde</p></div>
			</div>
			<div class="item-text item-text--morado">
				<h2>ESPECIALIDADES</h2>
				
				<p><span>Taco pueblero (1 PIEZA)</span><br>
				Con arrachera picada, frijoles refritos, chicharrón revuelto en salsa molcajeteada,<br>
				acompañado de guacamole</p>

				<p><span>Enmoladas con pollo ( 3 PIEZAS)</span><br>
				Tres tortillas de maíz rellenas con pollo, bañadas con mole de la casa<br>
				y queso fresco espolvoreado</p>

				<p><span>Enfrijoladas</span><br>
				Rellenas de pollo o huevo a la mexicana, servidas con chorizo, chile serrano y crema.<br>
				Pídelas con queso fresco o gratinadas</p>

				<p><span>Molletes (4 PIEZAS)</span><br>
				Untados con frijoles refritos y queso gratinado, acompañados de salsa mexicana</p>

				<p><span>Molletes mixtos (4 PIEZAS)</span><br>
				Rajas, jamón, chorizo y queso, acompañados de salsa mexicana</p>

				<p><span>Pastel Azteca</span><br>
				Gratinado, preparado con tortillas de maíz recién hechas, relleno de pollo, rajas<br>
				poblanas y elote. Servido sobre una salsa de chile chipotle</p>

				<p><span>Cecina de res Yecapixtla</span><br>
				Acompañados de guacamole, frijoles refritos, crema y tortillas de comal</p>

				<p><span>Taquitos de pollo bañados en pipián verde (3 PIEZAS)</span><br>
				Tacos dorados de pollo bañados con pipián verde y queso acompañados de frijoles de<br>
				la olla y cebollita morada en escabeche</p>

				<p><span>Sopes el fuerte</span><br>
				De chilorio, acompañados con un tamal de elote frito y gratinado ocn con rajas, huevos<br>
				estrellados con salsa molcajeteada y frijoles refritos</p>

				<p><span>Enfrijoladas</span><br>
				Rellenas de huevo a la mexicana y queso fresco<br>
				Con huevo gratinadas<br>
				Con pollo gratinadas</p>

			</div>			
		</div>
		<div class="item-menu">
			<div class="item-text-column">
				<div class="head-item head-item--pl">
					<div>
						<img src="/assets/img/platillos/recomendacion_mayora_morado.png" alt="Fonda Mexicana Recomendaciones de la Mayora">
					</div>
				</div>

				<div class="item-columns item-columns--pl">
					<div class="column">
						<p><span>Nopal asado con queso panela</span><br>
						Acompañado de jamón de pavo asado<br>
						y rebanadas de jitomate.</p>

						<p><span>Enchiladas de Pollo</span><br>
						Sin freír, bañadas en salsa verde o rojo<br>
						Pídelas gratinadas o con queso fresco.</p>

						<p><span>Omelette de huitlacoche</span><br>
						con salsa poblana<br>
						Acompañado de frijoles refritos.</p>

						<p><span>Omelette Veracruz</span><br>
						Relleno de chorizo y queso fresco, sobre<br> 
						salsa de frijol  con chile serrano y queso<br>
						panela asado.</p>

						<p><span>La tradicional Machaca</span><br>
						con Huevo<br>
						Con tortilla de harina calientitas<br>
						acompañadas de frijoles y salsa<br>
						molcajeteada.</p>

					</div>
					<div class="column">
						<p><span>Tacos de huevo con tiritas</span><br>
						de cecina en salsa morita<br>
						Acompañados de frijoles refritos</p>

						<p><span>Chalupas</span><br>
						Dos huevos fritos acompañados<br>
						acompañados de nuestras famosas<br>
						chalupas, una salsa verde<br>
						y otra con salsa roja</p>

						<p><span>Enchiladas rojas estilo<br>San Luis</span><br>
 						Rellenas de papa con queso Oaxaca,<br>
 						bañadas en salsa de guajillo; servidas<br>
 						con lechuga, queso fresco, crema<br>
 						y rábano</p> 

						<p><span>Tacos de cochinita Pibil</span><br>
						En tortillas de maíz recién hechas,<br>
						servidas con cebolla morada y chiles<br>
						habernos, acompañados de frijoles<br>
						refritos y queso fresco</p>

						<p><span>Café lechero</span><br>
						El clásico café lechero de Veracruz</p>
					</div>
				</div>
			</div>
			<div class="wrap-bg">
				<div class="bg bg--huevoschorizo"></div>
				<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
				<div class="caption caption--left"><p>Huevos al Gusto</p></div>
			</div>
		</div>
		<div class="item-menu">
			<div class="bg-content">
				<div class="info-add info-add--pl">
					<h2>FRUTAS<br>Y CEREALES</h2>
					<p>Plato de fruta<br>
					Melón, sandía, piña y papaya<br>
					Con yogurt, granola o queso cottage<br>
					Con queso cottage y granola o yogurt y granola</p>
				</div>
				<div class="wrap-bg">
					<!-- <img src="/assets/img/platillos/mole_poblano.jpg" alt="Fonda Mexicana mole poblano"> -->
					<div class="bg bg--chocolate"></div>
					<div class="caption caption--left"><p>Chocolate Caliente</p></div>
				</div>
			</div>
			<div class="item-text">

				<h2>JUGOS<br>
				NATURALES</h2>

				<p>Naranja, toronja, zanahoria y verde.<br>
				Vaso (300 ml.) Jarrita (480 ml.)</p>

				<p>Jugos combinados<br>
				Fresa-Naranja, Piña-Fresa, Toronja-Piña,<br>
				Zahoria-Naranja, Verde-Toronja<br>
				Vaso (300 ml.) Jarrita (480 ml.)</p>

				<h2>BEBIDAS<br>
				CALIENTES</h2>

				<p>Chocolate caliente (300 ml.)<br>
				Batido con molinillo<br>
				Café de olla (310 ml.)<br>
				Café americano (180 ml.)<br>
				Café express (60 ml.)<br>
				Té (240 ml.) Pregunta por nuestra cajita de tés <br>
				Café capuchino (180 ml.)<br>
				Café capuchino Baileys (180 ml.)<br>
				Capuchino de sabores (180 ml.)<br>
				Chocolate, cajeta y rompope<br>
				Vaso de leche (300 ml.)<br>
				Pídelo frío o caliente</p>
			</div>
		</div>
		<!-- end Desayunos -->
		<?php } ?>

	</div>
</main>