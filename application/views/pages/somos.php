<main class="historia">
	<div class="bg-historia"></div>
	<div class="wrap-historia">
		<div class="head-historia">
			<div class="icon"><img src="/assets/img/historia/ico_chiapas.png" alt="Fonda Mexicana chiapas"></div>
			<div class="icon"><img src="/assets/img/historia/ico_bajio.png" alt="Fonda Mexicana bajio"></div>
			<div class="icon"><img src="/assets/img/historia/ico_yucatan.png" alt="Fonda Mexicana yucatan"></div>
			<div class="icon"><img src="/assets/img/historia/ico_veracruz.png" alt="Fonda Mexicana veracruz"></div>
		</div>
		<div class="info-historia">
			<div class="info-icons">
				<div class="icon"><img src="/assets/img/historia/ico_tlaxcala.png" alt="Fonda Mexicana tlaxcala"></div>
				<div class="icon"><img src="/assets/img/historia/ico_sinaloa.png" alt="Fonda Mexicana sinaloa"></div>
				<div class="icon"><img src="/assets/img/historia/ico_jalisco.png" alt="Fonda Mexicana jalisco"></div>
				<div class="icon"><img src="/assets/img/historia/ico_michoacan.png" alt="Fonda Mexicana michoacan"></div>
				<div class="icon"><img src="/assets/img/historia/ico_morelos.png" alt="Fonda Mexicana morelos"></div>
			</div>
			<div class="info-text">
				<h1>NUESTRA HISTORIA<br><span>ES TRADICIÓN</span></h1>
				<p>FONDA MEXICANA reúne la riqueza gastronómica de la comida mexicana, con innovadores platillos e ingredientes que enaltecen la variedad y sabor de nuestra cultura. Fonda Mexicana, restaurante de comida mexicana, es donde retomamos la esencia de cada región mediante un recorrido culinario por la República Mexicana, generando grandes experiencias, que al fusionarlas con novedosas creaciones, deleitan al paladar. En Fonda Mexicana preparamos platillos típicos de calidad con ingredientes de origen 100% mexicano que brindan en cada visita una explosión de aromas, texturas, colores y sabores.</p>
			</div>
		</div>
		<div class="foot-historia">
			<div class="icon"><img src="/assets/img/historia/ico_puebla.png" alt="Fonda Mexicana puebla"></div>
			<div class="icon"><img src="/assets/img/historia/ico_tabasco.png" alt="Fonda Mexicana tabasco"></div>
			<div class="icon"><img src="/assets/img/historia/ico_guerrero.png" alt="Fonda Mexicana guerrero"></div>
			<div class="icon"><img src="/assets/img/historia/ico_oaxaca.png" alt="Fonda Mexicana oaxaca"></div>
		</div>
	</div>
</main>
