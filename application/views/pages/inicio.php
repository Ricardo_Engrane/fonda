<main class="inicio-fonda">
	<div class="slider">
		<div class="slider-inicio">
		    <div class="swiper-wrapper">
		    	<div class="swiper-slide">
		    		<style type="text/css">
		    			.bg.bg-inicio-24ads {
							background: url('/assets/img/inicio/slider/slider-fonda.jpg') 50% 50%/cover no-repeat;
		    			}
		    		</style>
		    		<div class="bg bg-inicio-24ads"></div>
		    		<div class="info">
		    			<h2><span>UNA  AMPLIA SELECCIÓN CULINARIA</span><br>DE LA TÍPICA COCINA  MEXICANA</h2>
		    			<div class="zigzag">
		    				<img src="/assets/img/inicio/slider/zigzag.png" alt="">
		    			</div>
		    		</div>
		    	</div>
		    	<div class="swiper-slide">
		    		<style type="text/css">
		    			.bg.bg-inicio-24ads {
							background: url('/assets/img/inicio/slider/slider-fonda.jpg') 50% 50%/cover no-repeat;
		    			}
		    		</style>
		    		<div class="bg bg-inicio-24ads"></div>
		    		<div class="info">
		    			<h2><span>UNA  AMPLIA SELECCIÓN CULINARIA</span><br>DE LA  TÍPICA COCINA  MEXICANA</h2>
		    			<div class="zigzag">
		    				<img src="/assets/img/inicio/slider/zigzag.png" alt="">
		    			</div>
		    		</div>
		    	</div>
		    </div>
			<!-- If we need pagination -->
			<div class="swiper-pagination"></div>

		    <!-- If we need navigation buttons -->
		    <div class="swiper-button-prev swiper-button-white"></div>
		    <div class="swiper-button-next swiper-button-white"></div>
		</div>
	</div>

	<div class="destacados">
		
		<div class="bg-destacado">
			<div class="bg-left"></div>
			<div class="bg-right"></div>
		</div>

		<div class="head-destacado">
			<div>
		    	<img src="/assets/img/inicio/destacado/arrow_down.png" alt="">
			</div>
			<p>El mejor restaurante para comer Mole en la Ciudad de México, en Fonda Mexicana reunimos una gran variedad de Moles, los cuales representan lo mejor de la tradición culinaria de México.</p>
		</div>
		
		<div class="items-destacado">
			<a href="/nuestros-platillos" class="item-destacado item-destacado--left">
				<div class="wrap-bg-item">
					<div class="bg-item-destacado bg-item-panucho"></div>
				</div>
				<div class="img-destacado">
		    		<img src="/assets/img/inicio/destacado/logo_yuc.jpg" alt="">
				</div>
				<div class="tag-destacado">
		    		<img src="/assets/img/inicio/destacado/bg_tag_rosa.png" alt="">
					<p>PANUCHO<br>YUCATECO</p>
				</div>
			</a>
			<a href="/nuestros-platillos" class="item-destacado item-destacado--right">
				<div class="wrap-bg-item">
					<div class="bg-item-destacado bg-item-filete"></div>
				</div>
				<div class="img-destacado">
		    		<img src="/assets/img/inicio/destacado/logo_ver.jpg" alt="">	
				</div>
				<div class="tag-destacado">
		    		<img src="/assets/img/inicio/destacado/bg_tag_naranja.png" alt="">
					<p>FILETE<br>DE PESCADO</p>
				</div>

			</a>
		</div>
		
		<div class="foot-destacado">
			<div>
				<a href="/nuestros-platillos">VER PLATILLOS</a>
			</div>
		</div>
	</div>

	<div class="top-platillos">
		<div class="top-wrap">
			<div class="top-head">
				<p>VEN A DESCUBRIR LA GRAN VARIEDAD DE PLATILLOS TRADICIONALES QUE TENEMOS PARA TI</p>
			</div>
			<div class="top-items">
				<a href="/nuestros-platillos" class="top-item">
					<div class="bg-item bg-item--chile"></div>
					<div class="info-rect">
						<div class="item-border"></div>
						<div class="item-text">
							<p>CHILE QUERETANO</p>
						</div>
					</div>
				</a>
				<a href="/nuestros-platillos" class="top-item">
					<div class="bg-item bg-item--mole"></div>
					<div class="info-rect">
						<div class="item-border"></div>
						<div class="item-text">
							<p>MOLE VERDE</p>
						</div>
					</div>
				</a>
				<a href="/nuestros-platillos" class="top-item">
					<div class="bg-item bg-item--mojarra">
					</div>
					<div class="info-rect">
						<div class="item-border"></div>
						<div class="item-text">
							<p>MOJARRA BOCA DEL RIO</p>
						</div>
					</div>
				</a>
				<a href="/nuestros-platillos" class="top-item">
					<div class="bg-item bg-item--poblano">
					</div>
					<div class="info-rect">
						<div class="item-border"></div>
						<div class="item-text">
							<p>CHILE POBLANO</p>
						</div>
					</div>
				</a>
			</div>

			<div class="top-icons">
		    	<div><img src="/assets/img/inicio/top/ico_yucatan.png" alt=""></div>
		    	<div><img src="/assets/img/inicio/top/ico_guerrero.png" alt=""></div>
		    	<div><img src="/assets/img/inicio/top/ico_puebla.png" alt=""></div>
		    	<div><img src="/assets/img/inicio/top/ico_sinaloa.png" alt=""></div>
		    	<div><img src="/assets/img/inicio/top/ico_jalisco.png" alt=""></div>
		    	<div><img src="/assets/img/inicio/top/ico_morelos.png" alt=""></div>
		    	<div><img src="/assets/img/inicio/top/ico_bajio.png" alt=""></div>
		    	<div><img src="/assets/img/inicio/top/ico_chiapas.png" alt=""></div>
		    	<div><img src="/assets/img/inicio/top/ico_oaxaca.png" alt=""></div>
		    	<div><img src="/assets/img/inicio/top/ico_tabasco.png" alt=""></div>
		    	<div><img src="/assets/img/inicio/top/ico_tlaxcala.png" alt=""></div>
		    	<div><img src="/assets/img/inicio/top/ico_veracruz.png" alt=""></div>
		    	<div><img src="/assets/img/inicio/top/ico_michoacan.png" alt=""></div>
			</div>

			<div class="top-foot">
				<div class="btn-morado">
					<a href="/nuestros-platillos">VER PLATILLOS</a>
				</div>
			</div>
		</div>
	</div>
	
	<div class="inicio-organiza">
		<div class="wrap-organiza">
			<div class="info-organiza">
				<h2>ORGANIZA TU EVENTO<br>CON NOSOTROS</h2>
				<p>Contamos con todos los servicios para ese evento especial; desde espacios, instalaciones y una serie de  platillos que se adaptan a cualquier necesidad.</h3>
				<p>Pregunta por nuestros paquetes</p>
				<div class="btn-morado">
					<a href="/nuestros-platillos">VER PLATILLOS</a>
				</div>
				<div id="contacto" class="arrow-down-white">
			    	<img src="/assets/img/inicio/destacado/arrow_down.png" alt="">
				</div>				
			</div>
		</div>
	</div>

	<div class="contacto-inicio">
		<div class="wrap-contacto">
			<div class="direccion">
				<div class="direccion-info">
					<h2>CONTACTO</h2>
					<h3>UBICACIÓN:</h3>
					<p>Avenida Homero 1910,<br>
					Polanco, Polanco I Secc,<br>
					11560 Ciudad de México,<br>
					CDMX.</p>
					<h3>HORARIOS:</h3>
					<p>9:30 - 21:00</p>
					<h3>TELÉFONO:</h3>
					<p>01 (55) 5557 6144</p>
				</div>
				<div class="mapa-contacto">
					<div id="mapa"></div>
				</div>
			</div>

			<div class="form-contacto">
				<div class="form-head">
					<h2>Tu opinión es muy importante<br>para nosotros, ¡ escríbenos !</h2>
				</div>
				<form id="form-inicio-contacto" method="post">
					<div class="input">
						<input name="nombre" type="text" placeholder="Nombre">
					</div>
					<div class="input">
						<input name="telefono" type="text" placeholder="Teléfono">
					</div>
					<!-- <div class="select">
						<select  name="sucursal" id="sucursal" placeholder="SUCURSAL">
							<option value="insurgentes">Insurgentes sur</option>
							<option value="polanco">Polanco</option>
						</select>
					</div> -->
					<div class="input">
						<input name="correo" type="text" placeholder="Correo">
					</div>
					<div class="comment">
						<textarea name="comentario" placeholder="Comentario"></textarea>
						<p class="msg-form"><span>GRACIAS,</span><br>TUS COMENTARIOS <br>HAN SIDO RECIBIDOS</p>
					</div>
					<div class="submit">
						<button class="btn-naranja">ENVIAR</button>
					</div>
				</form>				
			</div>
		</div>
	</div>
</main>