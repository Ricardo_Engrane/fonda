<main class="contrata-eventos">
	<form id="form-contrata" action="" method="post">

		<div class="head-eventos">
			<div class="bg">
				<div class="info">
					<h1>ORGANIZA TU EVENTO<br>CON NOSOTROS</h1>
					<h2>Será perfecto e inolvidable.<br>
					Sólo completa cuatro sencillos pasos<br>
					para ofrecerte la mejor recomendación. </h2>
				</div>
			</div>
		</div>
		<div class="info-evento">
			<div class="form-items">
				<div class="form-wrap tipo">
					<div class="title">
						<p>1. ESCRIBE EL TIPO DE EVENTO:</p>
					</div>
					<div class="input">
						<input class="validate-api-salones" type="text" name="tipo_evento" placeholder="Ejemplo: BODA">
					</div>
				</div>
				<div class="form-wrap invitados">
					<div class="title">
						<p>2. ¿CUAL ES TU NÚMERO DE INVITADOS?</p>
					</div>
					<div class="input">
						<input class="validate-api-salones" type="text" name="personas" placeholder="Ejemplo: 20">
					</div>
				</div>
				<div class="form-wrap horario">
					<div class="title">
						<p>3. COMPARTENOS TU DÍA Y HORARIO IDEAL</p>
					</div>
					<div class="input">
						<input class="validate-api-salones" type="date" name="fecha" placeholder="dd/mm/aaaa">
					</div>
				</div>
				<div class="input-group">
					<div>
						<select class="validate-api-salones" name="hora_ini">
							<option value="">HORA INICIO</option>
							<option value="7:00">7:00 am</option>
							<option value="7:30">7:30 am</option>
							<option value="8:00">8:00 am</option>
							<option value="8:30">8:30 am</option>
							<option value="9:00">9:00 am</option>
							<option value="9:30">9:30 am</option>
							<option value="10:00">10:00 am</option>
							<option value="10:30">10:30 am</option>
							<option value="11:00">11:00 am</option>
							<option value="11:30">11:30 am</option>
							<option value="12:00">12:00 pm</option>
							<option value="12:30">12:30 pm</option>
							<option value="13:00">1:00 pm</option>
							<option value="13:30">1:30 pm</option>
							<option value="14:00">2:00 pm</option>
							<option value="14:30">2:30 pm</option>
							<option value="15:00">3:00 pm</option>
							<option value="15:30">3:30 pm</option>
							<option value="16:00">4:00 pm</option>
							<option value="16:30">4:30 pm</option>
							<option value="17:00">5:00 pm</option>
							<option value="17:30">5:30 pm</option>
							<option value="18:00">6:00 pm</option>
							<option value="18:30">6:30 pm</option>
							<option value="19:00">7:00 pm</option>
							<option value="19:30">7:30 pm</option>
							<option value="20:00">8:00 pm</option>
							<option value="20:30">8:30 pm</option>
							<option value="21:00">9:00 pm</option>
							<option value="21:30">9:30 pm</option>
							<option value="22:00">10:00 pm</option>
							<option value="22:30">10:30 pm</option>
							<option value="23:00">11:00 pm</option>
						</select>
					</div>
					<div>
						<select class="validate-api-salones" name="hora_fin">
							<option value="">HORA FINAL</option>
							<option value="7:00">7:00 am</option>
							<option value="7:30">7:30 am</option>
							<option value="8:00">8:00 am</option>
							<option value="8:30">8:30 am</option>
							<option value="9:00">9:00 am</option>
							<option value="9:30">9:30 am</option>
							<option value="10:00">10:00 am</option>
							<option value="10:30">10:30 am</option>
							<option value="11:00">11:00 am</option>
							<option value="11:30">11:30 am</option>
							<option value="12:00">12:00 pm</option>
							<option value="12:30">12:30 pm</option>
							<option value="13:00">1:00 pm</option>
							<option value="13:30">1:30 pm</option>
							<option value="14:00">2:00 pm</option>
							<option value="14:30">2:30 pm</option>
							<option value="15:00">3:00 pm</option>
							<option value="15:30">3:30 pm</option>
							<option value="16:00">4:00 pm</option>
							<option value="16:30">4:30 pm</option>
							<option value="17:00">5:00 pm</option>
							<option value="17:30">5:30 pm</option>
							<option value="18:00">6:00 pm</option>
							<option value="18:30">6:30 pm</option>
							<option value="19:00">7:00 pm</option>
							<option value="19:30">7:30 pm</option>
							<option value="20:00">8:00 pm</option>
							<option value="20:30">8:30 pm</option>
							<option value="21:00">9:00 pm</option>
							<option value="21:30">9:30 pm</option>
							<option value="22:00">10:00 pm</option>
							<option value="22:30">10:30 pm</option>
							<option value="23:00">11:00 pm</option>
						</select>
					</div>
				</div>
			</div>
			<div class="foot-form">
				<p>TE PRESENTAMOS LAS RECOMENDACIONES QUE TENEMOS PARA TI.</p>
				<p>4. ELIGE EL SALÓN DE TU INTERÉS</p>
			</div>
		</div>

		<div class="salones-eventos">
			<div class="salones">
				<div class="wrap-salon" data-salon="capilla">
					<div class="salon">
						<div class="bg bg--capilla"></div>
						<div class="info">
							<h2>CAPILLA<br>· 20 PERSONAS</h2>
							<p>Ubicado en la planta alta,<br>totalmente privado.</p>
							<p class="disponibilidad">NO DISPONIBLE</p>
						</div>
					</div>
				</div>

				<div class="wrap-salon" data-salon="vitrales">
					<div class="salon">
						<div class="bg bg--vitrales"></div>
						<div class="info">
							<h2>VITRALES<br>· 40 PERSONAS</h2>
							<p>Ubicado en la planta alta,<br>totalmente privado.</p>
							<p class="disponibilidad">NO DISPONIBLE</p>
						</div>
					</div>
				</div>

				<div class="wrap-salon" data-salon="cupula">
					<div class="salon">
						<div class="bg bg--cupula"></div>
						<div class="info">
							<h2>CÚPULA<br>· 55 PERSONAS</h2>
							<p>Ubicado en la planta alta,<br>totalmente privado.</p>
							<p class="disponibilidad">NO DISPONIBLE</p>
						</div>
					</div>
				</div>
			</div>

			<div class="salones">
				<div class="wrap-salon" data-salon="cholula 1">
					<div class="salon">
						<div class="bg bg--cholula1"></div>
						<div class="info">
							<h2>CHOLULA 1<br>· 55 PERSONAS</h2>
							<p>Ubicado en la planta alta,<br>totalmente privado.</p>
							<p class="disponibilidad">NO DISPONIBLE</p>
						</div>
					</div>
				</div>

				<div class="wrap-salon" data-salon="cholula 2">
					<div class="salon">
						<div class="bg bg--cholula2"></div>
						<div class="info">
							<h2>CHOLULA 2<br>· 55 PERSONAS</h2>
							<p>Ubicado en la planta alta,<br>totalmente privado.</p>
							<p class="disponibilidad">NO DISPONIBLE</p>
						</div>
					</div>
				</div>

				<div class="wrap-salon" data-salon="principal">
					<div class="salon">
						<div class="bg bg--principal"></div>
						<div class="info">
							<h2>PRINCIPAL<br>· 70 PERSONAS</h2>
							<p>Ubicado en la planta alta,<br>totalmente privado.</p>
							<p class="disponibilidad">NO DISPONIBLE</p>
						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="form-reservacion">

			<div class="form-head-reservacion">
				<div class="head-center">
					<p>¡LISTO! COMPÁRTENOS TUS DATOS Y DAREMOS SEGUIMIENTO A TU SOLICITUD</p>
				</div>
			</div>

			<div class="form-info">

				<div class="wrap-info-form">
					<div class="revisa-datos">
						<div class="title">
							<p>REVISA QUE TUS DATOS SEAN CORRECTOS</p>
						</div>
						<div class="wrap-revisar">
							<!-- <p class="revisa-lugar"><span>LUGAR:</span> INSURGENTES SUR</p> -->
							<p class="revisa-evento"></p>
							<p class="revisa-invitados"></p>
							<p class="revisa-fecha"></p>
							<p class="revisa-horario"></p>
							<p class="revisa-salon"></p>
						</div>
					</div>
				</div>

				<div class="form">
					<!-- <input id="input-hidden-lugar" type="hidden" value="almendros insurgentes" name="lugar"> -->
					<input class="validate-api-salones" id="input-hidden-salon" type="hidden" value="" name="salon">
					<div>
						<input name="nombre" type="text" placeholder="NOMBRE Y APELLIDO*">
					</div>
					<div>
						<input name="email" type="text" placeholder="CORREO*">
					</div>
					<div>
						<input name="telefono" type="text" placeholder="TELÉFONO*">
					</div>
					<div class="checkbox">
						<div>
							<input name="aceptar" type="checkbox">
							<label for=""></label>
						</div>
						<p>ACEPTAR<br>POLITICAS DE CONTRATACIÓN</p>
					</div>
					<div>
						<div class="g-recaptcha" data-sitekey="6Lfo1V8UAAAAAKGgqHZmoKL8idioQ8wWsUKmxt7O"></div>
						<button>ENVIAR</button>
					</div>
				</div>
			</div>
		</div>	
	</form>
</main>