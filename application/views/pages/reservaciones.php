<main class="reservaciones">
	<div class="bg-reservaciones">
		<div class="icon icon1">
			<img src="/assets/img/reserv/icon1.png" alt="">
		</div>
		<div class="icon icon2">
			<img src="/assets/img/reserv/icon2.png" alt="">
		</div>
		<div class="icon icon3">
			<img src="/assets/img/reserv/icon3.png" alt="">
		</div>
		<div class="plato">
			<img src="/assets/img/reserv/plato.png" alt="">
		</div>
	</div>
	<div class="wrap">
		<div class="item">
			<div class="bg bg-mesa"></div>
			<div class="info info-ls">
				<h2>MESA</h2>
				<p>POLANCO<br>(55) 3933 0085</p>
				<p>INSURGENTES<br>(55) 3933 0410</p>
				<a href="https://api.whatsapp.com/api/send?phone=521551234567" target="_blank">
					<p class="icon">WHATSAPP<br>(55) 5594 2090</p>
				</a>
			</div>
		</div>
		<div class="item">
			<div class="bg bg-event"></div>
			<div class="info">
				<h2>EVENTOS A<br>DOMICILIO</h2>
				<p>A PARTIR DE 15 PERSONAS</p>
				<p>ELABORAMOS PRESUPUESTO<br>PERSONALIZADOS DE ACUERDO<br> A SUS NECESIDADES</p>
			</div>
		</div>
		<div class="item">
			<div class="bg bg-lugar"></div>
			<div class="info">
				<h2>EVENTOS<br>EN LUGAR</h2>
				<a href="/eventos">VER MÁS</a>
			</div>
		</div>
	</div>
</main>