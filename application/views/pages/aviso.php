<main class="aviso-privacidad">
	<div class="container">
		<div class="header-aviso">
			<div class="title">
				<h1><b>AVISO DE PRIVACIDAD</b></h1>
				<h2><b>AVISO DE PRIVACIDAD DEL SITIO WEB CMR.MX</b></h2>
				<p>En vigor desde el 28 de septiembre de 2011.</p>
			</div>
			<div class="grec">
				<img src="/assets/img/aviso/grec.png" alt="">
			</div>
		</div>

		<div class="content">
			<h2><b>Información del Responsable</b></h2>
			<br>
			<p>Para CMR lo más importante son sus clientes, por lo que se preocupa y ocupa de cuidar los datos personales que le son proporcionados a través del sitio Web CMR.MX, de cualquiera de los sitios Web de sus sociedades afiliadas o subsidiarias (en adelante y conjuntamente, los “sitios Web” o el “sitio Web”), y de las diferentes vías por las cuales CMR recaba datos personales. CMR tiene el compromiso de proteger su privacidad y cumplir con todas las leyes pertinentes sobre la protección y la privacidad de sus datos. El presente Aviso de Privacidad (en adelante, el “Aviso”) explica cómo procesamos sus datos personales. A lo largo de este Aviso, las expresiones “datos personales”, “datos” o “información personal” se refieren a cualquier información concerniente a una persona física identificada o identificable. El término “CMR” se refiere a CMR, S.A.B. de C.V., incluidas sus sociedades afiliadas y subsidiarias (también se hace referencia a CMR al usar los términos “nosotros”, “nuestro” u otros similares). Si Usted ha sido enlazado al presente Aviso mediante otro sitio Web, significa que el sitio por el cual ingresó pertenece a una sociedad afiliada, subsidiaria o empresa del mismo grupo de interés de CMR y, por ende, aplica el presente Aviso al tratamiento de sus datos personales.</p>
			<br>
			<p>Mediante otros avisos, extractos o vínculos relacionados, podemos proporcionar información adicional sobre su privacidad en relación con nuestros servicios relativos a datos personales específicos. Los términos de dichos otros avisos, extractos o vínculos relacionados prevalecerán sobre el presente Aviso en caso de presentarse algún conflicto.</p>
			<br>
			<p>En razón de lo anterior y en cumplimiento a lo establecido por los artículos 8, 15, 16, 33, 36 y demás relativos de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares (también referida en el presente Aviso como la “Ley”) y su Reglamento, hacemos de su conocimiento que CMR es el responsable del uso y protección de los datos personales que nos proporcione, por lo que hemos implementado las medidas de seguridad necesarias para mantener en todo momento su información personal confidencial y segura de cualquier intento de vulneración.</p>
			<br>
			<p><b>Asimismo, le informamos que nuestro domicilio se encuentra ubicado en:</b></p>
			<p>Havre No. 30., Colonia Juárez<br>
			Delegación Cuauhtémoc<br>
			C.P. 06600, México, Distrito Federal</p>
			<br>
			<p>Al utilizar este o cualquiera de los sitios Web de cualquier afiliada o subsidiaria de CMR o al enviar cualquier tipo de datos personales a CMR, Usted acepta el procesamiento de sus datos personales de la forma que se explica en el presente Aviso. Si Usted no acepta este Aviso, por favor no proporcione sus datos personales ni utilice este sitio Web o cualquier otro relacionado con CMR, ya que el mismo le es aplicable a todos ellos, ni proporcione sus datos personales a CMR o a cualquiera de sus sociedades subsidiarias o afiliadas.</p>
			<br>
			<p>Sitios incluidos en este Aviso de Privacidad CMR es una empresa mexicana, formada por entidades legales, procedimientos comerciales, estructuras de administración y sistemas técnicos aplicados en los Estados Unidos Mexicanos. El presente Aviso se aplica a todos los sitios Web propiedad de CMR, así como a aquellos que son propiedad de las sociedades afiliadas y subsidiarias que CMR posee en su totalidad, salvo que un sitio Web de CMR haya una declaración de privacidad especifica referida a un programa o servicio particular, en cuyo caso aplicará dicha declaración de privacidad especifica en vez del presente Aviso.</p>
			<br>
			<p>Intención del Aviso de Privacidad Es importante informarle que la Ley Federal de Protección de Datos Personales en Posesión de los Particulares y su Reglamento protegen su información personal de usos no autorizados y sin su consentimiento, por lo que el presente documento hará de su conocimiento la información que recabamos de Usted, para qué y cómo la usamos, las posibles transferencias a terceros, las finalidades del tratamiento de sus datos, sus Derechos ARCO (según dicho término se encuentra definido más adelante), así como la revocación de su consentimiento al uso de sus datos, los cuales Usted puede hacer valer ante CMR con el objetivo de que Usted tenga pleno control y decisión sobre sus datos personales. Por este motivo, le recomendamos que lea atentamente la siguiente información:</p>
			<br>
			<p>El presente Aviso se pone a su disposición con la intención de que Usted, en su carácter de Titular de los datos personales proporcionados, tenga conocimiento del tratamiento que se dará a los mismos, así como de la información precisa para ejercer sus derechos de acceso, rectificación, cancelación y oposición (en adelante, “Derechos ARCO”), así como la revocación de su consentimiento para el tratamiento de sus datos. A continuación se describen sus Derechos</p>
			<br>
			<p><b>ARCO</b></p>
			<br>
			<p>Acceso: Derecho a acceder a sus datos personales en posesión de CMR, a excepción de los casos mencionados en la Ley.</p>
			<p>Rectificación: Derecho a rectificar sus datos personales cuando estos sean inexactos o incompletos.</p>
			<p>Cancelación: Derecho en todo momento a cancelar sus datos personales, a excepción de los casos mencionados en la Ley.</p>
			<p>Oposición: Derecho en todo momento y por causa legítima a oponerse al tratamiento de sus datos personales.</p>
			<p>Revocación: Derecho en todo momento a revocar su consentimiento para el tratamiento específico de sus datos personales.</p>
			<br>
			<p>El presente Aviso es aplicable para los Titulares de datos personales obtenidos directa, indirecta o personalmente por CMR, a través de procesos de reservación, contratación de servicios, solicitudes de información, procesos de reclutamiento, así como de los distintos formularios contenidos en sus sitios Web o cualquier otro medio especificado para tales efectos, que hagan referencia al presente Aviso.</p>
			<br>
			<p>Formas de obtención de su información CMR obtiene, almacena y trata su información personal en virtud de la actual o futura relación jurídica que existe con Usted, así como las diferentes actividades relacionadas con los servicios que prestan CMR y sus sociedades afiliadas y subsidiarias. CMR puede obtener sus datos personales cuando Usted los entregue en forma física a alguno de nuestros empleados o autorizados, directamente cuando nos los haga llegar Usted o su representante legal a través de los medios tecnológicos o de correspondencia designados para tal efecto e indirectamente a través de bases de datos públicas o de terceros.</p>
			<br>
			<p>La información que recabamos</p>
			<br>
			<p>Entre la información personal que CMR obtiene directa, indirecta o personalmente por parte de sus usuarios, clientes, asociados, encargados, proveedores, candidatos y/o cualquier persona relacionada con los servicios que prestamos, se encuentran los mencionados en la siguiente clasificación:</p>
			<br>
			<p>1. Identificación: Teléfono fijo, celular, correo electrónico, número de identificación, domicilio, fecha de nacimiento, sexo, Registro Federal de Contribuyentes, CURP y contraseña de servicio.</p>
			<p>2. Laborales: puesto, área o departamento, ocupación, domicilio de trabajo, correo electrónico de trabajo, teléfono de trabajo, celular de trabajo, referencias, desempeño y actividades pasadas y presentes.</p>
			<p>3. Académicos: Datos de licenciatura, maestría, doctorado, diplomados, institución, área de estudios, inicio y término de estudios, estatus de estudios, conocimientos de cómputo e idiomas.</p>
			<p>4. Patrimoniales y financieros: número de tarjeta bancaria e identificadores de cuentas bancarias.</p>
			<p>5. Sensibles: salud pasada o presente.</p>
			<br>
			<p><b>Datos Sensibles</b></p>
			<br>
			<p>CMR no solicita Datos Personales Sensibles (según los mismos se encuentran definidos en la Ley) en los sitios Web. En caso de que se recaben datos personales de carácter sensible por alguna vía autorizada por CMR, se solicitará el consentimiento previo y por escrito de su titular.</p>
			<br>
			<p><b>Protección</b></p>
			<br>
			<p>CMR ha implementado medidas tecnológicas de protección de los datos personales que trata. Sin embargo, y a pesar de contar cada día con herramientas más seguras, la protección de los datos enviados a través de Internet no se puede garantizar al 100%; por lo que una vez recibidos, se hará todo lo posible por salvaguardar la información. Para garantizar que los datos personales sean tratados conforme a lo dispuesto en la Ley y su Reglamento, CMR cumple con los siguientes principios:</p>
			<br>
			<p>1.- Se le comunicarán las finalidades del tratamiento de sus datos personales recabando solo los datos necesarios para el cumplimiento de dichas finalidades y no se le dará a sus datos un uso distinto al establecido en el presente Aviso.</p>
			<p>2.- Se pondrán a su disposición herramientas suficientes para verificar que sus datos personales sean correctos y se mantengan actualizados. Si por razones para el cumplimiento de una relación contractual debemos conservar sus datos, el almacenamiento de los mismos se llevará a cabo conforme a lo dispuesto por el presente Aviso, la Ley aplicable y su Reglamento.</p>
			<p>3.- Se han implementado medidas de seguridad para garantizar la protección de sus datos personales.</p>
			<br>
			<p>El uso que le damos a su información personal La información que CMR o sus sociedades afiliadas o subsidiarias recopilan para comprender mejor sus necesidades e intereses, nos ayuda para proporcionarle una experiencia personalizada y coherente. Por ejemplo, CMR puede utilizar su información para:</p>
			<br>
			<p>• Ayudarle a completar una transacción o pedido;</p>
			<p>• Prevenir y detectar amenazas a la seguridad, comportamientos fraudulentos u otras actividades maliciosas;</p>
			<p>• Comunicarse con Usted acerca de productos y servicios;</p>
			<p>• Ponerle al día sobre nuevos servicios y prestaciones;</p>
			<p>• Proporcionar ofertas de promoción de forma personalizada;</p>
			<p>• Seleccionar el contenido que se le va a comunicar;</p>
			<p>• Evaluar el rendimiento de las iniciativas de marketing, anuncios y sitios Web gestionados por CMR u otras empresas en nombre de CMR;</p>
			<p>• Permitirle participar en concursos y encuestas; y</p>
			<p>• Ponerse en contacto con Usted con relación a los servicios de CMR.</p>
			<br>
			<p><b>Asimismo, CMR trata su información personal para los siguientes fines:</b></p>
			<br>
			<p>1. Para el inicio, cumplimiento, mantenimiento, negociación y terminación de la relación jurídica entre CMR y Usted;</p>
			<p>2. Para el funcionamiento, gestión, facturación electrónica, cobranza, administración, prestación, ampliación y mejora de nuestros servicios;</p>
			<p>3. Para otorgar el acceso a los sitios Web;</p>
			<p>4. Para la entrega de notificaciones, cartas o boletines informativos o atención a sus solicitudes relacionadas con los servicios que prestamos;</p>
			<p>5. Para ayudarle a completar una reservación o registro a promociones que se hayan realizado o se realicen en los sitios Web;</p>
			<p>6. Para la transferencia de su información personal en los casos aplicables de conformidad con el apartado CON QUIÉN PODEMOS COMPARTIR SU INFORMACIÓN del presente Aviso;</p>
			<p>7. Para la prevención o denuncia ante diferentes autoridades de actos o hechos ilícitos;</p>
			<p>8. Para el envío de publicidad y ofertas de las promociones existentes y nuevas que ofrecemos a través de e-mail marketing, publicidad física y telefónica de forma personalizada o de prospección comercial, por medio de CMR, sus subsidiarias, afiliadas o terceros. Al final de este Aviso Usted podrá manifestar su consentimiento para estos efectos;</p>
			<p>9. Para el mantenimiento de la relación laboral, pago de nómina, campañas de salud al personal y cumplimiento a las disposiciones de seguridad social aplicables, actividades de mejora al ambiente laboral, así como la contratación a nombre de los empleados de CMR de todo tipo de seguros y/o servicios a solicitud de los mismos;</p>
			<p>10. Para dar cumplimiento al ejercicio de sus Derechos ARCO, así como revocar su consentimiento al tratamiento de sus datos;</p>
			<p>11. Para el cumplimiento de las relaciones contractuales con CMR o sus sociedades afiliadas o subsidiarias, en las cuales sus datos única y exclusivamente podrán ser tratados para el cumplimiento de los fines objeto de dicha relación, ya sea como cliente o proveedor, en la forma y términos establecidos en el presente apartado de uso de su información personal.</p>
			<br>
			<p>Con quién podemos compartir su información CMR, para la correcta prestación de sus servicios y en cumplimiento con la Ley Federal de Protección de Datos Personales en Posesión de los Particulares y su Reglamento, podrá remitir y/o transferir a sus subsidiarias, afiliadas, empresas del mismo grupo, encargados, terceros nacionales o extranjeros en forma parcial o total, su información personal, para el cumplimiento de alguna o algunas de las finalidades señaladas en el presente documento, en los siguientes casos:</p>
			<br>
			<p>1. A personas afiliadas y/o relacionadas con CMR para fines de comprobación, revisión y certificación en materia fiscal, administrativa, o laboral;</p>
			<p>2. Al Servicio de Administración Tributaria y empresas prestadoras de servicios de facturación electrónica, en virtud de los procesos de emisión y envío de Comprobantes Fiscales Digitales por Internet;</p>
			<p>3. A compañías afiliadas o no afiliadas que asisten, apoyan o coadyuvan a CMR para el efectivo cumplimiento de la prestación de sus servicios, envío por correo electrónico de información relativa a nuestros servicios y ofertas, negociaciones o para la prestación de sus servicios con terceros;</p>
			<p>4. A encargados para proteger y defender los intereses y derechos de Usted, CMR y/u otros terceros;</p>
			<p>5. A encargados, para la contratación a nombre de los empleados de CMR de todo tipo de seguros y/o servicios a solicitud de los mismos;</p>
			<p>6. A encargados, para proteger la seguridad y/o intereses de Usted, del personal y afiliados de CMR, de sus clientes y proveedores o del público en general;</p>
			<p>7. A distintas autoridades para el desahogo de requerimientos o por constar estos en algún tipo de legislación o normatividad;</p>
			<p>8. Asimismo, se podrá llevar a cabo la remisión de sus datos personales a Encargados de CMR para el cumplimiento de aquellas finalidades que dieron origen al tratamiento de sus datos personales, para el cumplimiento de una relación jurídica o laboral según sea el caso, y cuando sea necesario, para la realización de actividades derivadas de la relación jurídica con Usted. Todo esto en el entendido que se tratarán sus datos personales únicamente conforme a las instrucciones de CMR y del presente Aviso, previo compromiso de los receptores de los datos de guardar confidencialidad de los datos personales que sean remitidos y resguardarlos bajo las mismas medidas de seguridad establecidas por CMR.</p>
			<p>9. Los datos personales serán suprimidos una vez que la relación jurídica se haya cumplido, por instrucciones de CMR o derivado del ejercicio de sus Derechos ARCO, por lo que el Encargado deberá suprimir los datos personales, excepto cuando sea exigible su conservación conforme a las leyes aplicables.</p>
			<p>10. Los Encargados no podrán transferir a su vez los datos personales que les hayan sido remitidos salvo que sea necesario para dar cumplimiento a su relación contractual con CMR o a las finalidades previstas en el presente Aviso.</p>
			<p>11. Cuando se transfieran datos personales a terceros y se realicen cambios en las finalidades autorizadas, Usted podrá revisar dichos cambios de la manera establecida en el apartado ACTUALIZACIONES Y/O MODIFICACIONES del presente Aviso. Utilización de herramientas para la recopilación automática de datos Las cookies son archivos de texto que son descargados automáticamente y almacenados en el disco duro del equipo de cómputo de un usuario al navegar en una página de Internet específica, que permiten recordar al servidor de Internet algunos datos sobre este usuario, entre ellos, sus preferencias de compra para la visualización de las páginas en ese servidor, nombre y contraseña. Por su parte, las Web beacons son imágenes insertadas en una página de Internet o correo electrónico, que puede ser utilizado para monitorear el comportamiento de un visitante, como almacenar información sobre la dirección IP del usuario, duración del tiempo de interacción en dicha página y el tipo de navegador utilizado, entre otros. Le informamos que utilizamos cookies y Web beacons para obtener información personal de Usted, como la siguiente:</p>
			<br>
			<p>El tipo de navegador y sistema operativo que utiliza; las páginas de Internet que visita en forma previa y posterior a la entrada de los sitios Web de CMR y de sus sociedades afiliadas y subsidiarias; los vínculos que sigue y permanencia en nuestro sitio; su dirección IP; lugar desde el cual nos visita y estadísticas de navegación. Dichas cookies y otras tecnologías pueden ser deshabilitadas. Puede buscar información sobre los navegadores conocidos y averiguar cómo ajustar las preferencias de las cookies en los siguientes sitios Web:</p>
			<br>
			<p>Microsoft Internet Explorer: http://www.microsoft.com/info/cookies.htm</p>
			<p>Mozilla Firefox: http://www.mozilla.org/projects/security/pki/psm/help_21/using_priv_help.html</p>
			<br>
			<p>En el caso de empleo de cookies, el botón de “ayuda” que se encuentra en la barra de herramientas de la mayoría de los navegadores, le dirá cómo evitar aceptar nuevos cookies, cómo hacer que el navegador le notifique cuando recibe un nuevo cookie o cómo deshabilitar todos los cookies.</p>
			<br>
			<p><b>Privacidad de los menores</b></p>
			<br>
			<p>CMR no recopila de forma intencional la información de menores de edad, por lo que se recomienda a los padres y tutores lleven a cabo las actividades de registro en los sitios Web ellos mismos.</p>
			<p>Limitación de uso y divulgación de información En nuestro programa de notificación de promociones, ofertas y servicios a través de correo electrónico, CMR, sus subsidiarias y encargados tienen acceso a la información recabada. Este tipo de publicidad se realiza mediante avisos y mensajes promocionales de correo electrónico, los cuales sólo serán enviados a Usted y a aquellos contactos registrados para tal propósito. Usted puede modificar esta autorización en cualquier momento.</p>
			<br>
			<p><b>Exclusión de responsabilidad</b></p>
			<br>
			<p>Nuestro sitio Web podría contener enlaces, hipervínculos o hipertextos “links”, banners, botones y/o herramientas de búsquedas en Internet que al ser utilizados por los usuarios transportan a otros portales o sitios de Internet que podrían ser propiedad de terceros. CMR no controla dichos sitios ni se hace responsable por los avisos de privacidad que ostenten, los datos personales que los usuarios llegasen a proporcionar a través de estos portales o sitios de Internet distintos a los sitios Web no son responsabilidad de CMR ni de sus sociedades afiliadas y subsidiarias, por lo que deberá verificar el aviso de privacidad en cada sitio al que acceda.</p>
			<br>
			<p>Asimismo, podemos facilitar en nuestro sitio Web funciones de medios sociales que le permiten compartir la información del sitio Web en dichas redes e interactuar con CMR en diversos sitios de esta naturaleza. La utilización de estas funciones puede implicar que se recabe o comparta información sobre Usted, lo cual dependerá de cada función concreta. Le recomendamos que revise la configuración y las políticas de privacidad de los sitios de los medios sociales con los que interactúa para asegurarse de que comprende la información que se podría compartir en dichos sitios.</p>
			<br>
			<p><b>Ejercicio de sus Derechos ARCO</b></p>
			<br>
			<p>CMR vela en todo momento por el cumplimiento de los principios de protección de los datos personales que trata. Por este motivo, Usted, en su carácter de Titular de los datos personales o su representante legal debidamente acreditado, podrá limitar el uso o divulgación de sus datos personales, así como revocar su consentimiento para su tratamiento. Para cualquier duda o aclaración con respecto al uso de sus datos personales, hemos implementado un sistema de quejas y sugerencias relativas al uso que damos a los datos personales que recabamos, mediante el número telefónico (55) 5263 6951.</p>
			<p>De la misma forma, Usted podrá ejercer los derechos de acceso, rectificación, cancelación u oposición que la Ley prevé mediante una solicitud enviada al correo electrónico infoprivacidad@cmr.mx o directamente en nuestras oficinas, donde se le brindará atención a su solicitud y/o resolverá cualquier duda con respecto al contenido de este Aviso. Para facilitar su solicitud, a continuación ponemos a su disposición un modelo de texto para ejercer sus Derechos ARCO:</p>

			<p>(Lugar y fecha)</p>
			<p>CMR, S.A.B. DE C.V. (CMR)</p>
			<p>Havre No. 30</p>
			<p>Col. Juárez, Del. Cuauhtémoc</p>
			<p>06600 México, Distrito Federal</p>
			<p>Atención: Departamento de Datos Personales (Jurídico).</p>
			<p>Referencia: Solicitud de ejercicio de Derechos en materia de Datos Personales.</p>
			<br>
			<div>
				<p>Estimados Señores,</p>
				<br>
				<p>Por la presente solicito: _______ (breve descripción clara y precisa de los datos personales con respecto a los que pretenda ejercer un derecho, así como el derecho que desea ejercer), para lo cual me identifico con ______ (señalar el documento oficial con el cual se identifica). Todo ello de conformidad con el aviso de privacidad de su empresa.</p>
				<p>Atentamente,</p>
				<p>Nombre y firma del titular de los datos o su representante legal (por su seguridad, se podrá solicitar el documento de identificación correspondiente y el documento comprobatorio de las facultades del representante).”</p>
				<p>Actualizaciones y/o modificaciones</p>
				<p>Cualquier modificación o actualización al presente Aviso será publicada en el sitio Web CMR.MX para que, en caso de considerarlo necesario, manifieste su oposición al nuevo tratamiento que se daría a sus datos personales.</p>
			</div>
			<br>
			<p><b>Aceptación de los Términos</b></p>
			<br>
			<p>Este Aviso está sujeto a los términos y condiciones de todos los sitios Web de CMR y de sus sociedades afiliadas y/o subsidiarias, lo cual constituye un acuerdo legal entre el Usted y CMR.</p>
			<p>Si Usted utiliza los servicios en cualquiera de los sitios de CMR, sus afiliadas y/o subsidiarias, significa que ha leído, entendido y acordado los términos contenidos en el presente Aviso o en el aviso respectivo. Si no está de acuerdo con los mismos, Usted no deberá proporcionar información personal alguna, ni utilizar los servicios de los sitios Web de CMR o de sus sociedades afiliadas o subsidiarias.</p>
			<p>Si Usted acepta haber leído el presente Aviso y no manifiesta su oposición para que sus datos personales sean tratados o transferidos, se entenderá que ha otorgado su consentimiento ello.</p>
			<br>
			<p>Fecha de última actualización: 22 de enero de 2016.</p>
			<br>
			<p>CMR, S.A.B. de C.V. (“CMR”), con domicilio en Havre No. 30, Col. Juárez, Del. Cuauhtémoc, C.P. 06600, Ciudad de México, es responsable de recabar sus datos personales y del uso y protección de los mismos. Sus datos personales serán utilizados para (i) proveer los servicios y productos que ha solicitado, (ii) informarle sobre cambios en los mismos, (iii) fines de identificación, verificación y contacto, (iv) comunicarle promociones, (v) atender quejas y aclaraciones, (vi) conocer sus necesidades de productos o servicios y/o (vii) evaluar la calidad del servicio que le brindamos. Sus datos personales pueden ser tratados dentro y fuera del país por personas (físicas o morales) distintas a CMR a las que se les hayan transferido o se les lleguen a transferir, siempre garantizando la confidencialidad de los mismos y su tratamiento conforme a las finalidades contenidas en este Aviso de Privacidad. Si Usted no manifiesta su oposición para que sus datos personales sean transferidos, se entenderá que ha otorgado su consentimiento para ello. Usted tiene derecho a acceder, rectificar y cancelar sus datos personales, así como a oponerse a su tratamiento (“Derechos ARCO”), mediante solicitud al correo electrónico infoprivacidad@cmr.mx. La versión completa de este Aviso de Privacidad, sus modificaciones y los procedimientos para el ejercicio de sus Derechos ARCO pueden ser consultados en nuestra página de Internet www.cmr.mx (última actualización: Agosto de 2016).</p>
		</div>
	</div>
</main>