<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fonda_pages extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
	}
	/**
	 *
	 * control view inicio
	 *
	 */
	public function index()
	{
		$data = array(
			'disclaimer' => true
		);
		$this->load->view('pages/_blocks/header', $data, FALSE);
		$this->load->view('pages/inicio', $data, FALSE);
		$this->load->view('pages/_blocks/footer', $data, FALSE);
	}
	/**
	 *
	 * control view aviso
	 *
	 */
	public function aviso()
	{
		$data = array();
		$this->load->view('pages/_blocks/header', $data, FALSE);
		$this->load->view('pages/aviso', $data, FALSE);
		$this->load->view('pages/_blocks/footer', $data, FALSE);	
	}
	/**
	 *
	 * control view terminos
	 *
	 */
	public function terminos()
	{
		$data = array();
		$this->load->view('pages/_blocks/header', $data, FALSE);
		$this->load->view('pages/terminos', $data, FALSE);
		$this->load->view('pages/_blocks/footer', $data, FALSE);
	}
	/**
	 *
	 * control view resevaciones
	 *
	 */
	public function reservaciones()
	{
		$data = array();
		$this->load->view('pages/_blocks/header', $data, FALSE);
		$this->load->view('pages/reservaciones', $data, FALSE);
		$this->load->view('pages/_blocks/footer', $data, FALSE);	
	}
	/**
	 *
	 * control view somos
	 *
	 */
	public function somos()
	{
		$data = array(
			'active_page' => 'page-somos'
		);
		$this->load->view('pages/_blocks/header', $data, FALSE);
		$this->load->view('pages/somos', $data, FALSE);
		$this->load->view('pages/_blocks/footer', $data, FALSE);	
	}
	/**
	 *
	 * control view carta
	 *
	 */
	public function carta()
	{
		$h = date('H');
		
		$menu = 'desayuno';
		if($h >= 12)
		{
			$menu = 'comida';
		}
		$data = array(
			'menu'       => $menu,
			'disclaimer' => true,
			'active_page' => 'page-carta'
		);
		$this->load->view('pages/_blocks/header', $data, FALSE);
		$this->load->view('pages/carta', $data, FALSE);
		$this->load->view('pages/_blocks/footer', $data, FALSE);	
	}
	/**
	 *
	 * control view eventos
	 *
	 */
	public function eventos()
	{
		$data = array(
			'active_page' => 'page-evento'
		);
		$this->load->view('pages/_blocks/header', $data, FALSE);
		$this->load->view('pages/eventos', $data, FALSE);
		$this->load->view('pages/_blocks/footer', $data, FALSE);	
	}
}

/* End of file Almendros_pages.php */
/* Location: ./application/controllers/Almendros_pages.php */