<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fonda_cms extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("almendros_cms_model");
		$this->load->library("util");
		$this->load->library("mails");
		$this->load->model("crud_model");
	}
	public function index()
	{
		// echo "algo";
		$data = array(
			"description" => "Restaurante Fonda Mexicana",
			"section" => "index", 
			"title"   => "Inicio"
		);
		$this->load->view("main", $data, FALSE);
	}


	public function admin()
	{

		$this->util->val_login('log');

		$data = array(
			"section" => "admin/login",
			"msg"     => ""
		);
		$this->load->view("admin/main_login", $data, FALSE);
	}

	public function login()
	{
		if($this->session->userdata('logged'))
			redirect(base_url().'admin/dashboard','refresh');

		// set validation rules
		$this->form_validation->set_rules('usuario', 'Usuario', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == false) {
			
			$this->session->set_flashdata('user', 'Por favor verifica tu información');
			$data['section'] = 'admin/login';
			$this->load->view('admin/main_login', $data);
			
			
		} else {

			$correo   = $this->input->post('usuario', true);
			$password = $this->input->post('password', true);

			if($row = $this->almendros_cms_model->login($correo, $password)){
				//Create session of user
				$data = array(
					'logged'   => TRUE,
					'id_user'  => $row->id,
					'nombre'   => $row->nombre,
					'email'    => $row->email,
					'sucursal' => $row->sucursal,
				);

				$this->session->set_userdata($data);
				redirect(base_url().'admin/dashboard','refresh');

			}else{
				//failed data
				$this->session->set_flashdata('user', 'Por favor verifica tu información');
				$data['section'] = 'admin/login';
				$this->load->view('admin/main_login', $data);
			}
		}	
	}

	public function salir()
	{
		$this->session->sess_destroy();
		redirect(base_url()."admin");		
	}


	public function dashboard()
	{
		$this->util->val_login();

		$data = array(
			'section' => '/admin/dashboard', 
		);

		$this->load->view('admin/main', $data, FALSE);
	}

	public function eventos($id = null)
	{
		$this->util->val_login();

			if (is_null($id)) 
			{
				$salones = $this->crud_model->get_data("salones");
				$data = array(
					'section' => "admin/eventos",
					'pedidos'   => $this->crud_model->get_data("eventos",'status = "pendiente" ', "leido ASC"),
					'salon' => $salones,
				);


				$this->load->view('admin/main', $data, FALSE);
			} 
			else
			{
				$prod = $this->crud_model->get_data("eventos", "id =".$id)[0];
				$salones = $this->crud_model->get_data("salones");
				$update = array('leido' =>'1');
				$this->crud_model->update($update, "id =".$id[0], "eventos");

				$data = array(
					'prod'    => $prod,
					'section' => "admin/evento-detalle",
					'salon' => $salones,
				);

				$this->load->view('admin/main', $data, FALSE);
			}
	}


	public function eventos_confirm()
	{
		$this->util->val_login();
		//print_r($_POST);

		if ($_POST)
		{
			//$fecha = $_POST['cambio_fecha'];

				if(!isset($_POST['cambio_fecha']))
				{
					
					//var_dump($fecha);
					$folio = $this->util->get_folio();
					$update_status = array(
					"status" => $_POST['select-status'], 
					"folio" => $folio,
					);

	
					if($_POST['select-status'] == 'confirmado')
					{
						if ($this->crud_model->update($update_status, "id =".$_POST['id'], "eventos"))
						{
							$data_mail = array(

								"folio" => $folio,
								"nombre" => $_POST['nombre_original'],
								"email" => $_POST['email_original'],
								"fecha" => $_POST['fecha_original'], 
								"hora_ini" => $_POST['horaini_original'], 
								'hora_fin' => $_POST['horafin_original'],
								"salon" => $_POST['salon_original'],
								// "lugar" => $_POST['lugar_original'],
								"personas" => $_POST['personas_original'],
								"nombre_responsable" => $_POST['nombre_responsable'],
								"correo_responsable" => $_POST['correo_responsable'],
							);



							if($this->mails->mail_to_contact($data_mail))
							{
								$this->mails->mail_confirm($data_mail);
								$data = array(
									'section' => 'admin/evento_confirmacion',
									'usuario' => $data_mail,
								);

								$this->load->view('admin/main', $data, FALSE);
							}
						}
					}
					else if($_POST['select-status'] == 'cancelado')
						{
						$folio = $this->util->get_folio();
						$update = array(
							"status" => $_POST['select-status'], 

						);


						if ($this->crud_model->update($update, "id =".$_POST['id'], "eventos"))
						{
							$data_mail = array(

				
								"nombre" => $_POST['nombre_original'],
								"email" => $_POST['email_original'],
								"correo_responsable" => $_POST['correo_responsable']
							);



							if($this->mails->mail_cancel($data_mail))
							{
								$this->mails->mail_cancel_admin($data_mail);
								$data = array('section' => 'admin/evento_cancel');

								$this->load->view('admin/main', $data, FALSE);
							}
							 
						}
					}

				else if($_POST['select-status'] == 'eliminado')
				{
					$update = array(
							"status" => $_POST['select-status'], 

						);
					if ($this->crud_model->update($update, "id =".$_POST['id'], "eventos"))
						{
							$this->session->set_flashdata('error_confirmar', 'La reservacion ha sido eliminada con exito');
							redirect('/admin/eventos/'.$_POST['id']);
						}

				}

				else if($_POST['select-status'] == 'pendiente')
				{
					$update = array(
							"status" => $_POST['select-status'], 

						);
					if ($this->crud_model->update($update, "id =".$_POST['id'], "eventos"))
					{
						$this->session->set_flashdata('error_confirmar', 'Status actualizado con exito');
						redirect('/admin/eventos/'.$_POST['id']);
					}

				}
					else
					{
						$this->session->set_flashdata('error_confirmar', 'Debes cambiar el status a "Confirmado" antes de guardar');	
						redirect('/admin/eventos/'.$_POST['id']);
					}

				}

				else
				{
		
					$folio = $this->util->get_folio();
					$update = array(
						"fecha" => $_POST['cambio_fecha'], 
						"hora_ini" => $_POST['cambio_horaini'], 
						'hora_fin' => $_POST['cambio_horafin'],
						"salon" => $_POST['select_salon'], 
						"status" => $_POST['select-status'], 
						"folio" => $folio,
						'personas' => $_POST['nuevas_personas'],
					);


					if($_POST['select-status'] == 'confirmado')
					{

						if ($this->crud_model->update($update, "id =".$_POST['id'], "eventos"))
						{

							$data_mail = array(

								"folio" => $folio,
								"nombre" => $_POST['nombre_original'],
								"email" => $_POST['email_original'],
								"fecha" => $_POST['cambio_fecha'], 
								"hora_ini" => $_POST['cambio_horaini'], 
								'hora_fin' => $_POST['cambio_horafin'],
								"salon" => $_POST['select_salon'],
								// "lugar" => $_POST['lugar'],
								"personas" => $_POST['nuevas_personas'],
								"nombre_responsable" => $_POST['nombre_responsable'],
								"correo_responsable" => $_POST['correo_responsable'],
							);



							if($this->mails->mail_to_contact($data_mail))
							{
								$this->mails->mail_confirm($data_mail);


								$data = array(
									'section' => 'admin/evento_confirmacion',
									'usuario' => $data_mail,

								);

								$this->load->view('admin/main', $data, FALSE);
							}
							
						}
					}


						
					else
					{
						$this->session->set_flashdata('error_confirmar', 'Debes cambiar el status a "Confirmado" antes de guardar');	
						redirect('/admin/eventos/'.$_POST['id']);
					}


				}
			


			

		}
	}

//valida si lo que se envia del front esta disponible y envia un array de los salones disponibles
	public function disponibilidad_front()
	{

			/*$data = array(
				'lugar' => 'almendros insurgentes',
				'fecha' => '2018-05-17',
				'hora_ini' => '17:00:00',
				'hora_fin' => '19:00:00',
				'personas' => '100',

			);*/

		$arreglo_eventos= array();
		
		
		if($_POST)
		{
			$data = array(
					'fecha' => ($this->input->post('fecha', true)),
					'hora_ini' => ($this->input->post('hora_ini', true)),
					'hora_fin' => ($this->input->post('hora_fin', true)),
					'personas' => $this->input->post('personas', true),

				);

			// $tmp_fecha = explode("/", $data['fecha']);
			// $fecha = $tmp_fecha[2] . "-".$tmp_fecha[1]."-".$tmp_fecha[0];

			$where = array(
				//'fecha' => $fecha,
				'fecha' => $data['fecha'],
			);


			$eventos = $this->crud_model->get_data("eventos", $where , "nombre ASC"); 

			

			foreach ($eventos as $evento) 
			{
					/*print_r(strtotime($data['hora_fin']) . "hora final usuario");
					print_r("separador");
					print_r(strtotime($evento['hora_ini']) . "hora inicial sql");

					print_r(strtotime($data['hora_ini']) . "hora inicial usuario");
					print_r("separador");
					print_r(strtotime($evento['hora_fin']) . "hora final sql");*/
				
				if( 

					(strtotime($data['hora_fin']) < strtotime($evento['hora_ini']) && strtotime($data['hora_ini']) < strtotime($evento['hora_ini'])  ) 
					|| 
					( strtotime($data['hora_ini']) > strtotime($evento['hora_fin']) && strtotime($data['hora_fin']) > strtotime($evento['hora_fin'])  )
					)
				{
						
					
				}	
				else
				{
					$arreglo_eventos[] = array('evento' => $evento);
				}
				
			}



			$rango = explode($data['personas'], "-");

			//si el arreglo de eventos esta vacio es porque las horas seleccionadas estan disponibles y hay que comprobar solo el salon

			if(empty($arreglo_eventos))
			{
			
				$arreglo_salones = array();


				$salones = $this->crud_model->get_data("salones", null, "salon ASC");


				foreach ($salones as $salon) 
				{

					$arreglo_salones[] = array('salon' => $salon);


					/*$rango_usr = explode("-",$data['personas']);
					$rango_salon = explode("-",$salon['personas']);

					if (count($rango_usr) > 1)//significa que hay dos numeros
					{
						
						if(  ($rango_salon[0] < $rango_usr[0]) && ($rango_usr[0] <  $rango_salon[1]) && ($rango_salon[0] < $rango_usr[1]) && ($rango_usr[1] < $rango_salon[1]) )
						{
							$arreglo_salones[] = array('salon' => $salon);

						}

					}
					else
					{

						if($rango_usr[0] <= $rango_salon[0])
						{

							$arreglo_salones[] = array('salon' => $salon);

						}

					}*/
				}
 
			}

			else
			{
				
				$arreglo_salones = array();

				//si esta lleno es porque si hay algo a esa hora pero puede haber otro salon, por lo que se procede a verificar si cumple con los requerimientos
				foreach ($arreglo_eventos as $eventos) 
				{

					$where = array(
						'salon !=' => $eventos['evento']['salon'],
					);
					$salones = $this->crud_model->get_data("salones", $where, "salon ASC");

					foreach ($salones as $salon) 
					{

						$arreglo_salones[] = array('salon' => $salon);
						/*$rango_usr = explode("-",$eventos['evento']['personas']);
						$rango_salon = explode("-",$salon['personas']);

						if (count($rango_usr) > 1)//significa que hay dos numeros
						{
							
							if(  ($rango_salon[0] < $rango_usr[0]) && ($rango_usr[0] <  $rango_salon[1]) && ($rango_salon[0] < $rango_usr[1]) && ($rango_usr[1] < $rango_salon[1]) )
							{


								 $arreglo_salones[] = array('salon' => $salon);
							}
						}
						else
						{
							
							if($eventos['evento']['personas'] <= $rango_salon[0])
							{
							

								$arreglo_salones[] = array('salon' => $salon);
							}
						}*/
					}
				}
			}
			


		}
		echo json_encode($arreglo_salones);
	}

	public function guardarDB_front()
	{
		/*$data = array(
				'lugar' => 'almendros insurgentes',
				'fecha' => '2018-05-17',
				'hora_ini' => '17:00:00',
				'hora_fin' => '19:00:00',
				'personas' => '100',
				'nombre' => 'tibu one',
				'email' => 'tibu@engranestudio.com',
				'telefono' => '45864069456',
				'salon' => 'salon 1',
				'status' => 'pendiente',
					'leido' => '1',

			);

		if ($this->crud_model->add('eventos', $data))
			{
				$mensaje = "Success";
			}

			else
			{
				$mensaje = "Fail";
			}

			print_r($mensaje);
		*/
		if($_POST)
		{

			// var_dump($this->input->post('captcha', true));

			$secret = "6Lfo1V8UAAAAAGpIN1TvF_DfV_8Tl1O7CjDnonXV";
			$response = $this->input->post('captcha', true);

			$verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");

			$captcha_success = json_decode($verify);


			if($captcha_success->success == true){

				$data = array(
					'fecha' => $this->input->post('fecha', true),
					'hora_ini' => $this->input->post('hora_ini', true),
					'hora_fin' => $this->input->post('hora_fin', true),
					'personas' => $this->input->post('personas', true),
					'nombre' => $this->input->post('nombre' , true),
					'email' => $this->input->post('email', true),
					'telefono' => $this->input->post('telefono', true),
					'salon' => $this->input->post('salon', true),
					'status' => 'pendiente',
					'leido' => '0',
					'tipo_evento' => $this->input->post('tipo_evento', true),

				);	

				// $tmp_fecha = explode("/", $data['fecha']);
				// $fecha = $tmp_fecha[2] . "-".$tmp_fecha[1]."-".$tmp_fecha[0];

				// $data['fecha'] = $fecha;

				if ($id = $this->crud_model->add('eventos', $data))
				{
					$data_asesor = array(
						'id' => $id
					);

					/*Engrane studio
					Aqui se reemplazaran por los datos de fonda mexicana*/
					$data_info_asesor = array(
						// 'asesor_nombre' => ($data['lugar'] == "polanco" ? "Tania Ávila" : "Nancy Olvivares"),
						// 'asesor_mail'   => ($data['lugar'] == "polanco" ? "ealmendrospol@cmr.mx" : "ealmendrossur@cmr.mx" ),
						// 'asesor_tel'    => ($data['lugar'] == "polanco" ? "<br>Tel. 5531-6646" : "" )
						'asesor_nombre' => "Asesor",
						'asesor_mail'   => "efondapol@cmr.mx",
						'asesor_tel'    => ""
					);

					$data_nueva_reserva = array_merge($data, $data_info_asesor);
					
					if($this->mails->mail_asesor($data_asesor)){}
					if($this->mails->mail_nueva_reserva($data_nueva_reserva)){}

					// $mensaje = "Sucess";
					$response = array(
						'mensaje' => 'success',
						'data' => '<div class="msg-thanks">
									<p><span>GRACIAS,</span><br> HEMOS RECIBIDO TU SOLICITUD. UN ASESOR TE CONTACTARÁ A LA BREVEDAD.</p>
									</div>
									<div class="disclaimer">
									<p>CONOCE NUESTRAS OPCIONES DE PLATILLOS, EQUIPO Y TODO EL SERVICIO PARA TU EVENTO.</p>
									<div>
										<a href="#">NUESTRAS OPCIONES</a>
									</div>
								</div>'
					);

				}

				else
				{
					// $mensaje = "Fail";
					$response = array(
						'mensaje' => 'fail',
						'data' => '<div class="msg-thanks">
									<p><span>Ooops!,</span><br> lo sentimos algo salió mal, intentalo más tarde.</p>
									</div>
								</div>'
					);
				}
			}else{
				$response = array(
					'mensaje' => 'fail',
					'data' => '<div class="msg-thanks">
								<p>Verifica que no eres un robot</p>
								</div>
							</div>'
				);				
			}
			

		}

		echo json_encode($response);
	}

	public function validarFechasCMS()
	{
		$eventos1 = array();
		$eventos2 = array();
		$arreglo_eventos= array();
		
		
		if($_POST)
		{
			$data = array(
					'id' => $this->input->post('id', true),
					'fecha' => ($this->input->post('fecha', true)),
					'hora_ini' => ($this->input->post('hora_ini', true)),
					'hora_fin' => ($this->input->post('hora_fin', true)),
					'personas' => $this->input->post('personas', true),
					'salon' => $this->input->post('salon', true),

				);



			//$tmp_fecha = explode("/", $data['fecha']);
			//$fecha = $tmp_fecha[2] . "-".$tmp_fecha[1]."-".$tmp_fecha[0];
		
			$where = array(
				'id!=' => $data['id'],
				'fecha' => $data['fecha'],
				'salon' => $data['salon'],
			);


			$eventos = $this->crud_model->get_data("eventos", $where , "nombre ASC"); 

			

			foreach ($eventos as $evento) 
			{
					/*print_r(strtotime($data['hora_fin']) . "hora final usuario");
					print_r("separador");
					print_r(strtotime($evento['hora_ini']) . "hora inicial sql");

					print_r(strtotime($data['hora_ini']) . "hora inicial usuario");
					print_r("separador");
					print_r(strtotime($evento['hora_fin']) . "hora final sql");*/
				
				if( 

					(strtotime($data['hora_fin']) < strtotime($evento['hora_ini']) && strtotime($data['hora_ini']) < strtotime($evento['hora_ini'])  ) 
					|| 
					( strtotime($data['hora_ini']) > strtotime($evento['hora_fin']) && strtotime($data['hora_fin']) > strtotime($evento['hora_fin'])  )
					)
				{
						
					
				}	
				else
				{
					$arreglo_eventos[] = array('evento' => $evento);
				}
				
			}

			//$rango = explode($data['personas'], "-");

			//si el arreglo de eventos esta vacio es porque las horas seleccionadas estan disponibles y hay que comprobar solo el salon

			if(empty($arreglo_eventos))
			{
			
				$arreglo_salones = array();

				$where = array(
					"salon" => $data['salon'],
				);
				$salones = $this->crud_model->get_data("salones", $where, "salon ASC");

				foreach ($salones as $salon) 
				{
					$rango_usr = explode("-",$data['personas']);
					$rango_salon = explode("-",$salon['personas']);

					if (count($rango_usr) > 1)//significa que hay dos numeros
					{
						
						if(  ($rango_salon[0] < $rango_usr[0]) && ($rango_usr[0] <  $rango_salon[1]) && ($rango_salon[0] < $rango_usr[1]) && ($rango_usr[1] < $rango_salon[1]) )
						{
							$arreglo_salones[] = array('salon' => $salon);

						}

					}
					else
					{

						if($rango_usr[0] <= $rango_salon[0])
						{

							$arreglo_salones[] = array('salon' => $salon);

						}
					}
				}


				if (!empty($arreglo_salones))
				{
			
					echo "success";
				}

				else
				{
				
					echo "fail";
				}

			}

			else
			{
				
				$arreglo_salones = array();

				//si esta lleno es porque si hay algo a esa hora pero puede haber otro salon, por lo que se procede a verificar si cumple con los requerimientos
				foreach ($arreglo_eventos as $eventos) 
				{

					$where = array(
						'salon !=' => $eventos['evento']['salon'],
					);
					$salones = $this->crud_model->get_data("salones", $where, "salon ASC");

					foreach ($salones as $salon) 
					{
						$rango_usr = explode("-",$eventos['evento']['personas']);
						$rango_salon = explode("-",$salon['personas']);

						if (count($rango_usr) > 1)//significa que hay dos numeros
						{
							
							if(  ($rango_salon[0] < $rango_usr[0]) && ($rango_usr[0] <  $rango_salon[1]) && ($rango_salon[0] < $rango_usr[1]) && ($rango_usr[1] < $rango_salon[1]) )
							{


								 $arreglo_salones[] = array('salon' => $salon);
							}
						}
						else
						{
							
							if($eventos['evento']['personas'] <= $rango_salon[1])
							{
							

								$arreglo_salones[] = array('salon' => $salon);
							}
						}
					}


				}


				if (empty($arreglo_salones))
				{
					
					echo "success";
				}

				else
				{

					echo "fail";
				}
			}
			
		

		}
	}


	public function notificaciones()
	{
		$leidos = $this->crud_model->get_data("eventos", "leido ='0'");

		print_r( count($leidos));
	}


	public function eliminar($id = null)
	{
		$this->util->val_login();
		$data = array(
					'status' => "eliminado", 
				);

		$this->crud_model->delete($data, array('id' => $id), "eventos");
		redirect(base_url()."admin/eventos", 'refresh');
	}


	public function filtred($id = null)
	{
		$this->util->val_login();

			if (is_null($id)) 

			{
				$salones = $this->crud_model->get_data("salones");
				$data = array(
					'section' => "admin/eventos",
					'pedidos'   => $this->crud_model->get_data("eventos",null,null, "leido ASC"),
					'salon' => $salones,
				);


				$this->load->view('admin/main', $data, FALSE);
			} 

			else
			{

				switch ($id) {
					case 'confirm':
						$salones = $this->crud_model->get_data("salones");
						$where = array(
							'status' => 'confirmado',
						);
						$data = array(
							'section' => "admin/eventos",
							'pedidos'   => $this->crud_model->get_data("eventos",$where, "leido ASC"),
							'salon' => $salones,
						);


						$this->load->view('admin/main', $data, FALSE);
						break;

					case 'pendient':
					$salones = $this->crud_model->get_data("salones");
						$where = array(
							'status' => 'pendiente',
						);
						$data = array(
							'section' => "admin/eventos",
							'pedidos'   => $this->crud_model->get_data("eventos",$where, "leido ASC"),
							'salon' => $salones,
						);


						$this->load->view('admin/main', $data, FALSE);
						
						break;

					case 'cancel':
					$salones = $this->crud_model->get_data("salones");
						$where = array(
							'status' => 'cancelado',
						);
						$data = array(
							'section' => "admin/eventos",
							'pedidos'   => $this->crud_model->get_data("eventos",$where, "leido ASC"),
							'salon' => $salones,
						);


						$this->load->view('admin/main', $data, FALSE);
						
						break;

					case 'deleted':
					$salones = $this->crud_model->get_data("salones");
						$where = array(
							'status' => 'eliminado',
						);
						$data = array(
							'section' => "admin/eventos",
							'pedidos'   => $this->crud_model->get_data("eventos",$where, "leido ASC"),
							'salon' => $salones,
						);


						$this->load->view('admin/main', $data, FALSE);
						
						break;
					
					default:
						
						break;
				}
			}

		}



		public function alerta_reserva()
		{
			if($_POST)
			{
				
				$data_mail = array(
						'nombre' => $this->input->post('nombre', true),
						'telefono' => $this->input->post('telefono', true),
						'correo' => $this->input->post('correo', true),
						'lugar' => $this->input->post('lugar', true),
						'tipo_evento' => $this->input->post('tipo_evento', true),
						'fecha' => $this->input->post('fecha', true),
						'hora_ini' => $this->input->post('hora_ini', true),
						'hora_fin' => $this->input->post('hora_fin', true),
						'personas' => $this->input->post('personas', true),
						'salon' => $this->input->post('salon', true),

					);	

				if($this->mails->mail_nueva_reserva($data_mail))
				{
					$this->mails->mail_asesor();
					echo "success";
				}
				else
				{
					echo "fail";
				}
			}
		}


		public function formulario_contacto()
		{
			if($_POST)
			{
				
				$data_mail = array(
						'nombre' => $this->input->post('nombre', true),
						'sucursal' => $this->input->post('sucursal', true),
						'correo' => $this->input->post('correo', true),
						'comentario' => $this->input->post('comentario', true),
					);	

				if($this->mails->mail_contacto($data_mail))
				{
					echo "success";
				}
				else
				{
					echo "fail";
				}
			}

		}


}




/* End of file Almendros_cms.php */
/* Location: ./application/controllers/Almendros_cms.php */